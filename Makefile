ENV_FILE ?= .env
CONTAINER_NAME ?= colmena_frontend
IMAGE_NAME ?= colmena_frontend
NETWORK_NAME ?= local_colmena_devops
DOCKERFILE_DIR ?= devops/builder/
SHELL := /bin/bash
VENV := venv
BIN := $(VENV)/bin

GREEN=\033[0;32m
YELLOW=\033[0;33m
NOFORMAT=\033[0m

# Add env variables if needed
ifneq (,$(wildcard ${ENV_FILE}))
	include ${ENV_FILE}
    export
endif

default: help

.PHONY: build
build: SHELL := /bin/bash
build:
	@npm run build

.PHONY: help
#❓ help: @ Displays this message
help: SHELL := /bin/sh
help:
	@echo ""
	@echo "List of available MAKE targets for development usage."
	@echo ""
	@echo "Usage: make [target]"
	@echo ""
	@echo "Examples:"
	@echo ""
	@echo "	make ${GREEN}setup${NOFORMAT}		- Setup requirements"
	@echo "	make ${GREEN}server${NOFORMAT}		- Start server"
	@echo ""
	@grep -E '[a-zA-Z\.\-]+:.*?@ .*$$' $(firstword $(MAKEFILE_LIST))| tr -d '#'  | awk 'BEGIN {FS = ":.*?@ "}; {printf "${GREEN}%-30s${NOFORMAT} %s\n", $$1, $$2}'
	@echo ""

#🐳 devops.build: @ Builds a new image for the frontend service.
docker.build: API_URL:=http://localhost:5001
docker.build: OPENAPI_SCHEMA_LOCATION:=https://backend.dev.colmena.network/api/schema
docker.build: CONTAINER_NAME:=$(CONTAINER_NAME)
docker.build:
	@docker build \
		--build-arg API_URL=$(API_URL) \
		--build-arg OPENAPI_SCHEMA_LOCATION=$(OPENAPI_SCHEMA_LOCATION) \
		-f $(DOCKERFILE_DIR)/Dockerfile.local \
		-t $(CONTAINER_NAME) \
		./

#🐳 docker.connect: @ Connect to the frontend running container
docker.connect:
	@docker exec -it $(CONTAINER_NAME) /bin/sh

#🐳 docker.delete: @ Delete the frontend docker container
docker.delete: CONTAINER_NAME:=$(CONTAINER_NAME)
docker.delete:
	@docker rm $(CONTAINER_NAME) 2> /dev/null || true
	@docker image rm $(IMAGE_NAME) 2> /dev/null || true

#🐳 docker.logs: @ Show logs for the docker container
docker.logs: CONTAINER_NAME:=$(CONTAINER_NAME)
docker.logs:
	@docker logs $(CONTAINER_NAME)

#🐳 docker.logs.watch: @ Watch logs for the docker container
docker.logs.watch: CONTAINER_NAME:=$(CONTAINER_NAME)
docker.logs.watch:
	@docker logs $(CONTAINER_NAME) -f

#🐳 docker.release: @ Re-create a docker image and run it
docker.release: docker.stop docker.delete docker.build docker.run

#🐳 docker.rerun: @ Stops and deletes old container to re-run a fresh new container
docker.rerun: docker.stop docker.delete docker.run

#🐳 docker.run: @ Run the frontend docker instance
docker.run: PORT:=5001
docker.run: CONTAINER_NAME:=$(CONTAINER_NAME)
docker.run: IMAGE_NAME:=$(IMAGE_NAME)
docker.run:
	@docker run --detach --name $(CONTAINER_NAME) --network $(NETWORK_NAME) -p 5002:80 --env PORT=5002 --env-file .env.prod $(IMAGE_NAME)

#🐳 docker.stop: @ Stop the frontend docker container
docker.stop: CONTAINER_NAME:=$(CONTAINER_NAME)
docker.stop:
	@docker container stop $(CONTAINER_NAME) 2> /dev/null || true

#💻 lint: @ Runs eslint fixes
.PHONY: lint
lint:
	@npm run lint:fix

#💻 check: @ Runs eslint checks
check:
	@npm run lint

.PHONY: setup
#📦 setup: @ Install node dependencies.
setup: OPENAPI_SCHEMA_LOCATION:=http://localhost:8000/api/schema
setup:
	@npm install

#💻 server: @ Starts a development server.
server:
	@npm run dev

#📙 translations: @ Extract new untranslated text.
translations:
	@npm run extract-translations
