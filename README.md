# Colmena

<p align="center" style="margin-top: 14px;">
  <a href="https://gitlab.com/colmena-project/dev/frontend/-/pipelines?page=1&scope=all&ref=dev">
    <img
      src="https://gitlab.com/colmena-project/dev/frontend/badges/dev/pipeline.svg?key_text=dev%20pipeline&key_width=80"
      alt="Dev Pipeline Status"
    >
  </a>
  <a href="https://gitlab.com/colmena-project/dev/frontend/-/pipelines?page=1&scope=all&ref=main">
    <img
      src="https://gitlab.com/colmena-project/dev/frontend/badges/main/pipeline.svg?key_text=staging%20pipeline&key_width=96"
      alt="Stage Pipeline Status"
    >
  </a>
  <a href="https://gitlab.com/colmena-project/dev/frontend/-/blob/dev/LICENSE">
    <img
      src="https://img.shields.io/badge/License-GPL%20v3-white.svg"
      alt="License"
    >
  </a>
</p>

`colmena-frontend` is the React application for the colmena platform.

## Requirements

To run the application your system needs to have nodejs installed. You can either use the official node installation guides or via the `asdf` package manager, using the fixed version from the *.tool-versions* file.

* Nodejs: `20.14.0`
* asdf: (optional) [installation instructions](https://asdf-vm.com/guide/getting-started.html#_2-download-asdf)
* Docker [`24.0.2`](https://docs.docker.com/desktop/install/ubuntu/) (optional): used to run dockerized images of the frontend application.
* Compose plugin [`v2.3.3`](https://docs.docker.com/compose/install/linux/) (optional)

### Dependencies

The frontend application depends on the backend application to be up and running. Go back to the [backend repository](https://gitlab.com/colmena-project/dev/backend/-/blob/dev/README.md) and setup it up to continue.

## Getting started

### Local Environment

Make a copy of the base env file.

```bash
cp .env.example .env
```

There's currently no need of making any changes any variable, but here's a brief definition for them:

#### Backend configuration

* `VITE_BACKEND_API_URL`: The hostname for the local backend instance.
* `OPENAPI_SCHEMA_LOCATION`: The url location of the openapi schema.

We use `make` to quickly run predefined tasks to setup, start and run tests against our environment.

```bash
# Type make to display the default help message.
# You'll notice a bunch of suggestions that you can use to manage the database, skip them for now, they'll be useful as
# a reminder.
make
```

Install all node dependencies

```bash
make setup
```

Run the eslint formatter

```bash
make lint
```

Run the development server

```bash
make server
```

Run checks, such as eslint

```bash
make check
```

Run integration tests

```bash
make test.integration
```

Run the translations generator

```bash
make translations
```

### Virtualization

To run a production release of the application we use Docker. There are a bunch of docker commands that help us completing this task easily.

First make a copy of the *.env.example* file into *.env.prod* and reconfigure variables using the hosts provided by the docker environment.

Stop and delete any running containers, re-build the application and run it using:

```bash
make docker.release
```

One could also run those tasks above individually using:

```bash
# Stop a running container
make docker.stop
# Build an image
make docker.build
# Watch logs
make docker.logs
# or
make docker.logs.watch
# Connect to a container
make docker.connect
```
