import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import reducers from './reducers';
import { SET_AUDIO_RENDERING } from './actions';

export const store = configureStore({
  reducer: reducers,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        // Ignore these action types
        ignoredActions: [SET_AUDIO_RENDERING],
        // Ignore these paths in the state
        ignoredPaths: ['recording.audioRenderingData'],
      },
    }),
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
