import { AlertType, ADD_ALERT, REMOVE_ALERT } from '../types/types.ts';
import { RootState } from '../store.ts';

export interface AlertContent {
  message: string;
  variant: 'default' | 'success' | 'danger' | 'warning' | 'info' | undefined;
}

interface AlertRepository {
  alertRepository: {
    alerts: Record<number, AlertContent>;
    lastGeneratedId: number;
  };
}

const initialState: AlertRepository = {
  alertRepository: {
    alerts: {},
    lastGeneratedId: 0,
  },
};

export default function (
  state = initialState,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  action: { type: AlertType; payload: any },
) {
  const { type, payload } = action;

  switch (type) {
    case ADD_ALERT: {
      const newAlertId = state.alertRepository.lastGeneratedId + 1;
      const newAlerts = {
        ...state.alertRepository.alerts,
        [newAlertId]: {
          message: payload.message,
          variant: payload.variant,
        },
      };

      return {
        ...state,
        alertRepository: {
          alerts: newAlerts,
          lastGeneratedId: newAlertId,
        },
      };
    }

    case REMOVE_ALERT: {
      const newAlerts = { ...state.alertRepository.alerts };
      delete newAlerts[payload.id];

      return {
        ...state,
        alertRepository: {
          alerts: newAlerts,
          lastGeneratedId: state.alertRepository.lastGeneratedId,
        },
      };
    }

    default:
      return state;
  }
}

export const getAllAlerts = (state: RootState): Record<number, AlertContent> =>
  state.alert.alertRepository.alerts;
