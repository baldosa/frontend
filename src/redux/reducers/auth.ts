import {
  REGISTER_SUCCESS,
  REGISTER_FAIL,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  LOGOUT,
  AuthType,
  REGISTER_TAC_CONFIRM_ACCEPTANCE,
  UPDATE_ACCESS_TOKEN,
  REGISTER_TAC_CANCEL_ACCEPTANCE,
  SAVE_AUTH_DATA,
  UPDATE_SERVER_DATA,
  SAVE_PREVIOUS_AUTH_URL
} from '../types/types.ts';
import { RootState } from '../store.ts';
import { getServerData } from '../../utils/logics/authUtils.ts';

export const userKey = 'user';
export const serverIdKey = 'serverId'

const userFromStorage: string | null = localStorage.getItem(userKey);
const serverIdFromStorage: string | null = localStorage.getItem(serverIdKey)

const defaultState = {
  isLoggedIn: false,
  user: {
    access: '',
    refresh: '',
    user: {
      email: null,
      username: null,
      password: null,
      organizationId: null,
      roles: [],
    },
  },
  server: {
    id: null,
    name: '',
    url: ''
  },
  previousUrl: '',
  acceptTermsAndConditions: false
};

const getInitialState = async () => {
  const state = defaultState

  if (userFromStorage) {
    state.isLoggedIn = true;
    state.user = JSON.parse(userFromStorage);
    state.acceptTermsAndConditions = true;
  }

  if (serverIdFromStorage) {
    state.server = await getServerData(Number(serverIdFromStorage));
  }

  return state
}

const initialState = await getInitialState()

export default function (
  state = initialState,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  action: { type: AuthType; payload: any },
) {
  const { type, payload } = action;

  switch (type) {
    case SAVE_AUTH_DATA:
      return {
        ...state,
        user: payload.user,
      };
    case REGISTER_TAC_CONFIRM_ACCEPTANCE:
      return {
        ...state,
        acceptTermsAndConditions: true,
      };
    case REGISTER_TAC_CANCEL_ACCEPTANCE:
      return {
        ...state,
        acceptTermsAndConditions: false,
      };
    case REGISTER_SUCCESS:
      return {
        ...state,
      };
    case REGISTER_FAIL:
      return {
        ...state,
      };
    case LOGIN_SUCCESS:
      return {
        ...state,
        isLoggedIn: true,
        user: payload.user,
      };
    case LOGIN_FAIL:
      return {
        ...state,
        isLoggedIn: false,
      };
    case LOGOUT:
      localStorage.removeItem(userKey);
      return {
        ...state,
        isLoggedIn: defaultState.isLoggedIn,
        user: defaultState.user,
        acceptTermsAndConditions: defaultState.acceptTermsAndConditions,
      };
    case UPDATE_ACCESS_TOKEN:
      return {
        ...state,
        user: {
          ...state.user,
          access: payload.access,
        },
      };
    case UPDATE_SERVER_DATA:
      return {
        ...state,
        server: {
          id: payload.serverId,
          name: payload.serverName,
          url: payload.serverUrl,
        }
      }
    case SAVE_PREVIOUS_AUTH_URL:
      return {
        ...state,
        previousUrl: payload.previousUrl
      }
    default:
      return state;
  }
}

export const getUsername = (state: RootState) => state.auth.user.user.username;
export const getFullName = (state: RootState) => state.auth.user.user.full_name;
export const getRoles = (state: RootState) => state.auth.user.user.roles;
export const getEmail = (state: RootState) => state.auth.user.user.email;
export const getAccessToken = (state: RootState) => state.auth.user.access;
export const getRefreshToken = (state: RootState) => state.auth.user.refresh;
export const getPassword = (state: RootState) => state.auth.user.user.password;
export const getOrganizationId = (state: RootState) => state.auth.user.user.organizationId;
export const selectAcceptTermsAndConditions = (state: RootState) => state.auth.acceptTermsAndConditions;
export const isLoggedIn = (state: RootState) => state.auth.isLoggedIn;
export const getUserId = (state: RootState) => state.auth.user.user.pk;
export const isSelectedServer = (state: RootState) => state.auth.server.id !== null;
export const getServer = (state: RootState) => state.auth.server;
export const getPreviousUrl = (state: RootState) => state.auth.previousUrl;