import {
  ENDING_AWAITING_RESPONSE,
  ActionStatusType,
  INITIATING_AWAITING_RESPONSE,
  ADD_BLOCKING_VALIDATION_REQUEST,
  RESOLVE_BLOCKING_VALIDATION_REQUEST,
  ADD_NON_BLOCKING_VALIDATION_REQUEST,
  RESOLVE_NON_BLOCKING_VALIDATION_REQUEST,
  RESET_VALIDATION_REQUESTS,
  SET_HEADER_DESCRIPTION,
  SET_HEADER_NAME,
  SET_HEADER_NAVIGATION_DESTINATION,
} from '../types/types.ts';
import { RootState } from '../store.ts';

export interface HelperTextData {
  status_code: string;
  error_code: string;
  value?: string;
  variant: 'default' | 'success' | 'warning' | 'error' | undefined;
  originId: string;
}

interface ActionStateReducer {
  blockingValidationRequest: Record<string, HelperTextData>;
  nonBlockingValidationRequest: Record<string, HelperTextData>;
  awaitingResponse: boolean;
  headerName: string;
  headerDescription: string;
  headerNavigationDestination: string;
}

const initialState: ActionStateReducer = {
  blockingValidationRequest: {},
  nonBlockingValidationRequest: {},
  awaitingResponse: false,
  headerName: '',
  headerDescription: '',
  headerNavigationDestination: '',
};

const getCompositeKey = (keys: string[]) => {
  return keys.join(' ');
};

const getKey = (
  value: string | string[],
  record: Record<string, HelperTextData>,
): string => {
  if (typeof value === 'string') {
    const keysComposed: string[] = Object.keys(record);
    const key: string | undefined = keysComposed.find((key) =>
      key.includes(value),
    );
    return key ? key : value;
  } else {
    return getCompositeKey(value);
  }
};

export default function (
  state: ActionStateReducer = initialState,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  action: { type: ActionStatusType; payload: any },
): ActionStateReducer {
  const { type, payload } = action;

  switch (type) {
    case INITIATING_AWAITING_RESPONSE:
      return {
        ...state,
        awaitingResponse: true,
      };
    case ENDING_AWAITING_RESPONSE:
      return {
        ...state,
        awaitingResponse: false,
      };
    case ADD_NON_BLOCKING_VALIDATION_REQUEST: {
      const nonBlockingValidationRequest: Record<string, HelperTextData> = {
        ...state.nonBlockingValidationRequest,
      };
      const key: string | undefined = getKey(
        payload.field_names,
        nonBlockingValidationRequest,
      );
      nonBlockingValidationRequest[key] = {
        status_code: payload.status_code,
        error_code: payload.error_code,
        value: payload.value,
        variant: payload.variant,
        originId: payload.originId,
      };
      return {
        ...state,
        nonBlockingValidationRequest: nonBlockingValidationRequest,
      };
    }
    case RESOLVE_NON_BLOCKING_VALIDATION_REQUEST: {
      const nonBlockingValidationRequest: Record<string, HelperTextData> = {
        ...state.nonBlockingValidationRequest,
      };
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      const key: string = getKey(
        payload.field_names,
        nonBlockingValidationRequest,
      )!;
      nonBlockingValidationRequest[key] = {
        ...nonBlockingValidationRequest[key],
        variant: 'success',
      };
      return {
        ...state,
        nonBlockingValidationRequest: nonBlockingValidationRequest,
      };
    }
    case ADD_BLOCKING_VALIDATION_REQUEST: {
      const blockingValidationRequest: Record<string, HelperTextData> = {
        ...state.blockingValidationRequest,
      };
      const key: string = getKey(
        payload.field_names,
        blockingValidationRequest,
      );
      blockingValidationRequest[key] = {
        status_code: payload.status_code,
        error_code: payload.error_code,
        value: payload.value,
        variant: payload.variant,
        originId: payload.originId,
      };
      return {
        ...state,
        blockingValidationRequest: blockingValidationRequest,
      };
    }
    case RESOLVE_BLOCKING_VALIDATION_REQUEST: {
      const blockingValidationRequest: Record<string, HelperTextData> = {
        ...state.blockingValidationRequest,
      };
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      const key: string = getKey(
        payload.field_names,
        blockingValidationRequest,
      )!;
      blockingValidationRequest[key] = {
        ...blockingValidationRequest[key],
        variant: 'success',
      };
      return {
        ...state,
        blockingValidationRequest: blockingValidationRequest,
      };
    }
    case RESET_VALIDATION_REQUESTS: {
      return {
        ...state,
        nonBlockingValidationRequest: {},
        blockingValidationRequest: {},
      };
    }
    case SET_HEADER_NAME: {
      return {
        ...state,
        headerName: payload.headerTeamName ? payload.headerTeamName : '',
      };
    }
    case SET_HEADER_DESCRIPTION: {
      return {
        ...state,
        headerDescription: payload.headerDescription,
      };
    }
    case SET_HEADER_NAVIGATION_DESTINATION: {
      return {
        ...state,
        headerNavigationDestination: payload.headerNavigationDestination,
      };
    }
    default:
      return state;
  }
}

export const getHeaderName = (state: RootState) =>
  state.generalActions.headerName;
export const getHeaderDescription = (state: RootState) =>
  state.generalActions.headerDescription;
export const getHeaderNavigationDestination = (state: RootState) =>
  state.generalActions.headerNavigationDestination;
export const getActionState = (state: RootState) =>
  state.generalActions.awaitingResponse;
export const getBlockingValidationRequest = (state: RootState) =>
  state.generalActions.blockingValidationRequest;
export const getNonBlockingValidationRequest = (state: RootState) =>
  state.generalActions.nonBlockingValidationRequest;
export const allNonBlockingRequestsWereValidated = (state: RootState) =>
  Object.values(state.generalActions.nonBlockingValidationRequest).every(
    (item: HelperTextData) => item.variant === 'success' ||
      item.variant === 'default',
  );
export const allBlockingRequestsWereValidated = (state: RootState) =>
  Object.values(state.generalActions.blockingValidationRequest).every(
    (item: HelperTextData) => item.variant === 'success',
  );
