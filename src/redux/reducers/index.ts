import { combineReducers } from 'redux';
import auth from './auth';
import userProfile from './configuration/initial/userProfileDetails.ts';
import organizationDetails from './configuration/initial/organizationDetails.ts';
import invitationSection from './configuration/initial/invitationSection.ts';
import preConfiguration from './configuration/preConfiguration.ts';
import generalActions from './generalActions.ts';
import recordingReducer from './recordings.ts';
import teams from './teams.ts';
import alert from './alert.ts';

export default combineReducers({
  auth,
  alert,
  generalActions,
  preConfiguration,
  userProfile,
  organizationDetails,
  invitationSection,
  recording: recordingReducer,
  teams,
});
