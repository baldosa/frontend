import {
  TeamType,
  MESSAGES_OVERWRITING_STARTED,
  MESSAGES_OVERWRITING_COMPLETED,
  MESSAGES_APPEND_STARTED,
  MESSAGES_APPEND_COMPLETED,
  REMOVE_MESSAGE,
  CONTENT_ACTION_COMPLETED,
  CONTENT_ACTION_STARTED,
} from '../types/types.ts';
import { RootState } from '../store.ts';
import { Message } from '../../pages/Team/Chat/Message/Message.tsx';

export interface Team {
  messages: Message[];
  lastMassageId: string;
  lastMessages: Message[];
  contentsActionInProgress: number[];
  isMessagesOverwriteRequired: boolean;
  isMessageAppendRequired: boolean;
}

interface TeamsRepository {
  data: Record<string, Team>;
}

const initialState: TeamsRepository = {
  data: {},
};

export default function (
  state: TeamsRepository = initialState,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  action: { type: TeamType; payload: any },
): TeamsRepository {
  const { type, payload } = action;

  switch (type) {
    case MESSAGES_APPEND_STARTED: {
      const teams: Record<string, Team> = { ...state.data };

      const team: Team | undefined = teams[payload.id];

      teams[payload.id] = {
        ...teams[payload.id],
        isMessageAppendRequired: true,
        lastMassageId: payload.lastMassageId,
        lastMessages: payload.messages,
        messages: team
          ? team.messages.concat(payload.messages)
          : payload.messages,
      };

      return { data: teams };
    }

    case MESSAGES_APPEND_COMPLETED: {
      const teams: Record<string, Team> = { ...state.data };

      teams[payload.id] = {
        ...teams[payload.id],
        isMessageAppendRequired: false,
      };

      return { data: teams };
    }

    case MESSAGES_OVERWRITING_STARTED: {
      const teams: Record<string, Team> = { ...state.data };

      teams[payload.id] = {
        ...teams[payload.id],
        isMessagesOverwriteRequired: true,
        isMessageAppendRequired: false,
        lastMassageId: payload.lastMassageId,
        lastMessages: payload.messages,
        messages: payload.messages,
      };

      return { data: teams };
    }

    case MESSAGES_OVERWRITING_COMPLETED: {
      const teams: Record<string, Team> = { ...state.data };

      teams[payload.id] = {
        ...teams[payload.id],
        isMessagesOverwriteRequired: false,
      };

      return { data: teams };
    }

    case REMOVE_MESSAGE: {
      const teams: Record<string, Team> = { ...state.data };
      const team: Team | undefined = teams[payload.id];

      teams[payload.id] = {
        ...teams[payload.id],
        lastMassageId: team.lastMassageId,
        messages: team.messages.filter(
          (message) => message.id !== payload.messageId,
        ),
      };
      return { data: teams };
    }

    case CONTENT_ACTION_STARTED: {
      const teams: Record<string, Team> = { ...state.data };

      teams[payload.teamId] = {
        ...teams[payload.teamId],
        contentsActionInProgress:
          teams[payload.teamId] &&
          teams[payload.teamId].contentsActionInProgress
            ? teams[payload.teamId].contentsActionInProgress.concat([
                payload.contentId,
              ])
            : [payload.contentId],
      };

      return {
        data: teams,
      };
    }

    case CONTENT_ACTION_COMPLETED: {
      const teams: Record<string, Team> = { ...state.data };

      teams[payload.teamId] = {
        ...teams[payload.teamId],
        contentsActionInProgress: teams[
          payload.teamId
        ].contentsActionInProgress.filter(
          (contentId: number) => contentId !== payload.contentId,
        ),
      };

      return {
        data: teams,
      };
    }

    default:
      return state;
  }
}
export const getTeam = (state: RootState, id: string): Team =>
  state.teams.data[id];
export const lastMessageId = (
  state: RootState,
  id: string,
): string | undefined => state.teams.data?.[id]?.lastMassageId;
export const lastMessages = (state: RootState, id: string): Message[] =>
  state.teams.data?.[id]?.lastMessages ?? [];
export const teamMessages = (state: RootState, id: string): Message[] =>
  state.teams.data?.[id]?.messages ?? [];
export const contentsActionInProgress = (
  state: RootState,
  teamId: string,
): number[] => state.teams.data[teamId]?.contentsActionInProgress ?? [];

export const isMessagesOverwriteRequired = (
  state: RootState,
  teamId: string,
): boolean =>
  state.teams.data[teamId] &&
  state.teams.data[teamId].isMessagesOverwriteRequired;
export const isMessageAppendRequired = (
  state: RootState,
  teamId: string,
): boolean => state.teams.data[teamId]?.isMessageAppendRequired;
