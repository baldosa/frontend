import {
  InitialConfigurationType,
  PRE_CONFIGURATION_COMPLETE_STEP,
  PRE_CONFIGURATION_LOAD_DATA,
} from '../../types/types.ts';
import { userProfileStepId } from './initial/userProfileDetails.ts';
import { organizationStepId } from './initial/organizationDetails.ts';
import { invitationStepId } from './initial/invitationSection.ts';
import { RootState } from '../../store.ts';
import { userKey } from '../auth.ts';

interface PreConfiguration {
  wizardSteps: string[];
  stepsCompleted: string[];
  isCompleted: boolean;
  indexToDisplay: number;
  isLoadedCompletely: boolean;
}

const initialState: PreConfiguration = {
  wizardSteps: [],
  stepsCompleted: [],
  isCompleted: false,
  indexToDisplay: 1,
  isLoadedCompletely: false,
};

const preconfigurationWizardIsCompletedKey =
  'preconfigurationWizardIsCompleted';
const indexToDisplayKey = 'indexToDisplay';
const stepsCompletedKey = 'stepsCompleted';

const preconfigurationWizardIsCompletedFromStorage: string | null =
  localStorage.getItem(preconfigurationWizardIsCompletedKey);

const availableSteps: Record<string, string[]> = {
  ['Admin']: [userProfileStepId, organizationStepId, invitationStepId],
  ['User']: [userProfileStepId],
  ['OrgOwner']: [userProfileStepId, organizationStepId, invitationStepId],
};

const stepsToDisplayByRole = (roles: string[]): string[] => {
  return roles[0] in availableSteps ? availableSteps[roles[0]] : [];
};

const preconfigurationWizardCompleted = (condition: boolean): boolean => {
  if (condition) {
    localStorage.setItem(preconfigurationWizardIsCompletedKey, 'true');
  }
  return condition;
};

export default function (
  state: PreConfiguration = initialState,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  action: { type: InitialConfigurationType; payload: any },
): PreConfiguration {
  const { type, payload } = action;

  switch (type) {
    case PRE_CONFIGURATION_LOAD_DATA: {
      const userFromStorage: string | null = localStorage.getItem(userKey);
      const stepsCompletedFromStorage: string | null =
        localStorage.getItem(stepsCompletedKey);
      const indexToDisplayFromStorage: string | null =
        localStorage.getItem(indexToDisplayKey);

      if (stepsCompletedFromStorage == null) {
        localStorage.setItem(stepsCompletedKey, JSON.stringify([]));
      }

      const listOfStepIdToBeDisplayed: string[] = userFromStorage
        ? stepsToDisplayByRole(JSON.parse(userFromStorage).user.roles)
        : [];
      const listOfStepIdCompleted: string[] = stepsCompletedFromStorage
        ? JSON.parse(stepsCompletedFromStorage)
        : [];
      const indexStepToDisplay: number = indexToDisplayFromStorage
        ? JSON.parse(indexToDisplayFromStorage)
        : 1;
      const preconfigurationWizardIsCompleted: boolean =
        preconfigurationWizardIsCompletedFromStorage
          ? JSON.parse(preconfigurationWizardIsCompletedFromStorage)
          : listOfStepIdToBeDisplayed.length == listOfStepIdCompleted.length;

      return {
        ...state,
        wizardSteps: listOfStepIdToBeDisplayed,
        stepsCompleted: listOfStepIdCompleted,
        isCompleted: preconfigurationWizardIsCompleted,
        indexToDisplay: indexStepToDisplay,
        isLoadedCompletely: true,
      };
    }
    case PRE_CONFIGURATION_COMPLETE_STEP: {
      const stepsCompleted: string[] = state.stepsCompleted.slice();
      let firstIndexNotFound = state.indexToDisplay;
      let isCompleted = state.isCompleted;

      if (!stepsCompleted.includes(payload.stepId)) {
        stepsCompleted.push(payload.stepId);
        localStorage.setItem(stepsCompletedKey, JSON.stringify(stepsCompleted));

        for (let i = 0; i < state.wizardSteps.length; i++) {
          if (!stepsCompleted.includes(state.wizardSteps[i])) {
            firstIndexNotFound = i + 1;
            break;
          }
        }

        localStorage.setItem(
          indexToDisplayKey,
          JSON.stringify(firstIndexNotFound),
        );

        isCompleted = preconfigurationWizardCompleted(
          stepsCompleted.length === state.wizardSteps.length,
        );
      }

      return {
        ...state,
        stepsCompleted: stepsCompleted,
        indexToDisplay: firstIndexNotFound,
        isCompleted: isCompleted,
      };
    }
    default:
      return state;
  }
}

export const getStepsToCompleteInTheWizard = (state: RootState) =>
  state.preConfiguration.wizardSteps;
export const isCompleted = (state: RootState) =>
  state.preConfiguration.isCompleted;
export const getIndexStepToDisplay = (state: RootState) =>
  state.preConfiguration.indexToDisplay;
export const getIsLoadedCompletely = (state: RootState) =>
  state.preConfiguration.isLoadedCompletely;
