import {
  ADD_INVITATION,
  COMPLETED_INVITATION_SECTION,
  InitialConfigurationType,
  INVITATION_SECTION_NO_REDIRECTION_REQUIRED,
  REMOVE_INVITATION,
  UNCOMPLETED_INVITATION_SECTION,
} from '../../../types/types.ts';
import { RootState } from '../../../store.ts';
import { Invitation } from '../../../../pages/User/Configuration/Invitation/InvitationItem.tsx';

export const invitationStepId = 'wizard_invitation_step';

interface InvitationSection {
  data: {
    invitationsDetails: {
      invitations: Invitation[];
      generatedInvitationsTotal: number;
    };
  };
  isCompleted: boolean;
  requiresRedirection: boolean;
}

const initialState: InvitationSection = {
  data: {
    invitationsDetails: {
      invitations: [],
      generatedInvitationsTotal: 0,
    },
  },
  isCompleted: false,
  requiresRedirection: true,
};

export default function (
  state = initialState,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  action: { type: InitialConfigurationType; payload: any },
): InvitationSection {
  const { type, payload } = action;

  switch (type) {
    case ADD_INVITATION:
      return {
        ...state,
        data: {
          invitationsDetails: {
            invitations: [
              ...state.data.invitationsDetails.invitations,
              payload.invitation,
            ],
            generatedInvitationsTotal:
              state.data.invitationsDetails.generatedInvitationsTotal + 1,
          },
        },
      };
    case REMOVE_INVITATION:
      return {
        ...state,
        data: {
          invitationsDetails: {
            invitations: state.data.invitationsDetails.invitations.filter(
              (item) => item.id !== payload.id,
            ),
            generatedInvitationsTotal:
              state.data.invitationsDetails.generatedInvitationsTotal,
          },
        },
      };
    case COMPLETED_INVITATION_SECTION:
      return {
        ...state,
        data: {
          invitationsDetails: {
            invitations: [],
            generatedInvitationsTotal: 0,
          },
        },
        isCompleted: true,
      };
    case UNCOMPLETED_INVITATION_SECTION:
      return {
        ...state,
        data: {
          invitationsDetails: {
            invitations: [],
            generatedInvitationsTotal: 0,
          },
        },
        isCompleted: false,
      };
    case INVITATION_SECTION_NO_REDIRECTION_REQUIRED:
      return {
        ...state,
        requiresRedirection: false,
      };
    default:
      return state;
  }
}

export const getIsCompleted = (state: RootState) =>
  state.invitationSection.isCompleted;

export const getInvitationList = (state: RootState) =>
  state.invitationSection.data.invitationsDetails.invitations;

export const getInvitationListLength = (state: RootState) =>
  state.invitationSection.data.invitationsDetails.invitations.length;

export const getGeneratedInvitationsTotal = (state: RootState) =>
  state.invitationSection.data.invitationsDetails.generatedInvitationsTotal;

export const getRequiresRedirectionStatus = (state: RootState) =>
  state.invitationSection.requiresRedirection;
