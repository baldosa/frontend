import {
  InitialConfigurationType,
  USER_PROFILE_COMPLETED,
  USER_PROFILE_LOADED,
  USER_PROFILE_LOADED_COMPLETELY,
  USER_PROFILE_NO_REDIRECTION_REQUIRED,
} from '../../../types/types.ts';
import { RootState } from '../../../store.ts';

export const userProfileStepId = 'wizard_user_profile_step';

interface UserProfileDetails {
  data: {
    avatar: string | null;
    fullname: string | null;
    email: string | null;
  };
  isLoadedCompletely: boolean;
  requiresRedirection: boolean;
  isCompleted: boolean;
}

const initialState: UserProfileDetails = {
  data: { avatar: null, fullname: null, email: null },
  requiresRedirection: true,
  isLoadedCompletely: false,
  isCompleted: false,
};

export default function (
  state: UserProfileDetails = initialState,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  action: { type: InitialConfigurationType; payload: any },
): UserProfileDetails {
  const { type, payload } = action;

  switch (type) {
    case USER_PROFILE_LOADED:
      return {
        ...state,
        data: {
          avatar: payload.avatar ? payload.avatar : state.data.avatar,
          email: payload.email ? payload.email : state.data.email,
          fullname: payload.username ? payload.username : state.data.fullname,
        },
      };
    case USER_PROFILE_LOADED_COMPLETELY:
      return {
        ...state,
        isLoadedCompletely: true,
      };
    case USER_PROFILE_COMPLETED:
      return {
        ...state,
        isCompleted: true,
      };
    case USER_PROFILE_NO_REDIRECTION_REQUIRED:
      return {
        ...state,
        requiresRedirection: false,
      };
    default:
      return state;
  }
}

export const getFullName = (state: RootState) =>
  state.userProfile.data.fullname;
export const getUserEmail = (state: RootState) => state.userProfile.data.email;
export const getAvatar = (state: RootState) => state.userProfile.data.avatar;
export const getIsCompleted = (state: RootState) =>
  state.userProfile.isCompleted;
export const getIsLoadedCompletely = (state: RootState) =>
  state.userProfile.isLoadedCompletely;
export const getRequiresRedirectionStatus = (state: RootState) =>
  state.userProfile.requiresRedirection;
