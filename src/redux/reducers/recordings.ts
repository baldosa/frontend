import { RecordingInterface } from '../../interfaces';
import {
  RECORDING_INSERT,
  RECORDING_UPDATE,
  CLEAR_RECORDINGS,
  SET_ACTIVE_RECORDING_STATE,
  SET_ALLOW_BROWSER_RECORDING,
  RECORDING_COUNTER,
  SET_RECORDING_NAME,
  SET_EDITOR_OPTIONS_VISIBILITY,
  SET_AUDIO_RENDERING,
  CLEAR_AUDIO_RENDERING,
  SET_CONTEXT_AUDIO_RENDERING,
  SET_EDITOR_CONTEXT,
  SET_RECORDING_DURATION,
} from '../actions';
import { AudioStateProps } from '../../types';
import { RootState } from '../store.ts';

type initialStateProps = {
  recordings: RecordingInterface[];
  activeRecordingState: AudioStateProps;
  allowBrowserRecording: boolean;
  recordingCounter: number;
  recordingName: string;
  editorOptionsVisibility: boolean;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  audioRenderingData: any;
  audioRenderingType: string;
  audioRenderingContext: string;
  editorContext: string;
  recordingDuration: number;
};

const initialState: initialStateProps = {
  recordings: [],
  activeRecordingState: 'NONE',
  allowBrowserRecording: false,
  recordingCounter: 0,
  recordingName: '',
  editorOptionsVisibility: false,
  audioRenderingData: null,
  audioRenderingType: '',
  audioRenderingContext: '',
  editorContext: '',
  recordingDuration: 0,
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const reducer = (state = initialState, action: any) => {
  switch (action.type) {
    case RECORDING_COUNTER:
      return { ...state, recordingCounter: action.recordingCounter };
    case SET_EDITOR_OPTIONS_VISIBILITY:
      return {
        ...state,
        editorOptionsVisibility: action.editorOptionsVisibility,
      };
    case SET_RECORDING_NAME:
      return { ...state, recordingName: action.recordingName };
    case RECORDING_INSERT:
      return {
        ...state,
        recordings: state.recordings.concat(action.recording),
      };
    case CLEAR_RECORDINGS:
      return { ...state, recordings: [] };
    case SET_ACTIVE_RECORDING_STATE:
      return { ...state, activeRecordingState: action.activeRecordingState };
    case SET_ALLOW_BROWSER_RECORDING:
      return { ...state, allowBrowserRecording: action.allowBrowserRecording };
    case RECORDING_UPDATE:
      // eslint-disable-next-line no-case-declarations
      const newRecordings = state.recordings;
      newRecordings.map((item: RecordingInterface) => {
        let i = item;
        if (i.id === action.id) {
          i = action.recording;
        }
        return i;
      });
      return { ...state, recordings: newRecordings };
    case SET_AUDIO_RENDERING:
      return {
        ...state,
        audioRenderingData: action.data,
        audioRenderingType: action.audioType,
      };
    case CLEAR_AUDIO_RENDERING:
      return {
        ...state,
        audioRenderingData: null,
        audioRenderingType: '',
      };
    case SET_CONTEXT_AUDIO_RENDERING:
      return {
        ...state,
        audioRenderingContext: action.audioRenderingContext,
      };
    case SET_EDITOR_CONTEXT:
      return {
        ...state,
        editorContext: action.payload.editorContext,
      };
    case SET_RECORDING_DURATION:
      return {
        ...state,
        recordingDuration: action.recordingDuration,
      };
    default:
      return state;
  }
};

export default reducer;

export const getRecordings = (state: RootState) => state.recording.recordings;
export const getActiveRecordingState = (state: RootState) =>
  state.recording.activeRecordingState;
export const getRecordingCounter = (state: RootState) =>
  state.recording.recordingCounter;
export const getRecordingName = (state: RootState) =>
  state.recording.recordingName;
export const getEditorOptionsVisibility = (state: RootState) =>
  state.recording.editorOptionsVisibility;
export const getAudioRenderingData = (state: RootState) =>
  state.recording.audioRenderingData;
export const getAudioRenderingType = (state: RootState) =>
  state.recording.audioRenderingType;
export const getAudioRenderingContext = (state: RootState) =>
  state.recording.audioRenderingContext;
export const getEditorContext = (state: RootState) =>
  state.recording.editorContext;
export const getRecordingDuration = (state: RootState) =>
  state.recording.recordingDuration;
