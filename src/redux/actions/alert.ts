import { Dispatch } from 'react';
import { AnyAction } from '@reduxjs/toolkit';
import { ADD_ALERT, REMOVE_ALERT } from '../types/types.ts';

export const addAlert =
  (
    variant: 'default' | 'success' | 'danger' | 'warning' | 'info' | undefined,
    message: string,
  ) =>
  (dispatch: Dispatch<AnyAction>): void => {
    dispatch({
      type: ADD_ALERT,
      payload: {
        variant: variant,
        message: message,
      },
    });
  };

export const removeAlert =
  (id: number) =>
  (dispatch: Dispatch<AnyAction>): void => {
    dispatch({
      type: REMOVE_ALERT,
      payload: { id: id },
    });
  };
