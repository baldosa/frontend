import {
  RECORDING_INSERT,
  RECORDING_UPDATE,
  CLEAR_RECORDINGS,
  SET_ACTIVE_RECORDING_STATE,
  SET_ALLOW_BROWSER_RECORDING,
  RECORDING_COUNTER,
  SET_RECORDING_NAME,
  SET_EDITOR_OPTIONS_VISIBILITY,
  SET_AUDIO_RENDERING,
  CLEAR_AUDIO_RENDERING,
  SET_CONTEXT_AUDIO_RENDERING,
  SET_EDITOR_CONTEXT,
  SET_RECORDING_DURATION,
} from '../../actions';
import { RecordingInterface } from '../../../interfaces';
import { AnyAction } from '@reduxjs/toolkit';
import { Dispatch } from 'react';

export const setRecordingCounter = (recordingCounter: number) => ({
  type: RECORDING_COUNTER,
  recordingCounter,
});

export const recordingCreate = (recording: RecordingInterface) => ({
  type: RECORDING_INSERT,
  recording,
});

export const recordingUpdate = (id: string, recording: RecordingInterface) => ({
  type: RECORDING_UPDATE,
  id,
  recording,
});

export const updateRecordingState = (activeRecordingState: string) => ({
  type: SET_ACTIVE_RECORDING_STATE,
  activeRecordingState,
});

export const updatePermissionBrowserRecording = ({
  allowBrowserRecording,
}: {
  allowBrowserRecording: string;
}) => ({
  type: SET_ALLOW_BROWSER_RECORDING,
  allowBrowserRecording,
});

export const clearRecording = () => ({
  type: CLEAR_RECORDINGS,
});

export const setRecordingName = (recordingName: string) => ({
  type: SET_RECORDING_NAME,
  recordingName,
});

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const setAudioRendering = (audioType: string, data: any) => ({
  type: SET_AUDIO_RENDERING,
  audioType,
  data,
});

export const clearAudioRendering = () => ({
  type: CLEAR_AUDIO_RENDERING,
});

export const setEditorOptionsVisibility = (
  editorOptionsVisibility: boolean,
) => ({
  type: SET_EDITOR_OPTIONS_VISIBILITY,
  editorOptionsVisibility,
});

export const setAudioRenderingContext = (audioRenderingContext: string) => ({
  type: SET_CONTEXT_AUDIO_RENDERING,
  audioRenderingContext,
});

export const setEditorContext =
  (editorContext: string | undefined) =>
  (dispatch: Dispatch<AnyAction>): void => {
    dispatch({
      type: SET_EDITOR_CONTEXT,
      payload: { editorContext: editorContext },
    });
  };

export const setRecordingDuration = (recordingDuration: number) => ({
  type: SET_RECORDING_DURATION,
  recordingDuration,
});
