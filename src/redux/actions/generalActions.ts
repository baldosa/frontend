import { AnyAction } from '@reduxjs/toolkit';
import { Dispatch } from 'react';
import {
  ADD_BLOCKING_VALIDATION_REQUEST,
  ADD_NON_BLOCKING_VALIDATION_REQUEST,
  ENDING_AWAITING_RESPONSE,
  INITIATING_AWAITING_RESPONSE,
  RESET_VALIDATION_REQUESTS,
  RESOLVE_BLOCKING_VALIDATION_REQUEST,
  RESOLVE_NON_BLOCKING_VALIDATION_REQUEST,
  SET_HEADER_DESCRIPTION,
  SET_HEADER_NAME,
  SET_HEADER_NAVIGATION_DESTINATION,
} from '../types/types.ts';
import {
  GENERAL,
  getValidationKey,
} from '../../components/Auth/ValidationRules.ts';

export const initiateAction =
  () =>
  (dispatch: Dispatch<AnyAction>): void => {
    dispatch({
      type: INITIATING_AWAITING_RESPONSE,
    });
  };

export const endAction =
  () =>
  (dispatch: Dispatch<AnyAction>): void => {
    dispatch({
      type: ENDING_AWAITING_RESPONSE,
    });
  };

/* eslint-disable @typescript-eslint/no-explicit-any */
export const defineValidationRequestLocation =
  (error: any) =>
  (dispatch: Dispatch<AnyAction>): void => {
    const errorCode =
      error.response && error.response.data.error_code
        ? error.response.data.error_code
        : 'ERROR_NOT_REGISTERED';
    const statusCode = error.code ? error.code : undefined;
    const fieldName: string = getValidationKey(statusCode, errorCode);
    dispatch({
      type:
        fieldName == GENERAL
          ? ADD_NON_BLOCKING_VALIDATION_REQUEST
          : ADD_BLOCKING_VALIDATION_REQUEST,
      payload: {
        field_names: fieldName,
        status_code: statusCode,
        error_code: errorCode,
        variant: 'error',
        value: '',
      },
    });
  };
/* eslint-enable @typescript-eslint/no-explicit-any */

export const addBlockingValidationRequest =
  (
    fieldNames: string[],
    status_code: string,
    variant: 'default' | 'success' | 'warning' | 'error' | undefined,
    value?: string | undefined,
  ) =>
  (dispatch: Dispatch<AnyAction>): void => {
    dispatch({
      type: ADD_BLOCKING_VALIDATION_REQUEST,
      payload: {
        field_names: fieldNames,
        status_code: status_code,
        variant: variant,
        value: value,
      },
    });
  };

export const updateBlockingValidationRequest =
  (
    fieldNames: string[],
    status_code: string,
    error_code: string | undefined,
    variant: 'default' | 'success' | 'warning' | 'error' | undefined,
    value?: string | undefined,
  ) =>
  (dispatch: Dispatch<AnyAction>): void => {
    dispatch({
      type: ADD_BLOCKING_VALIDATION_REQUEST,
      payload: {
        field_names: fieldNames,
        status_code: status_code,
        error_code: error_code,
        variant: variant,
        value: value,
      },
    });
  };

export const resolveBlockingValidationRequest =
  (fieldNames: string[]) =>
  (dispatch: Dispatch<AnyAction>): void => {
    dispatch({
      type: RESOLVE_BLOCKING_VALIDATION_REQUEST,
      payload: { field_names: fieldNames },
    });
  };

export const addNonBlockingValidationRequest =
  (
    fieldNames: string[],
    status_code: string,
    variant: 'default' | 'success' | 'warning' | 'error' | undefined,
    value?: string | undefined,
  ) =>
  (dispatch: Dispatch<AnyAction>): void => {
    dispatch({
      type: ADD_NON_BLOCKING_VALIDATION_REQUEST,
      payload: {
        field_names: fieldNames,
        status_code: status_code,
        variant: variant,
        value: value,
      },
    });
  };

export const updateNonBlockingValidationRequest =
  (
    fieldNames: string[],
    status_code: string,
    error_code: string | undefined,
    variant: 'default' | 'success' | 'warning' | 'error' | undefined,
    value?: string | undefined,
  ) =>
  (dispatch: Dispatch<AnyAction>): void => {
    dispatch({
      type: ADD_NON_BLOCKING_VALIDATION_REQUEST,
      payload: {
        field_names: fieldNames,
        status_code: status_code,
        error_code: error_code,
        variant: variant,
        value: value,
      },
    });
  };

export const resolveNonBlockingValidationRequest =
  (fieldNames: string[]) =>
  (dispatch: Dispatch<AnyAction>): void => {
    dispatch({
      type: RESOLVE_NON_BLOCKING_VALIDATION_REQUEST,
      payload: { field_names: fieldNames },
    });
  };

export const resetValidationRequests =
  () =>
  (dispatch: Dispatch<AnyAction>): void => {
    dispatch({
      type: RESET_VALIDATION_REQUESTS,
    });
  };

export const setHeaderName =
  (headerTeamName: string | undefined) =>
  (dispatch: Dispatch<AnyAction>): void => {
    dispatch({
      type: SET_HEADER_NAME,
      payload: { headerTeamName: headerTeamName },
    });
  };

export const setHeaderDescription =
  (headerDescription: string) =>
  (dispatch: Dispatch<AnyAction>): void => {
    dispatch({
      type: SET_HEADER_DESCRIPTION,
      payload: { headerDescription: headerDescription },
    });
  };

export const setHeaderNavigationDestination =
  (headerNavigationDestination: string) =>
  (dispatch: Dispatch<AnyAction>): void => {
    dispatch({
      type: SET_HEADER_NAVIGATION_DESTINATION,
      payload: { headerNavigationDestination: headerNavigationDestination },
    });
  };
