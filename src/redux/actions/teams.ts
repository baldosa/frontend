import { Dispatch } from 'react';
import { AnyAction } from '@reduxjs/toolkit';
import {
  MESSAGES_APPEND_STARTED,
  MESSAGES_APPEND_COMPLETED,
  MESSAGES_OVERWRITING_STARTED,
  MESSAGES_OVERWRITING_COMPLETED,
  REMOVE_MESSAGE,
  CONTENT_ACTION_STARTED,
  CONTENT_ACTION_COMPLETED,
} from '../types/types.ts';

export const messagesAppendStarted =
  (
    teamId: string,
    lastMassageId: string,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    messages: { [name: string]: any }[],
  ) =>
  (dispatch: Dispatch<AnyAction>): void => {
    dispatch({
      type: MESSAGES_APPEND_STARTED,
      payload: {
        id: teamId,
        lastMassageId: lastMassageId,
        messages: messages,
      },
    });
  };

export const messagesAppendCompleted =
  (teamId: string) =>
  (dispatch: Dispatch<AnyAction>): void => {
    dispatch({
      type: MESSAGES_APPEND_COMPLETED,
      payload: {
        id: teamId,
      },
    });
  };

export const messagesOverwritingStarted =
  (
    teamId: string,
    lastMassageId: string,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    messages: { [name: string]: any }[],
  ) =>
  (dispatch: Dispatch<AnyAction>): void => {
    dispatch({
      type: MESSAGES_OVERWRITING_STARTED,
      payload: {
        id: teamId,
        lastMassageId: lastMassageId,
        messages: messages,
      },
    });
  };

export const messagesOverwritingCompleted =
  (teamId: string) =>
  (dispatch: Dispatch<AnyAction>): void => {
    dispatch({
      type: MESSAGES_OVERWRITING_COMPLETED,
      payload: {
        id: teamId,
      },
    });
  };

export const removeMessage =
  (teamId: string, messageId: number) =>
  (dispatch: Dispatch<AnyAction>): void => {
    dispatch({
      type: REMOVE_MESSAGE,
      payload: {
        id: teamId,
        messageId: messageId,
      },
    });
  };

export const initiateActionForContent =
  (teamId: string, contentId: number) =>
  (dispatch: Dispatch<AnyAction>): void => {
    dispatch({
      type: CONTENT_ACTION_STARTED,
      payload: {
        teamId: teamId,
        contentId: contentId,
      },
    });
  };

export const completeActionForContent =
  (teamId: string, contentId: number) =>
  (dispatch: Dispatch<AnyAction>): void => {
    dispatch({
      type: CONTENT_ACTION_COMPLETED,
      payload: {
        teamId: teamId,
        contentId: contentId,
      },
    });
  };
