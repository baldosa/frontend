import {
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  REGISTER_TAC_CONFIRM_ACCEPTANCE,
  REGISTER_SUCCESS,
  REGISTER_FAIL,
  LOGOUT,
  UPDATE_ACCESS_TOKEN,
  REGISTER_TAC_CANCEL_ACCEPTANCE,
  SAVE_AUTH_DATA,
  UPDATE_SERVER_DATA,
  PASSWORD_RESET_SUCCESS,
  PASSWORD_RESET_FAIL,
  PASSWORD_RESET_CONFIRM_SUCCESS,
  PASSWORD_RESET_CONFIRM_FAIL,
  SAVE_PREVIOUS_AUTH_URL
} from '../types/types.ts';
import { Paths } from '../../api/utilities/Definitions';
import { Dispatch } from 'react';
import { AnyAction } from '@reduxjs/toolkit';
import { getTokenAndUID } from '../../utils/logics/authUtils.ts';
import {
  authLogin,
  authSignup, authPasswordReset,
  authPasswordResetConfirm, authLogout,
} from '../../service/authentication.ts';
import { TFunction } from 'i18next';
import { NavigateFunction } from 'react-router-dom';
import { addAlert } from './alert.ts';
import { loginFullPath } from '../../routes.tsx';
import OpenAPIClientAxios from 'openapi-client-axios';
import { AxiosResponse } from 'axios';

export const login = (
  username: string,
  password: string,
) => async (dispatch: Dispatch<AnyAction>, client: OpenAPIClientAxios) => {

  return await authLogin(username, password)(client)
      .then((response: AxiosResponse) => {
        dispatch({ type: LOGIN_SUCCESS, payload: { user: response.data } });
      })
      .catch((error) => {
        dispatch({ type: LOGIN_FAIL });
        throw error;
      });
};

export const signup = (
  username: string,
  password: string,
  parameters: Paths.ConfirmInvitation.PathParameters,
) => async (dispatch: Dispatch<AnyAction>, client: OpenAPIClientAxios) => {

  return await authSignup(username, password, parameters)(client)
    .then(() => {
      dispatch({ type: REGISTER_SUCCESS });
    })
    .catch((error) => {
      dispatch({ type: REGISTER_FAIL });
      console.error(error);
    });
};

export const logout = (
  accessToken: string,
) => async (
  t: TFunction,
  navigate: NavigateFunction,
  dispatch: Dispatch<AnyAction>,
  client: OpenAPIClientAxios
) => {

  return await authLogout(accessToken)(client)
    .catch((error) => {
      console.error(error);
    })
    .finally(() => {
      dispatch({ type: LOGOUT });
      navigate(loginFullPath(), { replace: true });
      addAlert('success', t('logout.alert.success'))(dispatch);
    })
};

export const passwordReset = (
  email: string
) => async (dispatch: Dispatch<AnyAction>, client: OpenAPIClientAxios) => {
  return await authPasswordReset(email)(client)
    .then(() => {
      dispatch({ type: PASSWORD_RESET_SUCCESS });
    })
    .catch((error) => {
      dispatch({ type: PASSWORD_RESET_FAIL });
      console.error(error);
    });
};

export const passwordResetConfirm = (
  paramsToken: string,
  newPassword: string,
  newPasswordConfirm: string,
) => async (dispatch: Dispatch<AnyAction>, client: OpenAPIClientAxios) => {
  const { token, uid } = getTokenAndUID(paramsToken);

  return authPasswordResetConfirm(
    uid,
    token,
    newPassword,
    newPasswordConfirm)(client)
    .then(() => {
      dispatch({ type: PASSWORD_RESET_CONFIRM_SUCCESS });
    })
    .catch((error) => {
      dispatch({ type: PASSWORD_RESET_CONFIRM_FAIL });
      console.error(error);
    });
};

export const saveUserData = (
  username: string | null,
  password: string | null
) => (dispatch: Dispatch<AnyAction>): void => {
    dispatch({
      type: SAVE_AUTH_DATA,
      payload: { user: { user: { email: username, password: password } } },
    });
};

export const confirmTAC = () => (dispatch: Dispatch<AnyAction>): void => {
  dispatch({ type: REGISTER_TAC_CONFIRM_ACCEPTANCE });
};

export const cancelTAC = () => (dispatch: Dispatch<AnyAction>): void => {
  dispatch({ type: REGISTER_TAC_CANCEL_ACCEPTANCE });
};

export const updateAccessToken = (
  accessToken: string | undefined
) => (dispatch: Dispatch<AnyAction>): void => {
   dispatch({ type: UPDATE_ACCESS_TOKEN, payload: { access: accessToken } });
};

export const updateServerData = (
  serverId: number,
  serverName: string,
  serverUrl: string
) =>
  (dispatch: Dispatch<AnyAction>): void => {
    dispatch({
      type: UPDATE_SERVER_DATA,
      payload: {
        serverId: serverId,
        serverName: serverName,
        serverUrl: serverUrl
      },
    });
  };

export const savePreviousAuthUrl = (url: string) => (dispatch: Dispatch<AnyAction>): void => {
  {
    dispatch({
      type: SAVE_PREVIOUS_AUTH_URL,
      payload: {
        previousUrl: url
      }
    })
  }
};