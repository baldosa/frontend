import { Dispatch } from 'react';
import { AnyAction } from '@reduxjs/toolkit';
import {
  ORGANIZATION_DETAILS_COMPLETED,
  ORGANIZATION_DETAILS_FAILED_LOAD,
  ORGANIZATION_DETAILS_LOADED,
  ORGANIZATION_DETAILS_LOADED_COMPLETELY,
  ORGANIZATION_DETAILS_NO_REDIRECTION_REQUIRED, ORGANIZATION_UPDATE_LOGO,
} from '../../../types/types.ts';

export const changeOrganizationDetailsName =
  (name: string | undefined) =>
  (dispatch: Dispatch<AnyAction>): void => {
    dispatch({
      type: ORGANIZATION_DETAILS_LOADED,
      payload: {
        name: name,
      },
    });
  };

export const changeOrganizationDetailsEmail =
  (email: string | undefined) =>
  (dispatch: Dispatch<AnyAction>): void => {
    dispatch({
      type: ORGANIZATION_DETAILS_LOADED,
      payload: {
        email: email,
      },
    });
  };

export const changeOrganizationLogo =
  (logo: string | null) =>
  (dispatch: Dispatch<AnyAction>): void => {
    dispatch({
      type: ORGANIZATION_UPDATE_LOGO,
      payload: {
        logo: logo,
      },
    });
  };

export const changeOrganizationDetailsCountry =
  (country: { key: string | number; value: string } | undefined) =>
  (dispatch: Dispatch<AnyAction>): void => {
    dispatch({
      type: ORGANIZATION_DETAILS_LOADED,
      payload: {
        country: country,
      },
    });
  };

export const changeOrganizationDetailsLanguage =
  (language: { key: string | number; value: string } | undefined) =>
  (dispatch: Dispatch<AnyAction>): void => {
    dispatch({
      type: ORGANIZATION_DETAILS_LOADED,
      payload: {
        language: language,
      },
    });
  };

export const organizationDetailsLoadedCompletely =
  () =>
  (dispatch: Dispatch<AnyAction>): void => {
    dispatch({
      type: ORGANIZATION_DETAILS_LOADED_COMPLETELY,
    });
  };

export const organizationDetailsFailedLoad =
  () =>
  (dispatch: Dispatch<AnyAction>): void => {
    dispatch({
      type: ORGANIZATION_DETAILS_FAILED_LOAD,
    });
  };

export const organizationDetailsCompleted =
  () =>
  (dispatch: Dispatch<AnyAction>): void => {
    dispatch({
      type: ORGANIZATION_DETAILS_COMPLETED,
    });
  };

export const endRedirection =
  () =>
  (dispatch: Dispatch<AnyAction>): void => {
    dispatch({
      type: ORGANIZATION_DETAILS_NO_REDIRECTION_REQUIRED,
    });
  };
