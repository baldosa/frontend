import definition from './utilities/schema-runtime.json';
import OpenAPIClientAxios from 'openapi-client-axios';

export const openApiVersion = definition.info.version

export const buildAuthToken = (token: string): string => {
  return 'Bearer ' + token
};

export const generateOpenAPIClient = (serverBaseUrl: string): OpenAPIClientAxios => {
  return new OpenAPIClientAxios({
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    definition,
    axiosConfigDefaults: {
      baseURL: serverBaseUrl,
      headers: { accept: 'application/json' },
    },
  });
}