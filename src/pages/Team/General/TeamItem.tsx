import * as React from 'react';
import {
  Avatar,
  Badge,
  Card,
  CardBody,
  Icon,
  ListItem,
  Skeleton,
} from '@patternfly/react-core';
import logo from '../../../assets/pages/Teams/team_logo.png';
import FolderIcon from '@patternfly/react-icons/dist/esm/icons/folder-icon';
import { useTranslation } from 'react-i18next';
import { useNavigate } from 'react-router-dom';
import { teamChatFullPath, teamSharedFilesFullPath } from '../../../routes.tsx';

export const TeamItem: React.FunctionComponent<{
  id: string;
  organizationId?: number;
  teamId?: number;
  name?: string;
  lastMessageTimestamp?: number;
  amountFiles?: number;
  isLoading: boolean;
}> = (props) => {
  const { t, i18n } = useTranslation();
  const navigate = useNavigate();

  return (
    <ListItem
      className={'list-item with-separation'}
      key={props.id}
      id={props.id}
    >
      <Card
        isFlat
        className={'card-items team-preview'}
        onClick={(): void => {
          // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
          navigate(teamChatFullPath(props.teamId!));
        }}
      >
        {props.isLoading ? (
          <Skeleton height="100%" width="100%" />
        ) : (
          <CardBody className={'body'}>
            <div style={{ display: 'flex' }}>
              <Avatar
                style={{ height: '24px', width: '24px' }}
                src={logo}
                alt="avatar"
                size="lg"
              />
              <p
                style={{
                  color: 'var(--violet-2-color)',
                  paddingLeft: '10px',
                  whiteSpace: 'nowrap',
                  overflow: 'hidden',
                  textOverflow: 'ellipsis',
                }}
              >
                <b>{props.name}</b>
              </p>
            </div>
            <div>
              <p style={{ color: 'var(--grey-color)', fontSize: '10px' }}>
                {t('structure_menu.item.date_of_last_message') +
                  new Date(
                    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                    props.lastMessageTimestamp! * 1000,
                  ).toLocaleDateString(i18n.language.replace('_', '-')) +
                  ' ' +
                  new Date(
                    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                    props.lastMessageTimestamp! * 1000,
                  ).toLocaleTimeString(i18n.language.replace('_', '-'))}
              </p>
            </div>
          </CardBody>
        )}
      </Card>
      <Card
        isFlat
        className={'card-items files-preview'}
        onClick={(): void => {
          navigate(
            // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
            teamSharedFilesFullPath(props.teamId!),
          );
        }}
      >
        {props.isLoading ? (
          <Skeleton height="100%" width="100%" />
        ) : (
          <CardBody className={'body'}>
            <Icon>
              <FolderIcon color={'var(--grey-color)'} />
            </Icon>
            <Badge style={{ display: 'none' }}>{props.amountFiles}</Badge>
          </CardBody>
        )}
      </Card>
    </ListItem>
  );
};
