import {
  Button,
  Dropdown,
  DropdownItem,
  DropdownPosition,
  KebabToggle,
  PageSection,
  TextInputGroup,
} from '@patternfly/react-core';
import { TextArea } from '@patternfly/react-core';
import {
  Dispatch,
  FunctionComponent,
  ReactElement,
  useEffect,
  useRef,
  useState,
} from 'react';
import { Message } from './Message/Message.tsx';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { getAccessToken } from '../../../redux/reducers/auth.ts';
import { AnyAction } from '@reduxjs/toolkit';
import {
  getTeam,
  isMessageAppendRequired,
  isMessagesOverwriteRequired,
  lastMessageId,
  lastMessages,
  Team,
  teamMessages,
} from '../../../redux/reducers/teams.ts';
import { useNavigate, useParams } from 'react-router-dom';
import {
  calculateFullDateTime,
  calculateSimplifiedDateTime,
} from '../../../utils/teamUtils.ts';
import {
  messagesOverwritingStarted,
  removeMessage,
  messagesAppendStarted,
  messagesAppendCompleted,
  messagesOverwritingCompleted,
} from '../../../redux/actions/teams.ts';
import { RootState } from '../../../redux/store.ts';
import {
  endAction,
  initiateAction,
  setHeaderDescription,
  setHeaderName,
} from '../../../redux/actions/generalActions.ts';
import { AttachFile } from '../../../assets/icon/chat/AttachFile.tsx';
import { SendMessage } from '../../../assets/icon/chat/SendMessage.tsx';
import { UploadFile } from '../../../components/Wizard/UploadFile.tsx';
import { teamSharedFilesFullPath } from '../../../routes.tsx';
import { SystemMessage } from './Message/SystemMessage.tsx';
import { getActionState } from '../../../redux/reducers/generalActions.ts';
import { getContentTypes } from '../../../utils/logics/fileUtils.ts';
import { useContextClient } from '../../AppLayout/Contexts.tsx';
import { getTeamDetails, getTeamMessages, sendMessageToTeam, uploadFileToTeam } from '../../../service/team.ts';

export const TeamChatDropdown = () => {
  const [isOpen, setIsOpen] = useState(false);
  const { teamId } = useParams();
  const navigate = useNavigate();
  const { t } = useTranslation();

  const dropdownItems = [
    <DropdownItem
      component={'button'}
      /* eslint-disable-next-line @typescript-eslint/no-non-null-assertion */
      onClick={() => navigate(teamSharedFilesFullPath(parseInt(teamId!)))}
      children={t('structure_menu.teams.chat.dropdown.view_files')}
    />,
  ];

  return (
    <Dropdown
      isOpen={isOpen}
      isPlain={true}
      position={DropdownPosition.right}
      menuAppendTo={'inline'}
      toggle={
        <KebabToggle
          id="toggle-kebab"
          onToggle={(value: boolean) => setIsOpen(value)}
        />
      }
      dropdownItems={dropdownItems}
    />
  );
};

export const TeamChat: FunctionComponent = () => {
  const [getLastRemovedMessageId, setLastRemovedMessageId] = useState<number | undefined>(undefined);
  const [getAcceptedContentTypes, setAcceptedContentTypes] = useState<string | undefined>(undefined);
  const [getViewingStatusFileUpload, setUploadButtonPressed] = useState<boolean>(false);
  const [getConversationToken, setConversationToken] = useState<string>('');
  const [getUploadErrors, setUploadErrors] = useState<boolean>(false);
  const [getMessages, setMessages] = useState<ReactElement[]>([]);
  const [getMessageDraft, setMessageDraft] = useState<string>('');
  const [getFiles, setFiles] = useState<File[]>([]);

  const messagesRef = useRef<HTMLOListElement>(null);
  const dispatch: Dispatch<AnyAction> = useDispatch();

  const { getOpenAPIClient } = useContextClient();
  const { t, i18n } = useTranslation();
  const { teamId } = useParams();

  const userToken = useSelector(getAccessToken);
  const isActionInProgress = useSelector(getActionState);

  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  const team: Team = useSelector((state: RootState) => getTeam(state, teamId!));

  const allMessagesFromStore = useSelector((state: RootState) =>
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    teamMessages(state, teamId!),
  );
  const lastMessagesFromStore = useSelector((state: RootState) =>
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    lastMessages(state, teamId!),
  );
  const lastMessageIdFromStore = useSelector((state: RootState) =>
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    lastMessageId(state, teamId!),
  );
  const requiredOverwriteMessages = useSelector((state: RootState) =>
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    isMessagesOverwriteRequired(state, teamId!),
  );
  const requiredConcatenateMessages = useSelector((state: RootState) =>
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    isMessageAppendRequired(state, teamId!),
  );

  useEffect(() => {
    if (getLastRemovedMessageId) {
      setMessages(
        getMessages.filter(
          (message) => message.props.id !== getLastRemovedMessageId,
        ),
      );
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [getLastRemovedMessageId]);

  useEffect((): void => {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    getTeamDetails(userToken, parseInt(teamId!))(getOpenAPIClient!)
      .then((response): void => {
        setHeaderName(
          response.data.is_personal_workspace
            ? t('structure_menu.teams.my_space.header').toString()
            : t('structure_menu.teams.chat.header', {
                group: response.data.organization_name,
              }).toString(),
        )(dispatch);
        setHeaderDescription(
          response.data.is_personal_workspace
            ? t('structure_menu.teams.my_space.description')
            : t('structure_menu.teams.chat.description', {
                amount: response.data.members_count,
              }).toString(),
        )(dispatch);
        setConversationToken(response.data.nc_conversation_token);
      })
      .catch((): void => {
        setHeaderName(t('structure_menu.teams.chat.title_error').toString())(
          dispatch,
        );
      });

    if (team) {
      generateMessageStructure(allMessagesFromStore, false, false);
    } else {
      generateMessageStructure([], false, true);
    }

    updateAcceptedContentTypesState(userToken);

    endAction()(dispatch);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (messagesRef.current) {
      messagesRef.current.scrollTop = messagesRef.current.scrollHeight;
    }
  }, [getMessages]);

  useEffect(() => {
    const intervalToGetNewMessages = setInterval(() => {
      if (teamId && getConversationToken && !requiredConcatenateMessages) {
        generateMessages(
          teamId,
          getConversationToken,
          lastMessageIdFromStore,
          false,
        );
      }
    }, 4000);

    const intervalToUpdateMessagesCompletely = setInterval(() => {
      if (teamId && getConversationToken && !requiredOverwriteMessages) {
        generateMessages(teamId, getConversationToken, undefined, true);
      }
    }, 60000);

    const getContentTypeInterval = setInterval(() => {
      updateAcceptedContentTypesState(userToken);
    }, 30000);

    return (): void => {
      clearInterval(intervalToGetNewMessages);
      clearInterval(getContentTypeInterval);
      clearInterval(intervalToUpdateMessagesCompletely);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    getConversationToken,
    lastMessageIdFromStore,
    requiredOverwriteMessages,
    requiredConcatenateMessages,
  ]);

  useEffect(() => {
    if (teamId && requiredOverwriteMessages) {
      generateMessageStructure(lastMessagesFromStore, false, false);
      messagesOverwritingCompleted(teamId)(dispatch);
    }
    if (teamId && requiredConcatenateMessages) {
      generateMessageStructure(lastMessagesFromStore, true, false);
      messagesAppendCompleted(teamId)(dispatch);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [requiredOverwriteMessages, requiredConcatenateMessages]);

  const generateMessage = (
    message: Message,
    language: string,
  ): ReactElement => {
    if (message.messageType === 'system') {
      return (
        <SystemMessage
          key={`message-${message.id}`}
          id={message.id}
          fullDate={calculateFullDateTime(message.timestamp, language)}
          simplifiedDateTime={calculateSimplifiedDateTime(
            message.timestamp,
            language,
          )}
          message={message.message}
          messageParameters={message.messageParameters}
        />
      );
    } else {
      return (
        <Message
          key={`message-${message.id}`}
          id={message.id}
          teamOrigin={teamId}
          ownerId={message.actorId}
          fullDate={calculateFullDateTime(message.timestamp, language)}
          simplifiedDateTime={calculateSimplifiedDateTime(
            message.timestamp,
            language,
          )}
          message={message.message}
          messageParameters={message.messageParameters}
          name={message.actorDisplayName}
          isLoading={false}
          delete={deleteFile}
        />
      );
    }
  };

  const generateMessages = async (
    teamId: string,
    conversationToken: string,
    lastMessageId: string | undefined,
    refreshTeam: boolean,
  ): Promise<void> => {
    await getTeamMessages(userToken, conversationToken, lastMessageId)
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    (getOpenAPIClient!)
      .then((response): void => {
        if (response.data.messages && response.data.messages.length) {
          if (refreshTeam || !allMessagesFromStore.length) {
            messagesOverwritingStarted(
              teamId,
              response.data.last_given,
              response.data.messages,
            )(dispatch);
          }

          if (lastMessageId && lastMessageId !== response.data.last_given) {
            messagesAppendStarted(
              teamId,
              response.data.last_given,
              response.data.messages,
            )(dispatch);
          }
        }
      })
      .catch((error): void => {
        console.error(error);
      });
  };

  const generateMessageStructure = (
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    newMessages: any[],
    concatMessages: boolean,
    skeletonMessages: boolean,
  ): void => {
    if (skeletonMessages) {
      const skeletons: ReactElement[] = [...Array(4)].map((_, index) => {
        return (
          <Message
            key={`message-${index}`}
            id={index}
            isLoading={true}
            delete={deleteFile}
          />
        );
      });
      setMessages(skeletons);
    } else {
      const messages: ReactElement[] = newMessages.map(
        (message: Message) => generateMessage(message, i18n.language),
      );
      setMessages((concatMessages ? getMessages : []).concat(messages));
    }
  };

  const sendMessageButtonIsDisabled = (): boolean => {
    return (
      (getFiles.length === 0 && getMessageDraft.length === 0) ||
      isActionInProgress
    );
  };

  const isUploadButtonDisabled = (): boolean => {
    return getFiles.length >= 1 || getAcceptedContentTypes === undefined;
  };

  const sendMessage = (teamId: string): void => {
    const message = {
      conversation_token: getConversationToken,
      message: getMessageDraft,
    };

    if (getMessageDraft.length > 0) {
      initiateAction()(dispatch);
      sendMessageToTeam(userToken, getConversationToken, message)
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      (getOpenAPIClient!)
        .then((): void => {
          setMessageDraft('');
        })
        .catch((error): void => {
          console.error(error);
        })
        .finally(() => {
          endAction()(dispatch);
        });
    }

    if (getFiles.length > 0) {
      getFiles.forEach((file: File): void => {
        const data = new FormData();
        data.append('file', file);
        data.append('filename', file.name);
        data.append('conversation_token', teamId);

        initiateAction()(dispatch);

        uploadFileToTeam(userToken, getConversationToken, data)
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        (getOpenAPIClient!)
          .then((): void => {
            setFiles([]);
            setUploadErrors(false);
          })
          .catch((): void => {
            setUploadErrors(true);
          })
          .finally(() => {
            endAction()(dispatch);
          });
      });
    }
  };

  const displayFileUploadView = (): void => {
    setUploadButtonPressed(true);
  };

  const deleteFile = (messageId: number) => {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    removeMessage(teamId!, messageId)(dispatch);
    setLastRemovedMessageId(messageId);
  };

  const updateAcceptedContentTypesState = (userToken: string) => {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    getContentTypes(userToken)(getOpenAPIClient!)
      .then((contentTypes: string) => {
        setAcceptedContentTypes(contentTypes);
      })
      .catch(() => {
        setAcceptedContentTypes(undefined);
      });
  };

  return (
    <PageSection
      className={'message-section'}
      key={`team-page-section-${teamId}`}
    >
      <div
        key={`team-menu-${teamId}`}
        className={'structure-menu'}
        style={{
          zIndex: 1,
          display: 'flex',
          flexDirection: 'column',
          overflowY: 'scroll',
        }}
      >
        <ol
          style={{ padding: '15px', overflowY: 'scroll' }}
          ref={messagesRef}
          children={getMessages}
        />
      </div>
      <TextInputGroup className={'text-input-chat'}>
        <UploadFile
          setUploadButtonPressed={setUploadButtonPressed}
          uploadButtonPressed={getViewingStatusFileUpload}
          acceptedContentTypes={getAcceptedContentTypes}
          clearStatus={getFiles.length === 0}
          setUploadedFiles={setFiles}
          hasErrors={getUploadErrors}
        />
        <TextArea
          type={'text'}
          key={'input-text-chat'}
          placeholder={t('structure_menu.teams.chat.placeholder').toString()}
          value={getMessageDraft}
          onChange={setMessageDraft}
          style={{ resize: 'none', padding: 0, outline: 'none' }}
          rows={1}
          aria-label={t(
            'structure_menu.teams.chat.input_text_aria_label',
          ).toString()}
          id={'message-text-area'}
        />
        <div className={'button-panel'}>
          <Button
            variant="plain"
            isDisabled={isUploadButtonDisabled()}
            icon={<AttachFile />}
            id={'upload-file-button'}
            onClick={displayFileUploadView}
          />
          <Button
            variant="plain"
            isDisabled={sendMessageButtonIsDisabled()}
            icon={<SendMessage />}
            isLoading={isActionInProgress}
            spinnerAriaValueText={t('general.awaiting_response').toString()}
            onClick={(): void => {
              /* eslint-disable-next-line @typescript-eslint/no-non-null-assertion */
              sendMessage(teamId!);
            }}
            id={'send-message-button'}
          />
        </div>
      </TextInputGroup>
    </PageSection>
  );
};
