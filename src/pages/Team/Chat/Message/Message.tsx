import * as React from 'react';
import {
  Avatar,
  Grid,
  GridItem,
  Icon,
  Skeleton,
  Title,
  Tooltip,
} from '@patternfly/react-core';
import avatar from '../../../../assets/pages/User/Configuration/avatar.png';
import { useState } from 'react';
import CheckIcon from '@patternfly/react-icons/dist/esm/icons/check-icon';
import { File } from '../../Parts/File/File.tsx';
import { FileMessage } from './FileMessage.tsx';
import { getNameData } from '../../../../utils/logics/fileUtils.ts';

export const Message: React.FunctionComponent<{
  id?: number;
  ownerId?: string;
  teamOrigin?: string;
  fullDate?: string;
  simplifiedDateTime?: string;
  message?: string;
  messageParameters?: MessageParameters;
  name?: string;
  isLoading?: boolean;
  delete?: (messageId: number) => void;
}> = (props) => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [wasSentToEveryone, _setSentToEveryone] = useState<boolean>(false);
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [wasReadByEverybody, _setReadByEveryone] = useState<boolean>(false);

  const parseMessage = (
    message: string,
    messageParams: MessageParameters | undefined,
  ) => {
    for (const property in messageParams) {
      if (messageParams[property]) {
        message = message.replace(
          new RegExp(`{${property}}`, 'g'),
          messageParams[property].name,
        );
      }
    }

    return message;
  };

  const generateFileMessage = (file: File): React.ReactElement => {
    const parts: string[] = getNameData(file.name);

    return (
      <FileMessage
        key={`message-${props.id}`}
        /* eslint-disable-next-line @typescript-eslint/no-non-null-assertion */
        id={props.id!}
        /* eslint-disable-next-line @typescript-eslint/no-non-null-assertion */
        teamOrigin={props.teamOrigin!}
        fileId={parseInt(file.id)}
        /* eslint-disable-next-line @typescript-eslint/no-non-null-assertion */
        ownerId={props.ownerId!}
        name={parts[0].toLowerCase()}
        extension={parts[1]}
        /* eslint-disable-next-line @typescript-eslint/no-non-null-assertion */
        delete={props.delete!}
      />
    );
  };

  return (
    <div
      id={`message-container-${props.id}`}
      key={`message-container-${props.id}`}
      className={'message-structure'}
    >
      {props.isLoading ? (
        <Skeleton
          shape="circle"
          className={'avatar-icon'}
          id={'message-avatar'}
        />
      ) : (
        <Avatar
          src={avatar}
          alt="avatar"
          size="sm"
          className={'avatar-icon'}
          id={'message-avatar'}
        />
      )}

      <Grid style={{ width: '95%' }}>
        <GridItem>
          <div className={'message-header'}>
            <Title
              id={'message-username'}
              headingLevel={'h6'}
              children={
                props.isLoading ? (
                  <Skeleton height="50%" width={'150px'} />
                ) : (
                  props.name
                )
              }
            />
            <Tooltip
              exitDelay={100}
              entryDelay={100}
              aria="none"
              aria-live="polite"
              content={props.fullDate}
            >
              <Title
                id={'message-hour'}
                headingLevel={'h6'}
                children={
                  props.isLoading ? (
                    <Skeleton height="50%" width={'60px'} />
                  ) : (
                    props.simplifiedDateTime
                  )
                }
              />
            </Tooltip>
          </div>
          {props.isLoading ? (
            <Skeleton height="75%" width="75%" />
          ) : props.messageParameters?.file ? (
            generateFileMessage(props.messageParameters.file)
          ) : (
            <p
              style={{ whiteSpace: 'pre-wrap', overflowWrap: 'break-word' }}
              /* eslint-disable-next-line @typescript-eslint/no-non-null-assertion */
              children={parseMessage(props.message!, props.messageParameters)}
            />
          )}
        </GridItem>
      </Grid>

      {wasSentToEveryone && <Icon iconSize={'sm'} children={<CheckIcon />} />}
      {wasReadByEverybody && <Icon iconSize={'sm'} children={<CheckIcon />} />}
    </div>
  );
};

export interface Message {
  id: number;
  token: string;
  actorType: string;
  actorId: string;
  actorDisplayName: string;
  timestamp: number;
  message: string;
  messageParameters: MessageParameters;
  systemMessage: string;
  messageType: string;
  isReplyable: boolean;
  referenceId: string;
  reactions: object;
  expirationTimestamp: number;
  isLoading: boolean;
}

export interface MessageParameters {
  actor: { type: string; id: string; name: string };
  group: { type: string; id: string; name: string };
  user: { type: string; id: string; name: string };
  file: File;
  [key: string]: { type: string; id: string; name: string };
}
