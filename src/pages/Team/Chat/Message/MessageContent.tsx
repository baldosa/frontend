import * as React from 'react';
import { Title } from '@patternfly/react-core';
import { defineIcon } from '../../../../utils/FileUtils.tsx';
import {
  getFileExtension,
  getFilename,
} from '../../../../utils/logics/fileUtils.ts';

export const MessageContent: React.FunctionComponent<{
  name: string;
  fileId: number;
  extension: string;
}> = (props): React.JSX.Element => {
  return (
    <div className={'file-message-content'}>
      {defineIcon(props.name + props.extension)}
      <div className={'file-fullname'}>
        <Title
          className={'file-name'}
          headingLevel={'h6'}
          style={{ fontSize: '14px' }}
          children={getFilename(props.name + props.extension)}
        />
        <Title
          headingLevel={'h6'}
          style={{ fontSize: '14px' }}
          children={'.' + getFileExtension(props.name + props.extension)}
        />
      </div>
    </div>
  );
};
