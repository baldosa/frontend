import { createContext, useContext } from 'react';

type FilesContext = {
  containsRealElements?: boolean;
}

export const FilesContext = createContext<FilesContext>({});

export const useFilesContext = () => {
  return useContext(FilesContext);
};