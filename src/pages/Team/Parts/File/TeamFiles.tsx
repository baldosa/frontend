import { PageSection } from '@patternfly/react-core';
import {
  Dispatch, FunctionComponent, ReactElement,
  useEffect,
  useState,
} from 'react';
import { File } from './File.tsx';
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import {
  getFullName,
  getAccessToken,
} from '../../../../redux/reducers/auth.ts';
import {
  setHeaderDescription,
  setHeaderName,
  setHeaderNavigationDestination,
} from '../../../../redux/actions/generalActions.ts';
import { AnyAction } from '@reduxjs/toolkit';
import { useTranslation } from 'react-i18next';
import { FilterByInput } from '../../../../components/Filter/FilterByInput.tsx';
import { FloatingCard } from '../../../../components/FloatingCard.tsx';
import { getNameData } from '../../../../utils/logics/fileUtils.ts';
import { teamChatFullPath } from '../../../../routes.tsx';
import { getTeamDetails, getTeamFiles } from '../../../../service/team.ts';
import { useContextClient } from '../../../AppLayout/Contexts.tsx';
import { FilesContext } from './types.ts';

export const TeamFiles: FunctionComponent = () => {
  const [getFileIdToDelete, setFileIdToDelete] = useState<number | undefined>(undefined);
  const [containsRealElements, setContainsRealElements] = useState<boolean>(false);
  const [getFileOrderStatus, setFileOrderStatus] = useState<boolean>(false);
  const [getConversationToken, setConversationToken] = useState<string>('');
  const [getFiles, setFiles] = useState<ReactElement[]>([]);
  const [getTeamName, setTeamName] = useState<string>('');

  const userToken: string = useSelector(getAccessToken);
  const fullName: string = useSelector(getFullName);

  const dispatch: Dispatch<AnyAction> = useDispatch();

  const { getOpenAPIClient } = useContextClient();
  const { teamId } = useParams();
  const { t } = useTranslation();

  useEffect(() => {
    if (getFileIdToDelete) {
      const filteredFiles = getFiles.filter(
        (file) => file.props.id !== getFileIdToDelete,
      );
      setFiles(
        filteredFiles.length > 0
          ? filteredFiles
          : [generateEmptyNotification()],
      );
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [getFileIdToDelete]);

  useEffect((): void => {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    getTeamDetails(userToken, parseInt(teamId!))(getOpenAPIClient!)
      .then((response): void => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        const teamName: string = response.data.is_personal_workspace
          ? t('structure_menu.teams.my_space.header')
          : response.data.name;
        setConversationToken(response.data.nc_conversation_token);
        setHeaderName(
          t('structure_menu.teams.files.header', {
            group: teamName,
          }).toString(),
        )(dispatch);
        setTeamName(teamName);
        setHeaderDescription(t('structure_menu.teams.files.description'))(
          dispatch,
        );
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        setHeaderNavigationDestination(teamChatFullPath(parseInt(teamId!)))(
          dispatch,
        );
      })
      .catch((): void => {
        setHeaderName(t('structure_menu.teams.chat.title_error').toString())(
          dispatch,
        );
      })
      .finally(() => {
        setFiles(generateSkeletonsPreview());
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    const intervalToUpdateFilesCompletely = setInterval(() => {
      if (teamId && getConversationToken) {
        generateFiles(getFileOrderStatus);
      }
    }, 10000);

    return (): void => {
      clearInterval(intervalToUpdateFilesCompletely);
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [getConversationToken]);

  const generateSkeletonsPreview = () => {
    setContainsRealElements(false);
    return [...Array(4)].map(
      (_, index) => (
        <File
          key={`file-skeleton-${index}`}
          id={index}
          isLoading={true}
          /* eslint-disable-next-line @typescript-eslint/no-non-null-assertion */
          teamOrigin={teamId!}
          delete={setFileIdToDelete}
        />
      ),
      [],
    );
  };

  const generateErrorNotification = () => {
    setContainsRealElements(false);
    return (
      <FloatingCard
        key={`team-error-card-${teamId}`}
        alt={t('structure_menu.team.card_error.files.alt')}
        title={t('structure_menu.team.card_error.files.title', {
          user: fullName,
        })}
        message={t('structure_menu.team.card_error.files.message')}
        style={{ margin: '20px 10px' }}
        closeable={false}
      />
    );
  };

  const generateEmptyNotification = () => {
    return (
      <FloatingCard
        key={`team-no-elements-card-${teamId}`}
        alt={t('structure_menu.team.card_no_elements.files.alt')}
        title={t('structure_menu.team.card_no_elements.files.title', {
          user: fullName,
          group: getTeamName,
        })}
        message={t('structure_menu.team.card_no_elements.files.message')}
        style={{ margin: '20px 10px' }}
        closeable={false}
      />
    );
  };

  useEffect(() => {
    if (getConversationToken) {
      generateFiles(getFileOrderStatus);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [getFileOrderStatus]);

  const generateFiles = async (filesOrder: boolean): Promise<void> => {
    await getTeamFiles(userToken, getConversationToken, filesOrder)
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      (getOpenAPIClient!)
        .then((response): void => {
          if (response.data.files.length > 0) {
            setContainsRealElements(true);
            setFiles(
              // eslint-disable-next-line @typescript-eslint/ban-ts-comment
              // @ts-ignore
              response.data.files.map((file: Message) => {
                const obtainedFile: File = file.messageParameters.file;
                obtainedFile.ownerName = file.messageParameters.actor.name;
                obtainedFile.ownerId = file.messageParameters.actor.id;
                return generateFile(obtainedFile);
              }),
            );
          } else {
            setFiles([generateEmptyNotification()]);
          }
        })
        .catch((): void => {
          setFiles([generateErrorNotification()]);
        });
  };

  const generateFile = (file: File): ReactElement => {
    const parts: string[] = getNameData(file.name);

    return (
      <File
        key={`file-${file.id}`}
        id={parseInt(file.id)}
        name={parts[0].toLowerCase()}
        ownerName={file.ownerName}
        ownerId={file.ownerId}
        size={file.size}
        extension={parts[1]}
        isLoading={false}
        /* eslint-disable-next-line @typescript-eslint/no-non-null-assertion */
        teamOrigin={teamId!}
        delete={setFileIdToDelete}
      />
    );
  };

  return (
    <PageSection style={{ overflowY: 'hidden' }}>
      <FilesContext.Provider value={{ containsRealElements }}>
        <FilterByInput
          id={'team-files-filter'}
          filters={[
            {
              name: t(
                'structure_menu.teams.files.filters.selector.options.name',
              ),
              value: 'name',
            },
            {
              name: t(
                'structure_menu.teams.files.filters.selector.options.extension',
              ),
              value: 'extension',
            },
          ]}
          options={getFiles}
          changeFileOrder={setFileOrderStatus}
        />
      </FilesContext.Provider>
    </PageSection>
  );
};
