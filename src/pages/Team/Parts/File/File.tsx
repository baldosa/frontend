import {
  Button,
  Card,
  CardBody,
  Dropdown,
  KebabToggle,
  ListItem,
  Modal,
  Skeleton,
  Title,
} from '@patternfly/react-core';
import { defineIcon, onDownload } from '../../../../utils/FileUtils.tsx';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import {
  getAccessToken,
  getUsername,
} from '../../../../redux/reducers/auth.ts';
import {
  completeActionForContent,
  initiateActionForContent,
} from '../../../../redux/actions/teams.ts';
import {
  Dispatch,
  Fragment,
  FunctionComponent,
  ReactElement,
  useState,
} from 'react';
import { AnyAction } from '@reduxjs/toolkit';
import { RootState } from '../../../../redux/store.ts';
import { contentsActionInProgress } from '../../../../redux/reducers/teams.ts';
import {
  createPreview,
  getSizeHumanized,
  isFilePreviewAvailable,
  onDelete,
  renderFileSize,
  renderFullName,
} from '../../../../utils/logics/fileUtils.ts';
import { addAlert } from '../../../../redux/actions/alert.ts';
import { useContextClient } from '../../../AppLayout/Contexts.tsx';

export const File: FunctionComponent<{
  id: number;
  teamOrigin: string;
  name?: string;
  size?: number;
  ownerName?: string;
  ownerId?: string;
  simplify?: boolean;
  extension?: string;
  isLoading: boolean;
  delete: (messageId: number) => void;
}> = (props) => {
  const [getModalContent, setModalContent] = useState<ReactElement | undefined>(undefined);
  const [getStatusOfContentDownload, setStatusOfContentDownload] = useState<boolean>(false);
  const [getStatusOfContentDeletion, setStatusOfContentDeletion] = useState<boolean>(false);
  const [getContentPreviewStatus, setContentPreviewStatus] = useState<boolean>(false);
  const [getModalActions, setModalActions] = useState<ReactElement[]>([]);
  const [getModalClass, setModalClass] = useState<string>('preview');
  const [isModalOpen, setIsModalOpen] = useState<boolean>(false);
  const [getModalTitle, setModalTitle] = useState<string>('');
  const [isOpen, setIsOpen] = useState<boolean>(false);

  const userToken = useSelector(getAccessToken);
  const username = useSelector(getUsername);

  const listOfContentActionInProgress = useSelector((state: RootState) =>
    contentsActionInProgress(state, props.teamOrigin),
  );

  const dispatch: Dispatch<AnyAction> = useDispatch();
  const { getOpenAPIClient } = useContextClient();
  const { t } = useTranslation();

  const thereIsAContentActionInProgress = (contentId: number) => {
    return listOfContentActionInProgress.includes(contentId);
  };

  const onToggle = (isOpen: boolean): void => {
    setIsOpen(isOpen);
  };

  const sizeHumanized: { size: number; unit: string } = getSizeHumanized(
    /* eslint-disable-next-line @typescript-eslint/no-non-null-assertion */
    props.size!,
  );

  const isFileOwner = () => {
    return props.ownerId === username;
  };

  const openModal = () => {
    setIsModalOpen(true);
  };

  const closeModal = () => {
    setIsModalOpen(false);
  };

  const downloadContent = () => {
    initiateActionForContent(props.teamOrigin, props.id)(dispatch);
    setStatusOfContentDownload(true);
    onDownload(
      props.id,
      userToken,
      /* eslint-disable-next-line @typescript-eslint/no-non-null-assertion */
      renderFullName(props.name!, props.extension!),
    )
    /* eslint-disable-next-line @typescript-eslint/no-non-null-assertion */
    (getOpenAPIClient!)
      .catch((error) => {
        console.error(error);
      })
      .finally(() => {
        setStatusOfContentDownload(false);
        completeActionForContent(props.teamOrigin, props.id)(dispatch);
      });
  };

  const deleteContent = () => {
    initiateActionForContent(props.teamOrigin, props.id)(dispatch);
    setStatusOfContentDeletion(true);
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    onDelete(props.id, userToken)(getOpenAPIClient!)
      .then(() => {
        props.delete(props.id);
        addAlert(
          'success',
          t('structure_menu.teams.files.delete.alert.success', {
            name: props.name,
          }),
        )(dispatch);
      })
      .catch(() => {
        closeModal();
        addAlert(
          'danger',
          t('structure_menu.teams.files.delete.alert.error', {
            name: props.name,
          }),
        )(dispatch);
      })
      .finally(() => {
        setStatusOfContentDeletion(false);
        completeActionForContent(props.teamOrigin, props.id)(dispatch);
      });
  };

  const prepareRemoveFileModal = () => {
    setModalTitle(
      t('structure_menu.teams.files.delete.modal.title', {
        name: props.name,
      }).toString(),
    );
    setModalActions([
      <Button
        key="confirm"
        variant="primary"
        children={t(
          'structure_menu.teams.files.delete.modal.confirm',
        ).toString()}
        onClick={deleteContent}
      />,
      <Button
        key="cancel"
        variant="link"
        children={t(
          'structure_menu.teams.files.delete.modal.cancel',
        ).toString()}
        onClick={closeModal}
      />,
    ]);
    setModalContent(undefined);
    setModalClass('remove');
    openModal();
  };

  const loadDataForModalPreview = () => {
    setModalTitle('');
    setModalActions([]);
    setModalContent(undefined);
    setModalClass('preview');
    loadPreviewContent();
  };

  const loadPreviewContent = () => {
    initiateActionForContent(props.teamOrigin, props.id)(dispatch);
    setContentPreviewStatus(true);
    createPreview(
      props.id,
      userToken,
      /* eslint-disable-next-line @typescript-eslint/no-non-null-assertion */
      props.extension!,
      t('structure_menu.teams.files.preview.file_aria_label', {
        name: props.name,
      }),
    )
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    (getOpenAPIClient!)
      .then((result: ReactElement) => {
        openModal();
        setModalContent(result);
      })
      .catch((error) => {
        closeModal();
        console.error(error);
      })
      .finally(() => {
        setContentPreviewStatus(false);
        completeActionForContent(props.teamOrigin, props.id)(dispatch);
      });
  };

  return (
    <Fragment>
      <ListItem
        id={`file-${props.id}`}
        key={`file-${props.id}`}
        className={'list-item files'}
        style={{ width: '100%' }}
      >
        <Card className={'card-items file'}>
          {props.isLoading ? (
            <Skeleton height="100%" width="100%" />
          ) : (
            <CardBody className={'body'}>
              {/* eslint-disable-next-line @typescript-eslint/no-non-null-assertion */}
              {!props.simplify && defineIcon(props.extension!)}
              <div style={{ display: 'grid', width: '50%', minWidth: '50%' }}>
                <Title
                  headingLevel={'h6'}
                  style={{ fontSize: '14px' }}
                  className={'file-description'}
                  /* eslint-disable-next-line @typescript-eslint/no-non-null-assertion */
                  children={renderFullName(props.name!, props.extension!)}
                />
                {!props.simplify && (
                  <Title
                    headingLevel={'h6'}
                    style={{ fontSize: '12px', color: 'var(--grey-color)' }}
                    className={'file-description'}
                    children={t(
                      'structure_menu.team.card_error.files.file.owner',
                      {
                        user: props.ownerName,
                      },
                    )}
                  />
                )}
              </div>
              <Title
                headingLevel={'h6'}
                className={'file-size'}
                children={renderFileSize(
                  sizeHumanized.size,
                  sizeHumanized.unit,
                )}
              />
              {!props.simplify && (
                <Dropdown
                  style={{ minWidth: 'min-content' }}
                  position={'right'}
                  menuAppendTo={'parent'}
                  isOpen={isOpen}
                  isPlain
                  toggle={<KebabToggle id="toggle-kebab" onToggle={onToggle} />}
                  dropdownItems={[
                    <Button
                      key={`download-file-${props.id}`}
                      className={'message_actions_button'}
                      isLoading={
                        getStatusOfContentDownload &&
                        thereIsAContentActionInProgress(props.id)
                      }
                      isDisabled={thereIsAContentActionInProgress(props.id)}
                      onClick={downloadContent}
                      children={t('structure_menu.teams.files.download')}
                    />,
                    <Button
                      key="delete-file"
                      className={'message_actions_button'}
                      isLoading={
                        getStatusOfContentDeletion &&
                        thereIsAContentActionInProgress(props.id)
                      }
                      isDisabled={thereIsAContentActionInProgress(props.id)}
                      style={{ display: isFileOwner() ? 'block' : 'none' }}
                      onClick={prepareRemoveFileModal}
                      children={t('structure_menu.teams.files.delete.button')}
                    />,
                    <Button
                      key="preview-file"
                      className={'message_actions_button'}
                      isLoading={
                        getContentPreviewStatus &&
                        thereIsAContentActionInProgress(props.id)
                      }
                      isDisabled={thereIsAContentActionInProgress(props.id)}
                      style={{
                        display:
                          props.extension &&
                          isFilePreviewAvailable(props.extension)
                            ? 'block'
                            : 'none',
                      }}
                      onClick={loadDataForModalPreview}
                      children={t('structure_menu.teams.files.preview.button')}
                    />,
                  ]}
                />
              )}
            </CardBody>
          )}
        </Card>
      </ListItem>
      <Modal
        className={`file-action-modal ${getModalClass}`}
        variant={'default'}
        title={getModalTitle}
        actions={getModalActions}
        isOpen={isModalOpen}
        onClose={closeModal}
        children={getModalContent}
        aria-label={t(
          'structure_menu.teams.files.delete.modal.aria_label',
        ).toString()}
      />
    </Fragment>
  );
};

export interface File {
  type: string;
  id: string;
  name: string;
  ownerName: string;
  ownerId: string;
  size: number;
  path: string;
  mimetype: string;
  'preview-available': string;
}

File.defaultProps = {
  simplify: false,
};
