import * as React from 'react';
import { ExclamationTriangleIcon } from '@patternfly/react-icons';
import { PageSection, Button } from '@patternfly/react-core';
import { useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import EmptyStateWrapper from '../../components/EmptyStateWrapper';

const NotFound: React.FunctionComponent = () => {
  const { t } = useTranslation();

  function GoHomeBtn() {
    const navigate = useNavigate();
    function handleClick() {
      navigate('/');
    }
    return <Button onClick={handleClick}>{t('notfound.action')}</Button>;
  }

  return (
    <PageSection>
      <EmptyStateWrapper
        title={t('notfound.title')}
        description={t('notfound.text')}
        icon={ExclamationTriangleIcon}
        action={<GoHomeBtn />}
      />
    </PageSection>
  );
};

export { NotFound };
