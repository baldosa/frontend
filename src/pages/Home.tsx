import React from 'react';
import {
  Button,
  Modal,
  PageSection,
  ProgressStep,
  ProgressStepper,
} from '@patternfly/react-core';
import logo from '/colmena.png';
import {
  Wizard,
  WizardFooterWrapper,
  WizardStep,
} from '@patternfly/react-core/next';
import { TourStep } from '../components/Wizard/Tour/TourStep.tsx';
import { Beehive } from '../assets/WelcomeTour/Beehive.tsx';
import { useTranslation } from 'react-i18next';
import { BeesTalking } from '../assets/WelcomeTour/BeesTalking.tsx';
import { BeesWorking } from '../assets/WelcomeTour/BeesWorking.tsx';
import { RoundBees } from '../assets/WelcomeTour/RoundBees.tsx';
import { BeesFlying } from '../assets/WelcomeTour/BeesFlying.tsx';
import { useWizardContext } from '@patternfly/react-core/next';
import { getProgressStepIconByState } from '../utils/LayoutUtils.tsx';
import { FloatingCard } from '../components/FloatingCard.tsx';

export const Home: React.FunctionComponent = () => {
  const { t } = useTranslation();
  const [isModalOpened, setIsModalOpened] = React.useState(false);

  const openWelcomeWizard = () => {
    setIsModalOpened(true);
  };

  const closeWelcomeWizard = () => {
    setIsModalOpened(false);
  };

  const steps: React.ReactNode[] = [
    <WizardStep
      key={'first_step_welcome_to_tour'}
      name={t('wizard_tour.first_step.title')}
      id={'first_step_welcome_to_tour'}
      children={
        <TourStep
          title={t('wizard_tour.first_step.title')}
          className={'welcome-wizard-upper-body'}
          belowTheImage={true}
          image={<Beehive className={'welcome-image'} />}
          content={t('wizard_tour.first_step.paragraph')}
          closeModal={closeWelcomeWizard}
        />
      }
    />,
    <WizardStep
      key={'second_step_welcome_to_tour'}
      name={t('wizard_tour.second_step.title')}
      id={'second_step_welcome_to_tour'}
      children={
        <TourStep
          title={t('wizard_tour.second_step.title')}
          className={'welcome-wizard-upper-body'}
          belowTheImage={true}
          image={<BeesTalking className={'welcome-image'} />}
          content={t('wizard_tour.second_step.paragraph')}
          closeModal={closeWelcomeWizard}
        />
      }
    />,
    <WizardStep
      key={'third_step_welcome_to_tour'}
      name={t('wizard_tour.third_step.title')}
      id={'third_step_welcome_to_tour'}
      children={
        <TourStep
          title={t('wizard_tour.third_step.title')}
          className={'welcome-wizard-upper-body'}
          belowTheImage={true}
          image={<BeesWorking className={'welcome-image'} />}
          content={t('wizard_tour.third_step.paragraph')}
          closeModal={closeWelcomeWizard}
        />
      }
    />,
    <WizardStep
      key={'fourth_step_welcome_to_tour'}
      name={t('wizard_tour.fourth_step.title')}
      id={'fourth_step_welcome_to_tour'}
      children={
        <TourStep
          title={t('wizard_tour.fourth_step.title')}
          className={'welcome-wizard-upper-body'}
          belowTheImage={true}
          image={<BeesFlying className={'welcome-image'} />}
          content={t('wizard_tour.fourth_step.paragraph')}
          closeModal={closeWelcomeWizard}
        />
      }
    />,
    <WizardStep
      key={'fifth_step_welcome_to_tour'}
      name={t('wizard_tour.fifth_step.title')}
      id={'fifth_step_welcome_to_tour'}
      children={
        <TourStep
          title={t('wizard_tour.fifth_step.title')}
          className={'welcome-wizard-upper-body'}
          belowTheImage={false}
          image={<RoundBees className={'welcome-image'} />}
          content={t('wizard_tour.fifth_step.paragraph')}
          closeModal={closeWelcomeWizard}
        />
      }
    />,
  ];

  const CustomProgressStepper = () => {
    const { activeStep } = useWizardContext();
    return (
      <ProgressStepper isCompact isVertical={false}>
        {steps.slice().map((_, index) => {
          let variant:
            | 'default'
            | 'danger'
            | 'warning'
            | 'pending'
            | 'success'
            | 'info'
            | undefined = 'pending';
          let ariaLabel = 'pending step';
          if (index + 1 < activeStep.index) {
            variant = 'success';
            ariaLabel = 'completed step, step with success';
          } else if (index + 1 === activeStep.index) {
            variant = 'info';
            ariaLabel = 'current step';
          }

          return (
            <ProgressStep
              titleId={`progress_step_title_${activeStep.index}`}
              icon={getProgressStepIconByState(variant)}
              id={`progress_step_${activeStep.index}`}
              key={index}
              isCurrent={index === activeStep.index}
              aria-label={ariaLabel}
            />
          );
        })}
      </ProgressStepper>
    );
  };

  const CustomFooter: React.FunctionComponent = () => {
    const { goToNextStep, goToPrevStep, activeStep } = useWizardContext();

    return (
      <WizardFooterWrapper>
        <Button
          id={'wizard_tour_back_button'}
          className={'action_button'}
          variant={'link'}
          onClick={goToPrevStep}
          isDisabled={activeStep.index === 1}
          children={t('wizard_tour.footer.back.content')}
          aria-label={'wizard_tour.footer.back.alt'}
        />
        {CustomProgressStepper()}
        {activeStep.index < steps.length ? (
          <Button
            id={'wizard_tour_next_button'}
            className={'action_button'}
            variant={'link'}
            onClick={goToNextStep}
            children={t('wizard_tour.footer.next.content')}
            aria-label={'wizard_tour.footer.next.alt'}
          />
        ) : (
          <Button
            id={'wizard_tour_finish_button'}
            className={'action_button'}
            variant={'link'}
            onClick={closeWelcomeWizard}
            children={t('wizard_tour.footer.finish.content')}
            aria-label={'wizard_tour.footer.finish.alt'}
          />
        )}
      </WizardFooterWrapper>
    );
  };

  return (
    <PageSection>
      <div className={'display-flex-row-between'}>
        <img src={logo} alt={'home.developer_logo'} />
        <Button
          id={'wizard_tour_initiate_trip_button'}
          className={'welcome-modal-button'}
          variant={'secondary'}
          children={t('home.initiate_trip.content')}
          onClick={openWelcomeWizard}
          aria-label={'home.initiate_trip.alt'}
        />
      </div>
      <FloatingCard
        title={t('home.welcome_modal.title')}
        alt={t('home.welcome_modal.alt')}
        message={t('home.welcome_modal.message')}
        closeable={true}
      />
      <Modal
        showClose={false}
        isOpen={isModalOpened}
        className={'modal-wizard-tour'}
        aria-label={'welcome-modal'}
      >
        <Wizard
          className={'wizard-tour'}
          children={steps}
          footer={<CustomFooter />}
        />
      </Modal>
    </PageSection>
  );
};
