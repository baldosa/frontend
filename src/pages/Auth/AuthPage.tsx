import {
  createContext,
  FunctionComponent,
  ReactElement,
  useContext,
  useEffect,
  useState,
} from 'react';
import { Card, CardTitle, PageSection } from '@patternfly/react-core';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';
import { TopSection } from '../../components/Auth/TopSection.tsx';
import { ProfileProperties } from '../../components/Auth/ProfileProperties.ts';
import { LOGIN } from '../../components/Auth/ValidationRules.ts';

function loginAuthFormProperties(
  t: TFunction<'translation', undefined, 'translation'>,
): ProfileProperties {
  return new ProfileProperties(
    t('login.form.username_label'),
    t('login.form.password_label'),
    t('login.form.button_content'),
    t('login.form.username_placeholder'),
    t('login.form.password_placeholder'),
    t('login.form.username_helper_text'),
    t('login.form.password_helper_text'),
    t('login.form.title'),
  );
}

function signupAuthFormProperties(
  t: TFunction<'translation', undefined, 'translation'>,
): ProfileProperties {
  return new ProfileProperties(
    t('signup.form.username_label'),
    t('signup.form.password_label'),
    t('signup.form.button_content'),
    t('signup.form.username_placeholder'),
    t('signup.form.password_placeholder'),
    t('signup.form.username_helper_text'),
    t('signup.form.password_helper_text'),
    t('signup.form.title'),
  );
}

interface AuthContext {
  flag: string;
  button_label: string;
  profile_properties: ProfileProperties | undefined;
}

const AuthContext = createContext<AuthContext>({
  flag: LOGIN,
  button_label: '',
  profile_properties: undefined,
});

export const AuthPage: FunctionComponent<{
  flag: string;
  children: ReactElement;
}> = (props) => {
  const { t, i18n } = useTranslation();
  const [profile, setProfile] = useState<ProfileProperties>(
    loginAuthFormProperties(t),
  );

  useEffect(() => {
    if (props.flag == LOGIN) {
      setProfile(loginAuthFormProperties(t));
    } else {
      setProfile(signupAuthFormProperties(t));
    }
  }, [props.flag, i18n.language, t]);

  return (
    <PageSection isCenterAligned style={{ height: '90%' }}>
      <AuthContext.Provider
        value={{
          flag: props.flag,
          button_label: profile.buttonContent,
          profile_properties: profile,
        }}
      >
        <Card className={'auth-page-card'} isPlain>
          <CardTitle className={'auth-page-title'} children={<TopSection />} />
          {props.children}
        </Card>
      </AuthContext.Provider>
    </PageSection>
  );
};

// eslint-disable-next-line react-refresh/only-export-components
export const useAuthContext = () => {
  return useContext(AuthContext);
};
