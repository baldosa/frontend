import {
  FunctionComponent,
  useState,
  useContext,
  createContext, useEffect, Dispatch,
} from 'react';
import {
  Card,
  CardTitle,
  PageSection,
  Banner,
  Button,
  DataList,
  CardBody,
  MenuContent, Menu,
} from '@patternfly/react-core';
import { ServerModal } from '../../components/Auth/servers/ServerModal.tsx';
import { TopSection } from '../../components/Auth/TopSection.tsx';
import { ServerElement } from '../../components/Auth/servers/ServerElement.tsx';
import { useTranslation } from 'react-i18next';
import {
  ServerContextType,
  ServerDataType,
  ServerCoreDataType
} from '../../components/Auth/servers/ServerDataType.ts';
import { getAllServers, saveServerData } from '../../utils/logics/authUtils.ts';
import { AnyAction } from '@reduxjs/toolkit';
import { useDispatch } from 'react-redux';
import { addAlert } from '../../redux/actions/alert.ts';
import { deleteServerData } from '../../utils/logics/authUtils.ts';

const ServerContext = createContext<ServerContextType>({
  id: undefined,
  name: '',
  url: '',
  updateIsRequired: undefined,
  clearServerData: undefined,
  setActualServerData: undefined,
  toggleModal: undefined,
  isOpen: false,
  deleteServer:undefined,
});

export const ServersPage: FunctionComponent = () => {
  const [getServers, setServers] = useState<Map<number, ServerCoreDataType>>(new Map());
  const [getServerData, setServerData] = useState<ServerDataType>({ id: undefined, name: '', url: '' });
  const [updateIsRequired, setUpdateIsRequired] = useState<boolean>(false);
  const [isModalOpen, setIsModalOpen] = useState<boolean>(false);
  const cardContainerId = 'server-list'
  const { t } = useTranslation();
  const dispatch: Dispatch<AnyAction> = useDispatch();

  const handleModalToggle = () => {
    setIsModalOpen(!isModalOpen);
  };

  const handleUpdateStatus = () => {
    setUpdateIsRequired(!updateIsRequired);
  };

  const setActualServerData = (id: number | undefined, name: string, url: string) => {
    setServerData({ id, name, url });
  };

  const clearActualServerData = () => {
    setActualServerData(undefined, '', '');
  }

  const updateServerList = (id: number, name: string, url: string) => {
    setServers((prevServers) => {
      prevServers.set(id, { name, url });
      return prevServers;
    });
  };

  const addOrUpdateServer = (id: number | undefined, name: string, url: string) => {
    saveServerData(id, name, url, t, dispatch)
      .then((serverId: number) => {
        updateServerList(serverId, name, url);
        clearActualServerData();
        handleUpdateStatus();
        handleModalToggle();
      });

  }

  const deleteServer = (id:number) => {
    deleteServerData(id)
    const updatedServers = new Map(getServers);
    updatedServers.delete(id);
    setServers(updatedServers);  
  }
  
  useEffect(() => {
    if (updateIsRequired) {
      addOrUpdateServer(getServerData.id, getServerData.name, getServerData.url);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [updateIsRequired]);

  useEffect(() => {
    getAllServers()
      .then((servers) => { setServers(servers) })
      .catch(() => addAlert('danger', t('auth.server_list.alert.danger.retrieve_servers'))(dispatch));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [setServers]);

  return (
    <ServerContext.Provider
      value={{ ...getServerData, isOpen: isModalOpen, setActualServerData: setActualServerData, toggleModal: handleModalToggle, updateIsRequired: handleUpdateStatus, clearServerData: clearActualServerData, deleteServer: deleteServer }}>
      <PageSection isCenterAligned style={{ height: '90%' }}>
        <Card className={'auth-page-card'} id={cardContainerId} isPlain>
          <CardTitle className={'auth-page-title'} children={<TopSection />} />
          <CardBody style={{ border: '1px solid gray', padding: "0px" }}>
            <Banner className={'servers-banner'}>
              <strong
                style={{ color: "black" }}
                children={t('auth.server_list.banner.title')}
              />
              <Button
                className={'serverButton'}
                variant={'primary'}
                isSmall
                onClick={handleModalToggle}
                children={t('auth.server_list.banner.content_button')}
              />
            </Banner>
            <Menu isScrollable isPlain>
              <MenuContent>
                  <DataList aria-label={'server list'} isCompact>
                    {Array.from(getServers.entries()).map(([key, server]) => (
                      <ServerElement
                        key={key}
                        id={key}
                        name={server.name}
                        url={server.url}
                        parentId={cardContainerId}
                      />
                    ))}
                  </DataList>
              </MenuContent>
            </Menu>
          </CardBody>
        </Card>
        <ServerModal />
      </PageSection >
    </ServerContext.Provider>
  );
};

// eslint-disable-next-line react-refresh/only-export-components
export const useServerContext = () => {
  return useContext(ServerContext);
};
