import React, { useContext } from 'react';
import { PageSection } from '@patternfly/react-core';
import RecorderWrapper from '../../components/Waveform/ui/WaveformPlaylist/Recorder';
import RecorderActions from '../../components/Waveform/ui/WaveformPlaylist/Recorder/RecorderActions.tsx';
import { setHeaderName } from '../../redux/actions/generalActions.ts';
import { Dispatch } from 'react';
import { AnyAction } from '@reduxjs/toolkit';
import { useDispatch } from 'react-redux';
import { PlaylistContextEnum } from '../../enums';
import { setEditorContext } from '../../redux/actions/recordings';
import { Helmet } from 'react-helmet';
import { ContextPropsType } from '../AppLayout/types.ts';
import { ContextPropsContext } from '../AppLayout/Contexts.tsx';

export const EditorDropdown = () => {
  // TODO: use contextProps.emitter to emit waveform events from the header dropdown actions.
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  const contextProps: ContextPropsType = useContext(ContextPropsContext);
  return <RecorderActions emitter={contextProps.emitter} />;
};

export const Editor: React.FunctionComponent = () => {
  const dispatch: Dispatch<AnyAction> = useDispatch();

  setHeaderName('')(dispatch);
  setEditorContext(PlaylistContextEnum.EDITOR)(dispatch);

  return (
    <PageSection isWidthLimited={false} padding={{ default: 'noPadding' }}>
      <div>
        <Helmet>
          <script src="https://kit.fontawesome.com/ef69927139.js" />
        </Helmet>
      </div>
      <RecorderWrapper />
    </PageSection>
  );
};
