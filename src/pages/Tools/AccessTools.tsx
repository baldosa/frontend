import * as React from 'react';
import { List, PageSection, Title } from '@patternfly/react-core';
import { ToolItem } from './ToolItem.tsx';
import { RecorderIcon } from '../../assets/icon/RecorderIcon.tsx';
import { EditorIcon } from '../../assets/icon/EditorIcon.tsx';
import { Dispatch } from 'react';
import { AnyAction } from '@reduxjs/toolkit';
import { useDispatch } from 'react-redux';
import { setHeaderName } from '../../redux/actions/generalActions.ts';
import { useTranslation } from 'react-i18next';
import { editorFullPath, recorderFullPath } from '../../routes.tsx';

const AccessTools: React.FunctionComponent = () => {
  const dispatch: Dispatch<AnyAction> = useDispatch();
  const { t } = useTranslation();

  setHeaderName(t('tools.header').toString())(dispatch);

  return (
    <PageSection className={'tools-section'}>
      <Title
        headingLevel={'h6'}
        className={'tools-title'}
        children={t('tools.title')}
      />
      <List className={'list'}>
        <ToolItem
          icon={<RecorderIcon />}
          name={t('title.tools.recorder')}
          url={recorderFullPath()}
          forceReloadPage
        />
        <ToolItem
          icon={<EditorIcon />}
          name={t('title.tools.editor')}
          url={editorFullPath()}
          forceReloadPage
        />
      </List>
    </PageSection>
  );
};

export { AccessTools };
