import { FunctionComponent } from 'react';
import { Avatar, Card, CardBody, CardTitle } from '@patternfly/react-core';
import avatar from '../../../assets/pages/User/Configuration/avatar.png';
import { convertBase64ToBlobURL } from '../../../utils/logics/fileUtils.ts';

export const MediaMember: FunctionComponent<{
  name: string;
  avatar: string;
  group: string;
  key: string;
}> = (props) => {
  return (
    <Card className={'media_member'}>
      <Avatar
        id={'message-avatar'}
        className={'avatar-icon'}
        alt={'avatar'}
        size={'md'}
        src={props.avatar ? convertBase64ToBlobURL(props.avatar) : avatar}
        style={{ marginLeft: '15px' }}
      />
      <div>
        <CardTitle children={props.name} />
        <CardBody children={props.group} />
      </div>
    </Card>
  );
};
