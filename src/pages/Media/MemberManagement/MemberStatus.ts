export enum MemberStatus {
  SUSPENDED = 'SUSPENDED',
  ACTIVE = 'ACTIVE',
}
