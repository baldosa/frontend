import { FunctionComponent, ReactElement, useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import {
  getAccessToken,
  getOrganizationId as getOrganizationIdFromState,
} from '../../../redux/reducers/auth.ts';
import { useTranslation } from 'react-i18next';
import { InvitationStatus } from './InvitationStatus.tsx';
import { Components } from '../../../api/utilities/Definitions';
import { Tab } from '@patternfly/react-core';
import { MediaEntitiesList } from '../MediaEntitiesList.tsx';
import { MediaInvitation } from './MediaInvitation.tsx';
import { useContextClient } from '../../AppLayout/Contexts.tsx';
import { getAllInvitations } from '../../../service/organization.ts';

export const MediaInvitationsList: FunctionComponent = () => {

  const [activeTabKey, setActiveTabKey] = useState<InvitationStatus>(InvitationStatus.ACCEPTED);
  const [getAmountOfAcceptedInvitations, setAmountOfAcceptedInvitations] = useState<number>(0);
  const [getAmountOfPendingInvitations, setAmountOfPendingInvitations] = useState<number>(0);
  const [getAcceptedInvitations, setAcceptedInvitations] = useState<ReactElement[]>([]);
  const [getPendingInvitations, setPendingInvitations] = useState<ReactElement[]>([]);
  const [getAmountOfInvitations, setAmountOfInvitations] = useState<number>(0);
  const [getInvitations, setInvitations] = useState<ReactElement[]>([]);
  const [getDataStatus, setDataStatus] = useState<boolean>(false);

  const getOrganizationId: number = useSelector(getOrganizationIdFromState);
  const getTokenFromUser: string = useSelector(getAccessToken);

  const { getOpenAPIClient } = useContextClient();
  const { t } = useTranslation();

  useEffect(() => {
    setDataInvitations(getOrganizationId, getTokenFromUser);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    const intervalToGetNewMembers = setInterval(() => {
      setDataStatus(false);
      setDataInvitations(getOrganizationId, getTokenFromUser);
    }, 10000);

    return (): void => {
      clearInterval(intervalToGetNewMembers);
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (getDataStatus) {
      switch (activeTabKey) {
        case InvitationStatus.ACCEPTED:
          setInvitations(getAcceptedInvitations);
          setAmountOfInvitations(getAmountOfAcceptedInvitations);
          break;
        case InvitationStatus.PENDING:
          setInvitations(getPendingInvitations);
          setAmountOfInvitations(getAmountOfPendingInvitations);
          break;
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [getDataStatus, activeTabKey]);

  const setDataInvitations = (organizationId: number, userToken: string) => {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    getAllInvitations(organizationId, userToken)(getOpenAPIClient!)
      .then((response) => {
        const acceptedInvitations: Components.Schemas.ListInvitationsResponse[] =
          response.data.filter(
            (invitation) => invitation.state == InvitationStatus.ACCEPTED,
          );
        const pendingInvitations: Components.Schemas.ListInvitationsResponse[] =
          response.data.filter(
            (invitation) => invitation.state != InvitationStatus.ACCEPTED,
          );

        setAcceptedInvitations(renderMediaInvitations(acceptedInvitations));
        setPendingInvitations(renderMediaInvitations(pendingInvitations));
        setAmountOfAcceptedInvitations(acceptedInvitations.length);
        setAmountOfPendingInvitations(pendingInvitations.length);
        setDataStatus(true);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const renderMediaInvitations = (invitationsData: any[]) => {
    return invitationsData.map((invitationData, index) => (
      <MediaInvitation
        key={`media_member_${index}`}
        name={invitationData.user.full_name}
        invited_by={invitationData.invited_by.full_name}
        status={t(
          `media_section.invitations_management.invitation.status.${invitationData.state.toLowerCase()}`,
        )}
        avatar={invitationData.user.avatar}
      />
    ));
  };

  const handleTabSelect = (tabKey: string) => {
    setActiveTabKey(tabKey as InvitationStatus);
  };

  return (
    <MediaEntitiesList
      entities={getInvitations}
      amount={getAmountOfInvitations}
      title={t('media_section.invitations_management.title')}
      activeTab={activeTabKey}
      tabs={[
        <Tab
          eventKey={InvitationStatus.ACCEPTED}
          title={t(
            'media_section.invitations_management.tabs.accepted_invitations',
            { amount: getAmountOfAcceptedInvitations },
          )}
        />,
        <Tab
          eventKey={InvitationStatus.PENDING}
          title={t(
            'media_section.invitations_management.tabs.pending_invitations',
            { amount: getAmountOfPendingInvitations },
          )}
        />,
      ]}
      onTabSelect={handleTabSelect}
    />
  );
};
