import React, {
  FunctionComponent,
  ReactElement,
  useEffect,
  useState,
} from 'react';
import {
  Button,
  List,
  Modal,
  Pagination,
  Tabs,
  Title,
} from '@patternfly/react-core';
import PlusCircleIcon from '@patternfly/react-icons/dist/esm/icons/plus-circle-icon';
import { InvitationForm } from '../User/Configuration/Invitation/InvitationForm.tsx';
import { useTranslation } from 'react-i18next';

export const MediaEntitiesList: FunctionComponent<{
  entities: ReactElement[];
  amount: number;
  title: string;
  tabs: ReactElement[];
  activeTab: string;
  onTabSelect: (tabKey: string) => void;
}> = (props) => {
  const [isModalOpen, setIsModalOpen] = useState<boolean>(false);
  const [getCurrentPage, setCurrentPage] = useState<number>(1);
  const [getAmountPerPage, setAmountPerPage] = useState<number>(10);
  const [activeTabKey, setActiveTabKey] = useState<string>(props.activeTab);
  const [getEntitiesToRender, setEntitiesToRender] = useState<ReactElement[]>(
    [],
  );

  const { t } = useTranslation();

  useEffect(() => {
    selectMediaEntities(getCurrentPage, getAmountPerPage, props.amount);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [activeTabKey, getCurrentPage, props.entities]);

  const handleModalToggle = () => {
    setIsModalOpen(!isModalOpen);
  };

  const onSetPage = (
    _event: React.MouseEvent | React.KeyboardEvent | MouseEvent,
    newPage: number,
  ) => {
    setCurrentPage(newPage);
  };

  const onPerPageSelect = (
    _event: React.MouseEvent | React.KeyboardEvent | MouseEvent,
    newPerPage: number,
    newPage: number,
  ) => {
    setCurrentPage(newPage);
    setAmountPerPage(newPerPage);
  };

  const handleTabClick = (
    _event: React.MouseEvent | React.KeyboardEvent | MouseEvent,
    tabIndex: string | number,
  ) => {
    setActiveTabKey(String(tabIndex));
    props.onTabSelect(String(tabIndex));
  };

  const selectMediaEntities = (
    currentPage: number,
    perPage: number,
    total: number,
  ) => {
    const initialIndex = (currentPage - 1) * perPage;
    let lastIndex = initialIndex + perPage;

    if (lastIndex > total) {
      lastIndex = total;
    }

    setEntitiesToRender(props.entities.slice(initialIndex, lastIndex));
  };

  return (
    <div style={{ height: 'inherit' }}>
      <div className={'media_entities_header'}>
        <Title
          className={'media_entities_title title'}
          headingLevel={'h3'}
          children={props.title}
        />
        <Button
          className={'add_media_member_button'}
          variant={'plain'}
          icon={<PlusCircleIcon size={'lg'} />}
          onClick={() => setIsModalOpen(true)}
        />
      </div>
      <Tabs
        isFilled={true}
        activeKey={activeTabKey}
        onSelect={handleTabClick}
        children={props.tabs}
      />
      <Pagination
        className={'media_entities_pagination'}
        isStatic={true}
        isCompact={true}
        variant={'bottom'}
        page={getCurrentPage}
        perPage={getAmountPerPage}
        perPageComponent={'button'}
        itemCount={props.amount}
        onSetPage={onSetPage}
        onPerPageSelect={onPerPageSelect}
        widgetId={'pagination-organization-members'}
        titles={{
          perPageSuffix: t('media_section.pagination.per_page').toString(),
          ofWord: t('media_section.pagination.of').toString(),
        }}
      />
      <List
        isPlain
        className={'media_entities_list'}
        children={getEntitiesToRender}
      />
      <Modal
        className={'media_entities_form'}
        aria-label={'media_members_invitation_modal'}
        isOpen={isModalOpen}
        onClose={handleModalToggle}
        children={
          <InvitationForm onSubmit={handleModalToggle} isStep={false} />
        }
      />
    </div>
  );
};
