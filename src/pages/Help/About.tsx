import {
    FunctionComponent
} from 'react';
import { useTranslation } from 'react-i18next';
import { Panel, PanelMain, PanelMainBody, PanelHeader, Divider } from '@patternfly/react-core';
import logo from '/favicon.ico';
import { openApiVersion } from '../../api/Client';
import colmena from '../../../package.json'
export const About: FunctionComponent = () => {
    const { t } = useTranslation();
    return (
        <Panel style={{backgroundColor: '#f9f4fb', borderRadius: '0.5rem', padding: '16px', marginTop: '20%'}}>
            <PanelHeader>
                <h1 className='about_dark_text'>{t('about.header.title')}</h1>
                <h4 className='about_light_text'>{t('about.header.description')}</h4>
            </PanelHeader>
            <Divider />
            <PanelMain>
                <PanelMainBody>
                    <h2 className='about_light_text'><b>{t('about.body.title')}</b></h2>
                    <br />
                    <div className='about_body'>
                        <img src={logo} alt="" style={{ width: "40px", height: "40px" }} />
                        <div>
                            <h4 className='about_dark_text'>Colmena</h4>
                            <h5 className='about_light_text'>{colmena.version}</h5>
                        </div>
                    </div>
                    <br />
                    <h4 className='about_dark_text'>{t('about.body.schema_version')}</h4>
                    <h5 className='about_light_text'>{openApiVersion}</h5>
                </PanelMainBody>
            </PanelMain>
        </Panel>
    );
};
