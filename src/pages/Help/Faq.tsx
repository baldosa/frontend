import { FunctionComponent, useState } from 'react';
import {
  Accordion,
  AccordionContent,
  AccordionItem,
  AccordionToggle,
  Title,
} from '@patternfly/react-core';
import { useTranslation } from 'react-i18next';

export const Faq: FunctionComponent = () => {
  const [getExpandedElementIds, setExpandedElementIds] = useState<string[]>([]);

  const amountOfQuestions = 12;

  const { t } = useTranslation();

  const toggle = (id: string) => {
    setExpandedElementIds((prevState) =>
      prevState.includes(id)
        ? prevState.filter((value) => value !== id)
        : [...prevState, id],
    );
  };

  const renderQuestions = () => {
    return [...Array(amountOfQuestions).keys()].map((index) => {
      const questionId = `question_${index}_id`;
      return (
        <AccordionItem key={`question_${index}_item`}>
          <AccordionToggle
            id={questionId}
            children={<b>{t(`faq.question_${index}.content`)}</b>}
            onClick={() => toggle(questionId)}
            isExpanded={getExpandedElementIds.includes(questionId)}
          />
          <AccordionContent
            className={'faq_accordion content'}
            isHidden={!getExpandedElementIds.includes(questionId)}
            children={<p>{t(`faq.question_${index}.reply`)}</p>}
          />
        </AccordionItem>
      );
    });
  };

  return (
    <div style={{ height: 'inherit' }}>
      <Title
        className={'media_entities_title title'}
        children={t('media_section.invitations_management.title')}
        headingLevel={'h3'}
      />
      <Accordion
        className={'faq_accordion'}
        style={{ height: '80%', overflowY: 'scroll' }}
        children={renderQuestions()}
      />
    </div>
  );
};
