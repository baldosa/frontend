import React, { useEffect, useState } from 'react';
import { Button, PageGroup, PageSection } from '@patternfly/react-core';
import { useNavigate } from 'react-router-dom';
import { configurationFullPath, generalPath } from '../../routes.tsx';
import { useTranslation } from 'react-i18next';
import { PreConfigurationIcon } from '../../assets/pages/User/Welcome/PreConfigurationIcon.tsx';
import { OverlappingHint } from '../../components/OverlappingHint.tsx';
import { isPortrait } from '../../utils/LayoutUtils.tsx';

export const AccessToInitialConfiguration: React.FunctionComponent = () => {
  const [isHintDisplayed, setIsHintDisplayed] = useState<boolean>(false);
  const [isPortraitView, setIsPortraitView] = React.useState<boolean>(false);
  const navigate = useNavigate();
  const { t } = useTranslation();

  const startConfiguringButtonClick = (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>,
  ): void => {
    event.preventDefault();
    navigate(configurationFullPath());
  };

  const handleHintToggle = () => {
    setIsHintDisplayed(!isHintDisplayed);
  };

  useEffect(() => {
    const handleResize = () => {
      setIsPortraitView(isPortrait());
    };
    window.addEventListener('resize', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  return (
    <PageGroup>
      <PageSection
        key={'top_section'}
        className={'column'}
        style={{
          height: 'auto',
          alignItems: isPortraitView ? 'initial' : 'center',
        }}
      >
        <span
          style={{ width: 'fit-content' }}
          children={t('user.configuration.welcome.main_title')}
        />
        <span
          style={{ width: 'fit-content' }}
          children={t('user.configuration.welcome.body')}
        />
        <span
          style={{ width: 'fit-content' }}
          children={t('user.configuration.welcome.greeting')}
        />
        <div
          style={{ marginLeft: 'auto', marginRight: 'auto' }}
          children={<PreConfigurationIcon />}
        />
      </PageSection>
      <PageSection key={'bottom_Section'} className={'welcome-bottom-section'}>
        <Button
          className={'welcome-modal-button'}
          variant={'secondary'}
          aria-label="Action"
          onClick={handleHintToggle}
          id={'skip_configuration_click_button'}
          children={
            <strong
              children={t('user.configuration.welcome.skip_section_button')}
            />
          }
        />
        <Button
          className={'initial-setup-button'}
          style={{ margin: 0 }}
          aria-label="Action"
          onClick={startConfiguringButtonClick}
          id={'start_configuration_click_button'}
          children={
            <strong
              children={t('user.configuration.welcome.edit_profile_button')}
            />
          }
        />
      </PageSection>
      <OverlappingHint
        route={generalPath}
        isDisplayed={isHintDisplayed}
        title={t('user.configuration.welcome.skip_section.title')}
        body={t('user.configuration.welcome.skip_section.body')}
        footer={t('user.configuration.welcome.skip_section.footer')}
        buttonContentTag={t(
          'user.configuration.welcome.skip_section.go_to_home_button',
        )}
      />
    </PageGroup>
  );
};
