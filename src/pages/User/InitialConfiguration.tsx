import React, { Dispatch, useEffect, useState } from 'react';
import { Wizard, WizardStep } from '@patternfly/react-core/next';
import { useTranslation } from 'react-i18next';
import {
  useWizardContext,
  WizardFooterWrapper,
} from '@patternfly/react-core/next';
import AngleLeftIcon from '@patternfly/react-icons/dist/esm/icons/angle-left-icon';
import { UserProfileForm } from './Configuration/UserProfileForm.tsx';
import { OrganizationDetailsForm } from './Configuration/Organization/OrganizationDetailsForm.tsx';
import { InvitationForm } from './Configuration/Invitation/InvitationForm.tsx';
import {
  Button,
  Modal,
  ModalBoxBody,
  ModalVariant,
  PageSection,
  WizardHeader,
} from '@patternfly/react-core';
import { generalPath } from '../../routes.tsx';
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { userProfileStepId } from '../../redux/reducers/configuration/initial/userProfileDetails.ts';
import { organizationStepId } from '../../redux/reducers/configuration/initial/organizationDetails.ts';
import { invitationStepId } from '../../redux/reducers/configuration/initial/invitationSection.ts';
import {
  getIndexStepToDisplay,
  getStepsToCompleteInTheWizard,
  isCompleted,
} from '../../redux/reducers/configuration/preConfiguration.ts';
import { AnyAction } from '@reduxjs/toolkit';
import { preConfigurationCompleteStep } from '../../redux/actions/configuration/preConfiguration.ts';
import { resetValidationRequests } from '../../redux/actions/generalActions.ts';

export interface CompleteStepInterface {
  stepId: string;
  isAwaitingResponse: boolean;
  isStepCompleted: boolean;
  isRedirectionRequired: boolean;
  firstNavigationAction: () => void;
  secondNavigationAction: (index: number) => void;
  stateAction: () => (dispatch: Dispatch<AnyAction>) => void;
}

const WizardFooter: React.FunctionComponent = () => {
  const { t } = useTranslation();
  const { goToPrevStep, activeStep } = useWizardContext();
  const navigate = useNavigate();

  return (
    <WizardFooterWrapper>
      <Button
        variant="plain"
        onClick={() => {
          if (activeStep.index == 1) {
            navigate(generalPath);
          } else {
            goToPrevStep();
          }
        }}
        iconPosition={'left'}
        style={{ color: 'white' }}
      >
        <AngleLeftIcon />
        <strong>{t('user.configuration.button_back_content')}</strong>
      </Button>
    </WizardFooterWrapper>
  );
};

export const InitialConfiguration: React.FunctionComponent = () => {
  const [getSteps, setSteps] = useState<React.ReactNode[]>([]);
  const [getCurrentStepId, setCurrentStepId] = useState('');
  const [isModalOpen, setIsModalOpen] = React.useState(false);
  const [getModalTitle, setModalTitle] = useState('');
  const [getModalContent, setModalContent] = useState('');

  const stepsToCompleteInTheWizard: string[] = useSelector(
    getStepsToCompleteInTheWizard,
  );
  const indexStepToDisplay: number = useSelector(getIndexStepToDisplay);
  const preConfigurationIsCompleted: boolean = useSelector(isCompleted);

  const { t } = useTranslation();
  const dispatch: Dispatch<AnyAction> = useDispatch();
  const navigate = useNavigate();

  const handleModalToggle = () => {
    setIsModalOpen(!isModalOpen);
  };

  const completeStep = (props: CompleteStepInterface) => {
    if (!preConfigurationIsCompleted) {
      if (props.isAwaitingResponse && props.isStepCompleted) {
        preConfigurationCompleteStep(props.stepId)(dispatch);

        if (!props.isRedirectionRequired) {
          props.firstNavigationAction();
        }
        if (props.isRedirectionRequired) {
          props.stateAction()(dispatch);
          props.secondNavigationAction(indexStepToDisplay);
        }
      }
    }
  };

  const availableSteps: Record<
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    any,
    {
      stepComponent: React.ReactElement;
      modalData: { title: string; content: string };
    }
  > = {
    [userProfileStepId]: {
      stepComponent: (
        <WizardStep
          id={userProfileStepId}
          key={userProfileStepId}
          name={t('user.configuration.first_step')}
          children={
            <UserProfileForm
              className={'initial-setup-form'}
              onSubmit={completeStep}
            />
          }
        />
      ),
      modalData: {
        title: t(
          'user.configuration.profile_form.post_send_data_modal.title',
        ).toString(),
        content: t(
          'user.configuration.profile_form.post_send_data_modal.content',
        ).toString(),
      },
    },
    [organizationStepId]: {
      stepComponent: (
        <WizardStep
          id={organizationStepId}
          key={organizationStepId}
          name={t('user.configuration.second_step')}
          children={
            <OrganizationDetailsForm
              className={'initial-setup-form'}
              onSubmit={completeStep}
            />
          }
        />
      ),
      modalData: {
        title: t(
          'user.configuration.organization_form.post_send_data_modal.title',
        ).toString(),
        content: t(
          'user.configuration.organization_form.post_send_data_modal.content',
        ).toString(),
      },
    },
    [invitationStepId]: {
      stepComponent: (
        <WizardStep
          id={invitationStepId}
          key={invitationStepId}
          name={t('user.configuration.third_title')}
          children={
            <InvitationForm onCompleteStep={completeStep} isStep={true} />
          }
        />
      ),
      modalData: {
        title: t(
          'user.configuration.invitation_form.invitation_list.post_send_invitation_modal.title',
        ).toString(),
        content: t(
          'user.configuration.invitation_form.invitation_list.post_send_invitation_modal.content',
        ).toString(),
      },
    },
  };

  useEffect(() => {
    if (preConfigurationIsCompleted) {
      const modalData = availableSteps[getCurrentStepId].modalData;
      setModalTitle(modalData.title);
      setModalContent(modalData.content);
      handleModalToggle();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [preConfigurationIsCompleted]);

  useEffect(() => {
    const wizardStepsToRender: React.ReactNode[] = [];
    stepsToCompleteInTheWizard.forEach((stepId: string, index: number) => {
      if (stepId in availableSteps) {
        if (index == 0) {
          setCurrentStepId(
            // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
            availableSteps[stepId].stepComponent.key!.toString(),
          );
        }
        wizardStepsToRender.push(availableSteps[stepId].stepComponent);
      }
    });

    setSteps(wizardStepsToRender);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [stepsToCompleteInTheWizard]);

  return getSteps.length > 0 ? (
    <PageSection className={'configuration-wizard'}>
      <Wizard
        className={'user-configuration-wizard'}
        title={t('user.configuration.main_title').toString()}
        footer={<WizardFooter />}
        header={
          <WizardHeader
            title={t('user.configuration.main_title')}
            hideClose={true}
            titleId={'wizard-main-title'}
          />
        }
        startIndex={indexStepToDisplay}
        onStepChange={(_event, currentStep) => {
          resetValidationRequests()(dispatch);
          setCurrentStepId(currentStep.id.toString());
        }}
        children={getSteps}
      />
      <Modal
        aria-label={'post_send_invitation_modal'}
        variant={ModalVariant.small}
        title={getModalTitle}
        isOpen={isModalOpen}
        onClose={handleModalToggle}
        showClose={false}
      >
        <ModalBoxBody className={'post_send_invitation_modal_body'}>
          {getModalContent}
          <Button
            id={'go_to_home_button'}
            className={'invitation-button'}
            onClick={() => {
              navigate(generalPath);
            }}
          >
            {t(
              'user.configuration.invitation_form.invitation_list.post_send_invitation_modal.button_content',
            )}
          </Button>
        </ModalBoxBody>
      </Modal>
    </PageSection>
  ) : (
    <PageSection style={{ height: '100%' }} />
  );
};
