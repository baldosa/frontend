import { Dispatch, FunctionComponent, useEffect, useState } from 'react';
import { Button, Form } from '@patternfly/react-core';
import { ExtendedTextInput } from '../../../../components/ExtendedTextInput.tsx';
import {
  COUNTRIES,
  EMAIL,
  FULLNAME, GENERAL,
  isAnInvalidFormat,
  isEmpty,
  LANGUAGES,
  NETWORK_ERROR,
  ORGANIZATION,
} from '../../../../components/Auth/ValidationRules.ts';
import { useTranslation } from 'react-i18next';
import { ExtendedSelect } from '../../../../components/ExtendedSelect.tsx';
import {
  changeOrganizationDetailsCountry,
  changeOrganizationDetailsEmail,
  changeOrganizationDetailsLanguage,
  changeOrganizationDetailsName,
  changeOrganizationLogo,
  organizationDetailsCompleted,
  organizationDetailsFailedLoad,
  organizationDetailsLoadedCompletely,
} from '../../../../redux/actions/configuration/initial/organizationDetails.ts';
import { AnyAction } from '@reduxjs/toolkit';
import { useDispatch, useSelector } from 'react-redux';
import {
  getOrganizationCountry,
  getOrganizationEmail,
  getOrganizationName,
  getIsCompleted,
  getIsLoadedCompletely,
  getOrganizationLanguage,
  getOrganizationLogo,
  getRequiresRedirectionStatus,
  organizationStepId,
} from '../../../../redux/reducers/configuration/initial/organizationDetails.ts';
import {
  getOrganizationId as getOrganizationIdFromState,
  getAccessToken,
} from '../../../../redux/reducers/auth.ts';
import { useWizardContext } from '@patternfly/react-core/next';
import { getIndexStepToDisplay } from '../../../../redux/reducers/configuration/preConfiguration.ts';
import { endRedirection } from '../../../../redux/actions/configuration/initial/organizationDetails.ts';
import {
  defineValidationRequestLocation,
  endAction,
  initiateAction,
  resolveBlockingValidationRequest, resolveNonBlockingValidationRequest,
  updateBlockingValidationRequest,
} from '../../../../redux/actions/generalActions.ts';
import {
  getActionState,
  HelperTextData,
  getNonBlockingValidationRequest, allNonBlockingRequestsWereValidated,
} from '../../../../redux/reducers/generalActions.ts';
import { CompleteStepInterface } from '../../InitialConfiguration.tsx';
import { UploadProfileImage } from '../../../../components/UploadProfileImage.tsx';
import {
  isNotNullNotUndefinedOrEqual
} from '../../../../utils/logics/generalUtils.ts';
import { addAlert } from '../../../../redux/actions/alert.ts';
import {
  BasicSelector,
  ComplexSelector,
  OrganizationData,
} from './OrganizationType.ts';
import {
  convertToKeyValueList
} from '../../../../utils/logics/organizationUtils.ts';
import { getCountries, getLanguages } from '../../../../service/generalDetails.ts';
import { useContextClient } from '../../../AppLayout/Contexts.tsx';
import {
  getOrganization,
  updateOrganization,
} from '../../../../service/organization.ts';

export const OrganizationDetailsForm: FunctionComponent<{
  className: string;
  onSubmit?: (params: CompleteStepInterface) => void;
}> = (props) => {
  const [getOrganizationData, setOrganizationData] = useState<OrganizationData>({});

  const [getName, setName] = useState<string | null>(null);
  const [getNewName, setNewName] = useState<string | null>(null);
  const [nameIsModified, setNameIsModified] = useState<boolean>(false);

  const [getEmail, setEmail] = useState<string | null>(null);
  const [getNewEmail, setNewEmail] = useState<string | null>(null);
  const [emailIsModified, setEmailIsModified] = useState<boolean>(false);


  const [getLogo, setLogo] = useState<string | null>(null);
  const [getNewLogo, setNewLogo] = useState<string | null>(null);
  const [logoIsModified, setLogoIsModified] = useState<boolean>(false);

  const [getCountryList, setCountryList] = useState<ComplexSelector[]>([]);
  const [getCountry, setCountry] = useState<BasicSelector | null>(null);
  const [getNewCountry, setNewCountry] = useState<BasicSelector | null>(null);
  const [countryIsModified, setCountryIsModified] = useState<boolean>(false);
  const [isValidCountry, setCountryValidation] = useState<boolean>(false);

  const [getLanguageList, setLanguageList] = useState<ComplexSelector[]>([]);
  const [getLanguage, setLanguage] = useState<BasicSelector | null>(null);
  const [getNewLanguage, setNewLanguage] = useState<BasicSelector | null>(null);
  const [languageIsModified, setLanguageIsModified] = useState<boolean>(false);
  const [isValidLanguage, setLanguageValidation] = useState<boolean>(false);

  const [isSubmitDisabled, setSubmitState] = useState<boolean>(true);

  const { goToNextStep, goToStepByIndex } = useWizardContext();
  const { getOpenAPIClient } = useContextClient();
  const { t } = useTranslation();

  const dispatch: Dispatch<AnyAction> = useDispatch();

  const getTokenFromUser = useSelector(getAccessToken);
  const getEmailFromState = useSelector(getOrganizationEmail);
  const getNameFromState = useSelector(getOrganizationName);
  const getOrganizationId = useSelector(getOrganizationIdFromState);
  const getCountryFromState = useSelector(getOrganizationCountry);
  const getLanguageFromState = useSelector(getOrganizationLanguage);
  const getLogoFromState = useSelector(getOrganizationLogo);
  const isOrganizationStepCompleted = useSelector(getIsCompleted);
  const isLoadedCompletelyFromState = useSelector(getIsLoadedCompletely);
  const isRedirectionRequired = useSelector(getRequiresRedirectionStatus);
  const indexToDisplay = useSelector(getIndexStepToDisplay);
  const isActionInProgress = useSelector(getActionState);
  const nonBlockingValidationRequest: Record<string, HelperTextData> = useSelector(getNonBlockingValidationRequest);
  const everythingWasValidated: boolean = useSelector(allNonBlockingRequestsWereValidated);

  useEffect(() => {
    async function fetchData() {
      const response = await getOrganization(
        getOrganizationId,
        getTokenFromUser,
      )
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      (getOpenAPIClient!)

      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      const organizationData: any = response.data;
      const logo: string | undefined = response.data.logo;

      changeOrganizationDetailsName(response.data.name)(dispatch);
      changeOrganizationDetailsEmail(response.data.email)(dispatch);
      changeOrganizationLogo(
        logo !== undefined && logo.length > 0 ? logo : null,
      )(dispatch);

      await Promise.all([response]);

      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      const languagePromise = getLanguages(getTokenFromUser)(getOpenAPIClient!)
        .then((response) => {
          const conversionResult = convertToKeyValueList(response.data);
          changeOrganizationDetailsLanguage(
            conversionResult.find(
              (value: { key: string | number; value: string }): boolean => {
                return value.key === organizationData.language.id;
              },
            ),
          )(dispatch);
          setLanguageList(conversionResult);
        });

      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      const countryPromise = getCountries(getTokenFromUser)(getOpenAPIClient!)
        .then((response) => {
          const conversionResult = convertToKeyValueList(response.data);
          changeOrganizationDetailsCountry(
            conversionResult.find(
              (value: { key: string | number; value: string }): boolean => {
                return value.key === organizationData.country.iso_code;
              },
            ),
          )(dispatch);
          setCountryList(conversionResult);
        });

      await Promise.all([countryPromise, languagePromise]);
    }

    fetchData()
      .then(() => {
        organizationDetailsLoadedCompletely()(dispatch);
      })
      .catch(() => {
        organizationDetailsFailedLoad()(dispatch);
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (isLoadedCompletelyFromState) {
      setNewLogo(getLogoFromState);
      setNewName(getNameFromState);
      setNewEmail(getEmailFromState);
      setNewCountry(getCountryFromState);
      setNewLanguage(getLanguageFromState);

      setLogo(getLogoFromState);
      setName(getNameFromState);
      setEmail(getEmailFromState);
      setCountry(getCountryFromState);
      setLanguage(getLanguageFromState);

      resolveBlockingValidationRequest([LANGUAGES])(dispatch);
      resolveBlockingValidationRequest([COUNTRIES])(dispatch);
    } else {
      updateBlockingValidationRequest(
        [LANGUAGES],
        NETWORK_ERROR,
        `ERROR_${ORGANIZATION}_UNOBTAINABLE_${LANGUAGES}`,
        'error',
        undefined,
      )(dispatch);
      updateBlockingValidationRequest(
        [COUNTRIES],
        NETWORK_ERROR,
        `ERROR_${ORGANIZATION}_UNOBTAINABLE_${COUNTRIES}`,
        'error',
        undefined,
      )(dispatch);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isLoadedCompletelyFromState]);

  useEffect(() => {
    const organizationData: OrganizationData = { ...getOrganizationData };

    if (isNotNullNotUndefinedOrEqual(getName, getNewName)) {
      setNameIsModified(true);
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      organizationData.name = getNewName!;
    } else {
      setNameIsModified(false);
    }

    if (isNotNullNotUndefinedOrEqual(getEmail, getNewEmail)) {
      setEmailIsModified(true);
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      organizationData.email = getNewEmail!;
    } else {
      setEmailIsModified(false);
    }

    if (getLogo != getNewLogo) {
      setLogoIsModified(true);
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      organizationData.logo = getNewLogo!;
    } else {
      setLogoIsModified(false);
    }

    if (isNotNullNotUndefinedOrEqual(getCountry?.key, getNewCountry?.key)) {
      setCountryIsModified(true);
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      organizationData.country = getNewCountry!.key;
    } else {
      setCountryIsModified(false);
    }

    if (isNotNullNotUndefinedOrEqual(getLanguage?.key, getNewLanguage?.key)) {
      setLanguageIsModified(true);
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      organizationData.language = getNewLanguage!.key;
    } else {
      setLanguageIsModified(false);
    }

    setOrganizationData(organizationData);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [getNewName, getNewEmail, getNewCountry, getNewLanguage, getNewLogo, getName, getEmail, getCountry, getLanguage, getLogo]);

  useEffect(() => {
    setSubmitState(![nameIsModified, emailIsModified, languageIsModified, countryIsModified, logoIsModified].some(value => value) ||
                   ![everythingWasValidated, isValidLanguage, isValidCountry].every((value) => value));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [nameIsModified, emailIsModified, languageIsModified, countryIsModified, logoIsModified, isValidCountry, isValidLanguage, nonBlockingValidationRequest]);



  useEffect(() => {
    props.onSubmit &&
      props.onSubmit({
        stepId: organizationStepId,
        isAwaitingResponse: isActionInProgress,
        isStepCompleted: isOrganizationStepCompleted,
        isRedirectionRequired: isRedirectionRequired,
        firstNavigationAction: goToNextStep,
        secondNavigationAction: () => goToStepByIndex(indexToDisplay),
        stateAction: endRedirection,
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isOrganizationStepCompleted, indexToDisplay, isActionInProgress]);

  const handleSaveProfileButton = (): void => {
    initiateAction()(dispatch);

    if (logoIsModified && getNewLogo == null) {
      getOrganizationData.logo = "";
    }

    updateOrganization(
      getOrganizationId,
      getTokenFromUser,
      getOrganizationData,
    )
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    (getOpenAPIClient!)
      .then(() => {
        organizationDetailsCompleted()(dispatch);

        if (nameIsModified) {
          // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
          changeOrganizationDetailsName(getNewName!)(dispatch);
          setName(getNewName);
        }

        if (emailIsModified) {
          // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
          changeOrganizationDetailsEmail(getNewEmail!)(dispatch);
          setEmail(getNewEmail);
        }

        if (countryIsModified) {
          // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
          changeOrganizationDetailsCountry(getNewCountry!)(dispatch);
          setCountry(getNewCountry);
        }

        if (languageIsModified) {
          // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
          changeOrganizationDetailsLanguage(getNewLanguage!)(dispatch);
          setLanguage(getNewLanguage);
        }

        if (logoIsModified) {
          changeOrganizationLogo(getNewLogo)(dispatch);
          setLogo(getNewLogo);
        }

        addAlert(
          'success',
          t('user.configuration.organization_form.alert.success'),
        )(dispatch);

        setOrganizationData({});
      })
      .catch((error) => {
        defineValidationRequestLocation(error)(dispatch);
        addAlert(
          'danger',
          t('user.configuration.organization_form.alert.error'),
        )(dispatch);
      })
      .finally(() => {
        endAction()(dispatch);
      });
  };

  const onUploadLogo = async (base64: string) => {
    setNewLogo(base64);
    resolveNonBlockingValidationRequest([GENERAL])(dispatch);
  };

  return (
    <Form
      className={props.className}
      key={'wizard_update_organization_form'}
      id={'wizard_update_organization_form'}
    >
      <UploadProfileImage
        title={t('update_image_profile.title_for_media_logo')}
        base64Image={getNewLogo}
        onUpload={onUploadLogo}
      />
      <ExtendedTextInput
        id={'organization_profile_name_text_input'}
        origin={ORGANIZATION}
        fieldNames={[FULLNAME]}
        label={t(
          'user.configuration.organization_form.name_input.label',
        ).toString()}
        isRequired={false}
        type={'text'}
        getValue={getNewName}
        setValue={setNewName}
        placeholder={t(
          'user.configuration.organization_form.name_input.placeholder',
        ).toString()}
        helperText={''}
        rules={[isEmpty, isAnInvalidFormat]}
      />
      <ExtendedTextInput
        id={'organization_profile_email_text_input'}
        origin={ORGANIZATION}
        fieldNames={[EMAIL]}
        label={t(
          'user.configuration.organization_form.email_input.label',
        ).toString()}
        isRequired={false}
        type={'text'}
        getValue={getNewEmail}
        setValue={setNewEmail}
        placeholder={t(
          'user.configuration.organization_form.email_input.placeholder',
        ).toString()}
        helperText={''}
        rules={[isEmpty, isAnInvalidFormat]}
      />
      <ExtendedSelect
        id={'organization_country_select'}
        origin={ORGANIZATION}
        fieldNames={[COUNTRIES]}
        label={t('user.configuration.organization_form.country_select.label')}
        isRequired={false}
        getValue={getNewCountry}
        setValue={setNewCountry}
        placeholder={t(
          'user.configuration.organization_form.country_select.placeholder',
        )}
        isValidated={setCountryValidation}
        helperText={''}
        options={getCountryList}
      />
      <ExtendedSelect
        id={'organization_language_select'}
        origin={ORGANIZATION}
        fieldNames={[LANGUAGES]}
        label={t('user.configuration.organization_form.language_select.label')}
        isRequired={false}
        getValue={getNewLanguage}
        setValue={setNewLanguage}
        placeholder={t(
          'user.configuration.organization_form.language_select.placeholder',
        )}
        helperText={''}
        isValidated={setLanguageValidation}
        options={getLanguageList}
      />
      <Button
        className={'initial-setup-button'}
        isDisabled={isSubmitDisabled}
        onClick={handleSaveProfileButton}
        isLoading={isActionInProgress}
        spinnerAriaValueText={t('general.awaiting_response').toString()}
        id={'handle_save_profile_submit_button'}
        children={t('user.configuration.organization_form.save_button_content')}
      />
    </Form>
  );
};
