export interface OrganizationData {
  email?: string;
  name?: string;
  country?: string;
  language?: string;
  logo?: string;
}

export interface BasicSelector {
  key: string;
  value: string
}

export interface ComplexSelector {
  key: string | number;
  value: string
}

