import React, { Dispatch, useEffect, useState } from 'react';
import { Button, Form } from '@patternfly/react-core';
import { useTranslation } from 'react-i18next';
import { ExtendedTextInput } from '../../../components/ExtendedTextInput.tsx';
import {
  EMAIL,
  FULLNAME,
  isAnInvalidFormat,
  isEmpty,
  PROFILE,
} from '../../../components/Auth/ValidationRules.ts';
import { useDispatch, useSelector } from 'react-redux';
import {
  userProfileCompleted,
  changeUserProfileEmail,
  changeUserProfileUsername,
  endRedirection,
  changeUserProfileAvatar,
} from '../../../redux/actions/configuration/initial/userProfileDetails.ts';
import { AnyAction } from '@reduxjs/toolkit';
import { getAccessToken, getUserId } from '../../../redux/reducers/auth.ts';
import {
  getAvatar,
  getFullName,
  getIsLoadedCompletely,
  getRequiresRedirectionStatus,
  getUserEmail,
  userProfileStepId,
} from '../../../redux/reducers/configuration/initial/userProfileDetails.ts';
import { useWizardContext } from '@patternfly/react-core/next';
import {
  defineValidationRequestLocation,
  endAction,
  initiateAction,
} from '../../../redux/actions/generalActions.ts';
import {
  getActionState,
  getBlockingValidationRequest,
  HelperTextData,
  allBlockingRequestsWereValidated,
} from '../../../redux/reducers/generalActions.ts';
import { getIsCompleted } from '../../../redux/reducers/configuration/initial/userProfileDetails.ts';
import { getIndexStepToDisplay } from '../../../redux/reducers/configuration/preConfiguration.ts';
import { CompleteStepInterface } from '../InitialConfiguration.tsx';
import { UploadProfileImage } from '../../../components/UploadProfileImage.tsx';
import { saveUserData } from '../../../utils/logics/userUtils.ts';
import { isNotNullNotUndefinedOrEqual } from '../../../utils/logics/generalUtils.ts';
import { addAlert } from '../../../redux/actions/alert.ts';
import { useContextClient } from '../../AppLayout/Contexts.tsx';
import { updateUserData } from '../../../service/user.ts';

interface UserData {
  email?: string;
  full_name?: string;
  avatar?: string;
}

export const UserProfileForm: React.FunctionComponent<{
  className: string;
  onSubmit?: (params: CompleteStepInterface) => void;
}> = (props: {
  className: string;
  onSubmit?: (params: CompleteStepInterface) => void;
}) => {
  const getUserLoggedId = useSelector(getUserId);
  const getTokenFromUser = useSelector(getAccessToken);
  const indexToDisplay = useSelector(getIndexStepToDisplay);
  const getUserAvatarFromState = useSelector(getAvatar);
  const getFullNameFromState = useSelector(getFullName);
  const getUserEmailFromState = useSelector(getUserEmail);
  const isLoadedCompletelyFromState = useSelector(getIsLoadedCompletely);
  const isUserProfileStepCompleted = useSelector(getIsCompleted);
  const isRedirectionRequired = useSelector(getRequiresRedirectionStatus);
  const isActionInProgress = useSelector(getActionState);
  const blockingValidationRequest: Record<string, HelperTextData> = useSelector(
    getBlockingValidationRequest,
  );
  const everythingWasValidated: boolean = useSelector(
    allBlockingRequestsWereValidated,
  );

  const [getUserData, setUserData] = useState<UserData>({});
  const [isSubmitDisabled, setSubmitState] = useState(true);
  const [getEmail, setEmail] = useState<string | null>(null);
  const [getUserAvatar, setUserAvatar] = useState<string | null>(null);
  const [getProfileName, setProfileName] = useState<string | null>(null);

  const dispatch: Dispatch<AnyAction> = useDispatch();
  const { goToNextStep, goToStepByIndex } = useWizardContext();
  const { getOpenAPIClient } = useContextClient();
  const { t } = useTranslation();

  useEffect(() => {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    saveUserData(getUserLoggedId, getTokenFromUser)(dispatch, getOpenAPIClient!);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (isLoadedCompletelyFromState) {
      setUserAvatar(getUserAvatarFromState);
      setProfileName(getFullNameFromState);
      setEmail(getUserEmailFromState);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isLoadedCompletelyFromState]);

  useEffect(() => {
    if (isNotNullNotUndefinedOrEqual(getUserEmailFromState, getEmail)) {
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      changeUserProfileEmail(getEmail!)(dispatch);
      const userData: UserData = { ...getUserData };
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      userData.email = getEmail!;
      setUserData(userData);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [getEmail]);

  useEffect(() => {
    if (isNotNullNotUndefinedOrEqual(getFullNameFromState, getProfileName)) {
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      changeUserProfileUsername(getProfileName!)(dispatch);
      const userData: UserData = { ...getUserData };
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      userData.full_name = getProfileName!;
      setUserData(userData);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [getProfileName]);

  useEffect(() => {
    if (getUserAvatar != null) {
      changeUserProfileAvatar(getUserAvatar)(dispatch);
      const userData: UserData = { ...getUserData };
      userData.avatar = getUserAvatar;
      setUserData(userData);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [getUserAvatar]);

  useEffect(() => {
    setSubmitState(!everythingWasValidated);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [blockingValidationRequest]);

  useEffect(() => {
    props.onSubmit &&
      props.onSubmit({
        stepId: userProfileStepId,
        isAwaitingResponse: isActionInProgress,
        isStepCompleted: isUserProfileStepCompleted,
        isRedirectionRequired: isRedirectionRequired,
        firstNavigationAction: goToNextStep,
        secondNavigationAction: () => goToStepByIndex(indexToDisplay),
        stateAction: endRedirection,
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isUserProfileStepCompleted, indexToDisplay, isActionInProgress]);

  const handleSaveProfileButton = () => {
    initiateAction()(dispatch);

    if (isValidParams(getUserData)) {
      updateUserData(getUserLoggedId, getTokenFromUser, getUserData)
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      (getOpenAPIClient!)
        .then(() => {
          userProfileCompleted()(dispatch);
          addAlert(
            'success',
            t('user.configuration.profile_form.alert.success'),
          )(dispatch);
        })
        .catch((error) => {
          defineValidationRequestLocation(error)(dispatch);
          addAlert(
            'danger',
            t('user.configuration.profile_form.alert.error'),
          )(dispatch);
        })
        .finally(() => {
          endAction()(dispatch);
        });
    }
  };

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const isValidParams = (params: any) => {
    return Object.keys(params).length > 0;
  };

  const onUploadAvatar = async (base64: string) => {
    setUserAvatar(base64);
  };

  return (
    <Form
      key={'wizard_update_user_form'}
      id={'wizard_update_user_form'}
      className={props.className}
    >
      {
        // FIXME: When the user load can be used elsewhere we re-enable it
        false && (
          <UploadProfileImage
            title={t('update_image_profile.title_for_user_avatar')}
            base64Image={getUserAvatar}
            onUpload={onUploadAvatar}
          />
        )
      }
      <ExtendedTextInput
        id={'username_profile_name_text_input'}
        origin={PROFILE}
        fieldNames={[FULLNAME]}
        label={t('user.configuration.profile_form.name_input.label').toString()}
        isRequired={true}
        type={'text'}
        getValue={getProfileName}
        setValue={setProfileName}
        placeholder={t(
          'user.configuration.profile_form.name_input.placeholder',
        ).toString()}
        helperText={''}
        rules={[isEmpty, isAnInvalidFormat]}
      />
      <ExtendedTextInput
        id={'username_email_text_input'}
        origin={PROFILE}
        fieldNames={[EMAIL]}
        label={t(
          'user.configuration.profile_form.email_input.label',
        ).toString()}
        isRequired={true}
        type={'text'}
        getValue={getEmail}
        setValue={setEmail}
        placeholder={t(
          'user.configuration.profile_form.email_input.placeholder',
        ).toString()}
        helperText={''}
        rules={[isEmpty, isAnInvalidFormat]}
      />
      <Button
        id={'save_my_profile_submit_button'}
        className={'initial-setup-button'}
        isDisabled={isSubmitDisabled}
        onClick={handleSaveProfileButton}
        isLoading={isActionInProgress}
        spinnerAriaValueText={t('general.awaiting_response').toString()}
        children={t('user.configuration.profile_form.save_button_content')}
      />
    </Form>
  );
};
