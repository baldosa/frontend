import { FunctionComponent } from 'react';
import {
  Button,
  Card,
  CardActions,
  CardBody,
  CardHeader,
  Title,
} from '@patternfly/react-core';
import TimesIcon from '@patternfly/react-icons/dist/esm/icons/times-icon';

export interface Invitation {
  id: number | null;
  fullName: string | null;
  email: string | null;
  accountType: string | null;
  group_id: number;
}

export interface InvitationView extends Invitation {
  removeInvitation?: (key: number) => void;
}

export const InvitationItem: FunctionComponent<InvitationView> = (props) => {
  return (
    <Card isFlat className={'invitation-structure'}>
      <CardHeader>
        <CardActions>
          <Button
            variant={'plain'}
            id={`delete_${props.id}_button`}
            /* eslint-disable-next-line @typescript-eslint/no-non-null-assertion */
            onClick={() => props.removeInvitation?.(props.id!)}
            children={<TimesIcon />}
          />
        </CardActions>
      </CardHeader>
      <CardBody>
        <Title headingLevel={'h6'}>{props.fullName}</Title>
        <Title headingLevel={'h6'}>{props.email}</Title>
        <Title headingLevel={'h6'}>{props.accountType}</Title>
      </CardBody>
    </Card>
  );
};
