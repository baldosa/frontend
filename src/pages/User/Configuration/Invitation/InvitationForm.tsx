import React, {
  Dispatch,
  Fragment,
  FunctionComponent,
  useEffect,
  useState,
} from 'react';
import {
  Button,
  Form,
  Hint,
  HintBody,
  HintFooter,
  HintTitle,
  Modal,
  Title,
} from '@patternfly/react-core';
import { useTranslation } from 'react-i18next';
import { ExtendedTextInput } from '../../../../components/ExtendedTextInput.tsx';
import {
  ACCOUNT_TYPE,
  EMAIL,
  FULLNAME,
  INTERNAL_ERROR,
  INVITATION,
  isAnInvalidFormat,
  isEmpty,
  NETWORK_ERROR,
  PROFILE,
} from '../../../../components/Auth/ValidationRules.ts';
import { ExtendedSelect } from '../../../../components/ExtendedSelect.tsx';
import { InvitationList } from './InvitationList.tsx';
import { useDispatch, useSelector } from 'react-redux';
import {
  getInvitationListLength,
  getGeneratedInvitationsTotal,
  getIsCompleted,
  getInvitationList,
  invitationStepId,
  getRequiresRedirectionStatus,
} from '../../../../redux/reducers/configuration/initial/invitationSection.ts';
import { AnyAction } from '@reduxjs/toolkit';
import {
  addInvitation,
  endRedirection,
  invitationSectionCompleted,
  invitationSectionUncompleted,
} from '../../../../redux/actions/configuration/initial/invitationSection.ts';
import { Invitation } from './InvitationItem.tsx';

import {
  getEmail as userEmailFromState,
  getOrganizationId as getOrganizationIdFromState,
  getAccessToken,
} from '../../../../redux/reducers/auth.ts';
import { useWizardContext } from '@patternfly/react-core/next';
import { getIndexStepToDisplay } from '../../../../redux/reducers/configuration/preConfiguration.ts';
import {
  getActionState,
  getBlockingValidationRequest,
  HelperTextData,
  allBlockingRequestsWereValidated,
} from '../../../../redux/reducers/generalActions.ts';
import { CompleteStepInterface } from '../../InitialConfiguration.tsx';
import { ExtendedHelperText } from '../../../../components/ExtendedHelperText.tsx';
import {
  endAction,
  initiateAction,
  resolveBlockingValidationRequest,
  resolveNonBlockingValidationRequest,
  updateNonBlockingValidationRequest,
} from '../../../../redux/actions/generalActions.ts';
import { Components } from '../../../../api/utilities/Definitions';
import { useContextClient } from '../../../AppLayout/Contexts.tsx';
import { getGroups } from '../../../../service/generalDetails.ts';
import { getMembersOrganizations } from '../../../../service/organization.ts';

export const InvitationForm: FunctionComponent<{
  className?: string;
  onCompleteStep?: (params: CompleteStepInterface) => void;
  onSubmit?: () => void;
  isStep: boolean;
}> = (props) => {
  const [getFullName, setFullName] = useState<string | null>(null);
  const [getEmail, setEmail] = useState<string | null>(null);
  const [getAccountTypeList, setAccountTypeList] = useState<
    { key: number; value: string }[]
  >([]);
  const [getSelectedValue, setSelectedValue] = useState<{
    key: number;
    value: string;
  } | null>(null);
  const [isValidAccountType, setAccountTypeValidation] =
    useState<boolean>(false);
  const [isSubmitDisabled, setSubmitState] = useState<boolean>(true);
  const [isModalOpen, setIsModalOpen] = useState<boolean>(false);
  const [isAddInvitationViewOpen, setAddInvitationViewOpen] =
    useState<boolean>(true);
  const [getOrganizationMembers, setOrganizationMembers] = useState<
    Components.Schemas.UserOrganizationGroup[]
  >([]);
  const totalNumberOfInvitations = useSelector(getInvitationListLength);
  const isActionInProgress = useSelector(getActionState);
  const indexToDisplay = useSelector(getIndexStepToDisplay);
  const getOrganizationId = useSelector(getOrganizationIdFromState);
  const getInvitationIndex = useSelector(getGeneratedInvitationsTotal);
  const isRedirectionRequired = useSelector(getRequiresRedirectionStatus);
  const isInvitationStepCompleted = useSelector(getIsCompleted);
  const invitationList = useSelector(getInvitationList);
  const getTokenFromUser = useSelector(getAccessToken);
  const userEmail = useSelector(userEmailFromState);
  const blockingValidationRequest: Record<string, HelperTextData> = useSelector(
    getBlockingValidationRequest,
  );
  const everythingWasValidated: boolean = useSelector(
    allBlockingRequestsWereValidated,
  );

  const dispatch: Dispatch<AnyAction> = useDispatch();

  const { goToPrevStep, goToStepByIndex } = useWizardContext();
  const { getOpenAPIClient } = useContextClient();
  const { t } = useTranslation();

  useEffect(() => {
    getMembersOrganizations(getOrganizationId, getTokenFromUser)
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    (getOpenAPIClient!)
      .then((response) => {
        const activeUsers: Components.Schemas.UserOrganizationGroup[] =
          response.data.active.users;
        const suspendedUsers: Components.Schemas.UserOrganizationGroup[] =
          response.data.suspended.users;
        setOrganizationMembers([...activeUsers, ...suspendedUsers]);
      });

    if (totalNumberOfInvitations) {
      setAddInvitationViewOpen(false);
    }
    setAccountTypes();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    setSubmitState(
      ![isValidAccountType, everythingWasValidated].every((value) => value),
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isValidAccountType, blockingValidationRequest]);

  useEffect(() => {
    if (props.onCompleteStep) {
      props.onCompleteStep({
        stepId: invitationStepId,
        isAwaitingResponse: isActionInProgress,
        isStepCompleted: isInvitationStepCompleted,
        isRedirectionRequired: isRedirectionRequired,
        firstNavigationAction: goToPrevStep,
        secondNavigationAction: () => goToStepByIndex(indexToDisplay),
        stateAction: endRedirection,
      });
    }
    if (props.onSubmit && isInvitationStepCompleted) {
      invitationSectionUncompleted()(dispatch);
      props.onSubmit();
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isInvitationStepCompleted, indexToDisplay, isActionInProgress]);

  const setAccountTypes = (): void => {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    getGroups(getTokenFromUser)(getOpenAPIClient!)
      .then((response): void => {
        resolveBlockingValidationRequest([ACCOUNT_TYPE]);
        setAccountTypeList(
          response.data.map((item) => ({ key: item.id, value: item.name })),
        );
      })
      .catch((): void => {
        setAccountTypeList([]);
        updateNonBlockingValidationRequest(
          [ACCOUNT_TYPE],
          NETWORK_ERROR,
          `ERROR_${INVITATION}_UNOBTAINABLE_${ACCOUNT_TYPE}`,
          'error',
          undefined,
        )(dispatch);
      });
  };

  const handleModalToggle = () => {
    setIsModalOpen(!isModalOpen);
  };

  const handleAddInvitation = (
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    _event: React.MouseEvent<HTMLButtonElement>,
  ): void => {
    setIsModalOpen(!isModalOpen);
  };

  const handleAcceptInvitationData = (
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    _event: React.MouseEvent<HTMLButtonElement>,
  ): void => {
    if (getEmail !== userEmail) {
      if (
        !getOrganizationMembers.some(
          (member: Components.Schemas.UserOrganizationGroup) =>
            getEmail === member.user.email,
        ) &&
        !invitationList.some(
          (invitation: Invitation) => invitation.email === getEmail,
        )
      ) {
        handleModalToggle();
        addInvitation({
          id: getInvitationIndex,
          // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
          fullName: getFullName!,
          // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
          email: getEmail!,
          // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
          accountType: getSelectedValue!.value,
          // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
          group_id: getSelectedValue!.key,
        })(dispatch);
        resetForm();
      } else {
        updateNonBlockingValidationRequest(
          [EMAIL],
          INTERNAL_ERROR,
          `ERROR_${INVITATION}_${EMAIL}_ALREADY_INVITED`,
          'error',
          undefined,
        )(dispatch);
      }
    } else {
      updateNonBlockingValidationRequest(
        [EMAIL],
        INTERNAL_ERROR,
        `ERROR_${INVITATION}_OWN_${EMAIL}`,
        'error',
        undefined,
      )(dispatch);
    }
  };

  const handleEditInvitationData = (
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    _event: React.MouseEvent<HTMLButtonElement>,
  ): void => {
    resolveNonBlockingValidationRequest([EMAIL])(dispatch);
    handleModalToggle();
  };

  const openAddInvitationView = (): void => {
    setAddInvitationViewOpen(true);
  };

  const resetForm = (): void => {
    setAddInvitationViewOpen(false);
    setFullName(null);
    setEmail(null);
    setSelectedValue(null);
    setAccountTypeValidation(false);
  };

  const handleSkipStep = (): void => {
    async function handleSkipStep() {
      initiateAction()(dispatch);
      invitationSectionCompleted()(dispatch);
    }
    handleSkipStep().finally(() => endAction()(dispatch));
  };

  return totalNumberOfInvitations && !isAddInvitationViewOpen ? (
    <InvitationList openAddInvitationView={openAddInvitationView} />
  ) : (
    <Form
      key={'wizard_invitation_form'}
      id={'wizard_invitation_form'}
      className={props.className}
    >
      {props.className ? (
        <Fragment />
      ) : (
        <Title headingLevel={'h3'}>
          {t('user.configuration.invitation_form.main_title')}
        </Title>
      )}
      <ExtendedTextInput
        id={'username_invitation_text_input'}
        origin={PROFILE}
        fieldNames={[FULLNAME]}
        label={t(
          'user.configuration.invitation_form.name_input.label',
        ).toString()}
        isRequired={true}
        type={'text'}
        getValue={getFullName}
        setValue={setFullName}
        placeholder={t(
          'user.configuration.invitation_form.name_input.placeholder',
        ).toString()}
        helperText={''}
        rules={[isEmpty, isAnInvalidFormat]}
      />
      <ExtendedTextInput
        id={'email_invitation_text_input'}
        origin={PROFILE}
        fieldNames={[EMAIL]}
        label={t(
          'user.configuration.invitation_form.email_input.label',
        ).toString()}
        isRequired={true}
        type={'text'}
        getValue={getEmail}
        setValue={setEmail}
        placeholder={t(
          'user.configuration.invitation_form.email_input.placeholder',
        ).toString()}
        helperText={''}
        rules={[isEmpty, isAnInvalidFormat]}
      />
      <ExtendedSelect
        id={'role_invitation_select'}
        label={t(
          'user.configuration.invitation_form.account_type_select.label',
        )}
        origin={INVITATION}
        fieldNames={[ACCOUNT_TYPE]}
        isRequired={true}
        getValue={getSelectedValue}
        setValue={setSelectedValue}
        placeholder={t(
          'user.configuration.invitation_form.account_type_select.placeholder',
        )}
        helperText={''}
        isValidated={setAccountTypeValidation}
        options={getAccountTypeList}
      />
      <div style={{ display: 'flex' }}>
        <Button
          id={'add_invitation_button'}
          className={'initial-setup-button'}
          isDisabled={isSubmitDisabled}
          onClick={handleAddInvitation}
          children={t('user.configuration.invitation_form.save_button_content')}
        />
        {props.isStep && (
          <Button
            id={'not_inviting_anyone_button'}
            className={'initial-setup-button'}
            onClick={handleSkipStep}
            children={t('user.configuration.invitation_form.not_inviting')}
          />
        )}
      </div>
      <Modal
        aria-label={'view-invitation-data'}
        className={'view-invitation-data'}
        aria-describedby="modal-no-header-description"
        isOpen={isModalOpen}
        style={{ padding: 0, margin: 0 }}
        showClose={false}
      >
        <Hint aria-label={'view-invitation-data'}>
          <HintTitle>
            {t('user.configuration.invitation_form.hint.title')}
          </HintTitle>
          <HintBody>
            {getFullName}
            <br />
            {getEmail}
            <br />
            {getSelectedValue?.value}
            <ExtendedHelperText
              origin={INVITATION}
              fieldNames={[EMAIL]}
              validationRules={[]}
            />
          </HintBody>
          <HintFooter>
            <Button
              variant={'plain'}
              id={'edit_invitation_data_button'}
              className={'button_invitation_data'}
              onClick={handleEditInvitationData}
              children={t('user.configuration.invitation_form.hint.edit')}
            />
            <Button
              variant={'plain'}
              id={'accept_invitation_data_button'}
              className={'button_invitation_data'}
              onClick={handleAcceptInvitationData}
              children={t('user.configuration.invitation_form.hint.accept')}
            />
          </HintFooter>
        </Hint>
      </Modal>
    </Form>
  );
};
