import {
  Dispatch,
  FunctionComponent,
  ReactElement,
  useEffect,
  useState,
} from 'react';
import { Button, Menu, MenuContent, MenuList } from '@patternfly/react-core';
import { useDispatch, useSelector } from 'react-redux';
import { getInvitationList } from '../../../../redux/reducers/configuration/initial/invitationSection.ts';
import { Invitation, InvitationItem } from './InvitationItem.tsx';
import { AnyAction } from '@reduxjs/toolkit';
import {
  invitationSectionCompleted,
  removeInvitation,
} from '../../../../redux/actions/configuration/initial/invitationSection.ts';
import { useTranslation } from 'react-i18next';
import {
  getOrganizationId as organizationIdFromState,
  getAccessToken,
} from '../../../../redux/reducers/auth.ts';
import { getActionState } from '../../../../redux/reducers/generalActions.ts';
import {
  endAction,
  initiateAction,
} from '../../../../redux/actions/generalActions.ts';
import { addAlert } from '../../../../redux/actions/alert.ts';
import { useContextClient } from '../../../AppLayout/Contexts.tsx';
import {
  sendOrganizationInvitations
} from '../../../../service/organization.ts';


export const InvitationList: FunctionComponent<{
  openAddInvitationView: () => void;
}> = (props) => {
  const [getInvitations, setInvitations] = useState<ReactElement[]>([]);

  const invitationList = useSelector(getInvitationList);
  const getTokenFromUser = useSelector(getAccessToken);
  const getOrganizationId = useSelector(organizationIdFromState);
  const isActionInProgress = useSelector(getActionState);

  const dispatch: Dispatch<AnyAction> = useDispatch();
  const { getOpenAPIClient } = useContextClient();
  const { t } = useTranslation();

  useEffect(() => {
    setInvitations(
      invitationList.map((invitation: Invitation) => (
        <InvitationItem
          key={`invitation_key_${invitation.id}`}
          id={invitation.id}
          fullName={invitation.fullName}
          email={invitation.email}
          accountType={invitation.accountType}
          group_id={invitation.group_id}
          removeInvitation={removeInvitationFromList}
        />
      )),
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [invitationList]);

  const removeInvitationFromList = (invitationId: number) => {
    removeInvitation(invitationId)(dispatch);
  };

  const sendInvitations = () => {
    initiateAction()(dispatch);

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const organizationMemberInvitations: any = [];
    invitationList.forEach((invitation: Invitation) =>
      organizationMemberInvitations.push({
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        full_name: invitation.fullName!,
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        email: invitation.email!,
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        group_id: invitation.group_id!,
      }),
    );

    sendOrganizationInvitations(
      getOrganizationId,
      getTokenFromUser,
      organizationMemberInvitations,
    )
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    (getOpenAPIClient!)
      .then(() => {
        invitationSectionCompleted()(dispatch);
        addAlert(
          'success',
          t('user.configuration.invitation_form.invitation_list.alert.success'),
        )(dispatch);
      })
      .catch(() => {
        addAlert(
          'danger',
          t('user.configuration.invitation_form.invitation_list.alert.error'),
        )(dispatch);
      })
      .finally(() => {
        endAction()(dispatch);
      });
  };

  return (
    <div className={'invitation-content-structure'}>
      <div className={'invitation-button-panel'}>
        <div className={'content'}>
          <Button
            variant={'tertiary'}
            className={'invitation-button'}
            id={'open_invitation_view_button'}
            onClick={() => props.openAddInvitationView()}
            children={t(
              'user.configuration.invitation_form.invitation_list.add_invitation_view_button',
            )}
          />
          <Button
            className={'invitation-button'}
            id="send_invitations_submit_button"
            onClick={sendInvitations}
            isLoading={isActionInProgress}
            spinnerAriaValueText={t('general.awaiting_response').toString()}
            children={t(
              'user.configuration.invitation_form.invitation_list.send_invitation_button',
            )}
          />
        </div>
      </div>
      <Menu isPlain isScrollable className={'invitation-list-content'}>
        <MenuContent>
          <MenuList>{getInvitations}</MenuList>
        </MenuContent>
      </Menu>
    </div>
  );
};
