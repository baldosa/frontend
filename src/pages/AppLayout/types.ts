import OpenAPIClientAxios from 'openapi-client-axios';
import { ReactElement } from 'react';
import ee from 'event-emitter';

export type ContextPropsType = {
  emitter: ee.Emitter | undefined;
};

export type ContextClientType = {
  getOpenAPIClient: OpenAPIClientAxios | undefined;
  setOpenAPIClient: ((client: OpenAPIClientAxios) => void) | undefined;
};

export type HandleProperties = {
  footer: string | undefined;
  header: string | undefined;
  background: string | undefined;
  title: string;
  showCompletely: boolean;
  showNavMenu: boolean;
  isPathTraceable: boolean;
  actionsMenu: ReactElement[];
  contextProps: ContextPropsType;
};