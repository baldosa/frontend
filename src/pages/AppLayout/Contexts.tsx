import {
  createContext,
  FunctionComponent,
  ReactElement,
  useContext,
} from 'react';
import { ContextClientType, ContextPropsType } from './types.ts';

export const ContextPropsContext = createContext<ContextPropsType | undefined>(undefined);

export const ContextPropsProvider: FunctionComponent<{
  contextProps: ContextPropsType | undefined;
  children: ReactElement[];
}> = (props) => {
  return <ContextPropsContext.Provider value={props.contextProps} children={props.children} />
};

export const ContextClient = createContext<ContextClientType>({
  getOpenAPIClient: undefined,
  setOpenAPIClient: undefined
});

export const ContextClientProvider: FunctionComponent<{
  contextProps: ContextClientType;
  children: ReactElement;
}> = (props) => {
  return <ContextClient.Provider value={props.contextProps} children={props.children} />
};

// eslint-disable-next-line react-refresh/only-export-components
export const useContextClient = () => {
  return useContext(ContextClient);
};
