import { useLocation, Outlet, useMatches, Params } from 'react-router-dom';
import {
  BreadcrumbItem,
  Page,
  PageSidebar,
  Title,
} from '@patternfly/react-core';
import { useDocumentTitle } from '../../utils/useDocumentTitle';
import { useTranslation } from 'react-i18next';
import { MainNavigationMenu, Navigation } from '../../routes.tsx';
import {
  Dispatch,
  Fragment, FunctionComponent,
  ReactElement,
  useEffect,
  useState,
} from 'react';
import { preConfigurationLoadData } from '../../redux/actions/configuration/preConfiguration.ts';
import { AnyAction } from '@reduxjs/toolkit';
import { useDispatch, useSelector } from 'react-redux';
import {
  getOrganizationId,
  getAccessToken,
  getUserId,
  isLoggedIn, serverIdKey,
} from '../../redux/reducers/auth.ts';
import { resetValidationRequests } from '../../redux/actions/generalActions.ts';
import {
  getHeaderDescription,
  getHeaderName,
  getHeaderNavigationDestination,
} from '../../redux/reducers/generalActions.ts';
import auth_page_colmena_logo_footer from '../../assets/pages/Auth/auth_page_colmena_logo_footer.png';
import { isLandscape } from '../../utils/LayoutUtils.tsx';
import { GenericHeader, MainHeader } from './header/Header.tsx';
import {
  audioStore,
  IndexedDBManager,
  mediaDataBase, serverDataBase, serverStore,
  visualStore,
} from '../../utils/logics/indexedDBManager.ts';
import { saveOrganizationData } from '../../utils/logics/organizationUtils.ts';
import { saveUserData } from '../../utils/logics/userUtils.ts';
import { AlertManager } from '../../utils/AlertManager.tsx';
import { updateServerData } from '../../redux/actions/auth.ts';
import { getServerData } from '../../utils/logics/authUtils.ts';
import { ContextPropsType, HandleProperties } from './types.ts';
import { useContextClient } from './Contexts.tsx';

export const Home = 'Home';
export const Main = 'Main';
export const ActionMenus = 'ActionMenus';
export const BreadCrumb = 'BreadCrumb';
export const Tools = 'Tools';
export const AuthFooterKey = 'AuthFooter';
export const TeamList = 'TeamList';

export const AppLayout: FunctionComponent = () => {
  const [getContextProps, setContextProps] = useState<ContextPropsType | undefined>(undefined);
  const [getNavigationMenu, setNavigationMenu] = useState<ReactElement>(<Fragment />);
  const [getTraceablePaths, setTraceablePaths] = useState<ReactElement[]>([]);
  const [getActionsMenus, setActionsMenus] = useState<ReactElement[]>([]);
  const [showCompletely, setShowCompletely] = useState<boolean>(true);
  const [showNavMenu, setShowNavMenu] = useState<boolean>(false);
  const [isNavOpen, setOpenNav] = useState<boolean>(false);
  const [getBackground, setBackground] = useState<string>('');
  const [getHeader, setHeader] = useState<string>('');
  const [getFooter, setFooter] = useState<string>('');
  const [getTitle, setTitle] = useState<string>('');

  const getUserLoggedId: number = useSelector(getUserId);
  const getTokenFromUser: string = useSelector(getAccessToken);
  const isLogged: boolean = useSelector(isLoggedIn);
  const headerName: string = useSelector(getHeaderName);
  const organizationId: number = useSelector(getOrganizationId);
  const headerDescription: string = useSelector(getHeaderDescription);
  const navigationDestination: string = useSelector(getHeaderNavigationDestination);

  const matches: {
    id: string;
    pathname: string;
    params: Params;
    data: unknown;
    handle: unknown;
  }[] = useMatches();

  const dispatch: Dispatch<AnyAction> = useDispatch();
  const location = useLocation();

  const { getOpenAPIClient } = useContextClient();
  const { t } = useTranslation();

  useEffect(() => {
    IndexedDBManager.createObjectStore(mediaDataBase, audioStore).then(() => {
      IndexedDBManager.createObjectStore(mediaDataBase, visualStore);
    });
    if (getUserLoggedId && getOpenAPIClient) {
      saveUserData(getUserLoggedId, getTokenFromUser)
      (dispatch, getOpenAPIClient);
    }
    if (organizationId && getOpenAPIClient) {
      saveOrganizationData(organizationId, getTokenFromUser)
      (dispatch, getOpenAPIClient);
    }
    IndexedDBManager.createObjectStore(serverDataBase, serverStore, true);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect((): void => {
    if (isLogged) preConfigurationLoadData()(dispatch);
    resetValidationRequests()(dispatch);
    getHandleFromRoute();
    setOpenNav(false);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [location.pathname]);

  useEffect(() => {
    const serverId: string | null = localStorage.getItem(serverIdKey);

    if(serverId) {
      getServerData(Number(serverId))
        .then((value) => updateServerData(value.id, value.name, value.url)(dispatch))
        .catch((error) => console.error(error));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const onNavToggle = () => {
    setOpenNav(!isNavOpen);
  };

  const Sidebar: ReactElement = (
    <PageSidebar
      style={{ height: 'inherit' }}
      theme="dark"
      nav={<Navigation />}
      isNavOpen={isNavOpen}
    />
  );

  const AuthFooter: ReactElement = (
    <div className={'auth-page-footer'}>
      <Title
        headingLevel={'h6'}
        children={t('auth_page.card_footer.description')}
      />
      <img
        id={'colmena-logo'}
        height={'15px'}
        src={auth_page_colmena_logo_footer}
        alt={'colemena logo'}
      />
    </div>
  );

  const getHeaderFromName = (name: string | undefined): ReactElement => {
    switch (name) {
      case Main:
        return <MainHeader onNavToggle={onNavToggle} />;
      case ActionMenus:
        return (
          <GenericHeader
            lightModeIsActive={false}
            mainTitle={headerName}
            secondaryTitle={headerDescription}
            navigationDestination={navigationDestination}
            actionMenus={getActionsMenus}
            contextProps={getContextProps}
          />
        );
      case BreadCrumb:
        return (
          <GenericHeader
            lightModeIsActive={true}
            mainTitle={headerName}
            secondaryTitle={headerDescription}
            navigationDestination={navigationDestination}
            breadCrumb={getTraceablePaths}
            contextProps={getContextProps}
          />
        );
      default:
        return <Fragment />;
    }
  };

  const getBackgroundFromName = (name: string | undefined): string => {
    switch (name) {
      case Home:
        return 'home-background';
      case ActionMenus:
      case BreadCrumb:
        return 'util-background';
      case Tools:
        return 'tools-background';
      case TeamList:
        return 'team-list-background';
      default:
        return '';
    }
  };

  const getFooterFromName = (name: string | undefined): ReactElement => {
    switch (name) {
      case AuthFooterKey:
        return AuthFooter;
      default:
        return <Fragment />;
    }
  };

  const getHandleFromRoute = (): void => {
    const handle: HandleProperties = matches.find(
      (route): boolean => location.pathname === route.pathname,
    )?.handle as HandleProperties;
    const header: string = handle.header ? handle.header : '';
    const footer: string = handle.footer ? handle.footer : '';
    const background: string = handle.background ? handle.background : '';
    const actionsMenu: ReactElement[] = handle.actionsMenu
      ? handle.actionsMenu
      : [];
    const contextProps: ContextPropsType | undefined = handle.contextProps
      ? handle.contextProps
      : undefined;
    const isPathTraceable: boolean = handle.isPathTraceable
      ? handle.isPathTraceable
      : false;

    setHeader(header);
    setFooter(footer);
    setContextProps(contextProps);
    setActionsMenus(actionsMenu);
    setBackground(background);
    setTitle(handle.title);
    setShowNavMenu(handle.showNavMenu);
    setShowCompletely(handle.showCompletely);

    if (isPathTraceable) {
      const traceablePaths: ReactElement[] = getTraceablePaths.slice();
      const indexOfElement: number = traceablePaths.findIndex(
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        (element: any): boolean => {
          return element.props.to === location.pathname;
        },
      );

      if (indexOfElement !== -1) {
        traceablePaths.splice(indexOfElement + 1);
      } else {
        traceablePaths.push(
          <BreadcrumbItem
            key={`breadcrumb-item-${location.pathname}`}
            to={location.pathname}
            children={t(handle.title)}
          />
        );
      }

      setTraceablePaths(traceablePaths);
    }
  };
  useDocumentTitle(t(`${getTitle ? 'title.main' : getTitle}`));

  useEffect(() => {
    if (!showNavMenu) {
      setNavigationMenu(<div></div>);
    }
  }, [showNavMenu]);

  const MobileMenu: ReactElement = showNavMenu ? (
    <MainNavigationMenu variant={'horizontal'} className={'horizontal-menu'} />
  ) : (
    <Fragment />
  );
  const DesktopMenu: ReactElement = showNavMenu ? (
    <MainNavigationMenu variant={'default'} className={'vertical-menu'} />
  ) : (
    <Fragment />
  );

  const handleResize = (): void => {
    if (isLandscape()) {
      setNavigationMenu(DesktopMenu);
    } else {
      setNavigationMenu(MobileMenu);
    }
  };

  useEffect(() => {
    handleResize();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [showNavMenu]);

  return (
      <Page
        style={{ backgroundColor: 'transparent' }}
        header={getHeaderFromName(getHeader)}
        sidebar={
          <Fragment>
            {Sidebar}
            {getNavigationMenu}
          </Fragment>
        }
        mainContainerId={'primary-app-container'}
        className={
          showCompletely
            ? `framed-page ${getBackgroundFromName(getBackground)}`
            : `unframed-page ${getBackgroundFromName(getBackground)}`
        }
        onPageResize={handleResize}
      >
        <AlertManager />
        <Outlet context={getContextProps} />
        {getFooterFromName(getFooter)}
      </Page>
  );
};