import { generateAuthorizationHeaders } from '../utils/logics/authUtils.ts';
import { Client } from '../api/utilities/Definitions';
import OpenAPIClientAxios from 'openapi-client-axios';

export const getAllProjects = (
  userToken: string,
) => async (openAPIClient: OpenAPIClientAxios) => {
  const client: Client = await openAPIClient.getClient<Client>();

  return await client.list_projects(
    null,
    null,
    generateAuthorizationHeaders(userToken),
  );
};

export const shareUpload = (
  userToken: string,
  filename: string,
  /* eslint-disable-next-line @typescript-eslint/no-explicit-any */
  content: any,
) => async (openAPIClient: OpenAPIClientAxios) => {
  const client: Client = await openAPIClient.getClient<Client>();

  content.append('filename', filename);

  return await client.shares_upload_create(
    { filename },
    content,
    generateAuthorizationHeaders(userToken),
  );
};

export const shareProject = (
  userToken: string,
  filename: string,
  /* eslint-disable-next-line @typescript-eslint/no-explicit-any */
  content: any,
) => async (openAPIClient: OpenAPIClientAxios) => {
  const client: Client = await openAPIClient.getClient<Client>();

  content.append('filename', filename);

  return await client.export_project(
    { filename },
    content,
    generateAuthorizationHeaders(userToken),
  );
};

export const importProject = (
  userToken: string,
  projectId: number
) => async (openAPIClient: OpenAPIClientAxios) => {
  const client: Client = await openAPIClient.getClient<Client>();

  return await client.import_project(
    { project_id: projectId },
    null,
    generateAuthorizationHeaders(userToken),
  );
};

export const getFilesContentTypes = (
  userToken: string,
) => async (openAPIClient: OpenAPIClientAxios) => {
  const client: Client = await openAPIClient.getClient<Client>();

  return await client.files_content_types_retrieve(
    null,
    null,
    generateAuthorizationHeaders(userToken),
  );
};