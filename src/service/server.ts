import OpenAPIClientAxios from 'openapi-client-axios';
import { Client } from '../api/utilities/Definitions';

export const getServerStatus = () => async (openAPIClient: OpenAPIClientAxios) => {
  const client: Client = await openAPIClient.getClient<Client>();

  return await client.status_retrieve()
    .then(() => true)
    .catch((error) => {
      console.error(error);
      return false;
    });
}