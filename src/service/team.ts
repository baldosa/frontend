import OpenAPIClientAxios from 'openapi-client-axios';
import { Client } from '../api/utilities/Definitions';
import { generateAuthorizationHeaders } from '../utils/logics/authUtils.ts';


export const getAllTeams = (
  userToken: string,
  skipPersonalWorkspace?: boolean,
  last_message?: boolean,
) => async (openAPIClient: OpenAPIClientAxios) => {
  const client: Client = await openAPIClient.getClient<Client>();
  return await client.teams_list(
    {
      skip_personal_workspace: skipPersonalWorkspace,
      last_message: last_message
    },
    null,
    generateAuthorizationHeaders(userToken),
  );
}

export const getTeamMessages = (
  userToken: string,
  teamToken: string,
  lastKnownMessage?: string,
  limit?: number,
  lookIntoFuture?: boolean,
) => async (openAPIClient: OpenAPIClientAxios) => {
  const client: Client = await openAPIClient.getClient<Client>();

  return await client.conversations_messages_retrieve(
    {
      conversation_token: teamToken,
      last_known_message: lastKnownMessage,
      look_into_future: lookIntoFuture,
      limit: limit
    },
    null,
    generateAuthorizationHeaders(userToken)
  );
};

export const getTeamDetails = (
  userToken: string,
  teamId: number,
) => async (openAPIClient: OpenAPIClientAxios) => {
  const client: Client = await openAPIClient.getClient<Client>();

  return await client.teams_retrieve(
    { id: teamId },
    null,
    generateAuthorizationHeaders(userToken),
  );
};

export const getTeamFiles = (
  userToken: string,
  teamToken: string,
  descending?: boolean,
  limit?: number,
) => async (openAPIClient: OpenAPIClientAxios) => {
  const client: Client = await openAPIClient.getClient<Client>();

  return await client.conversations_files_retrieve(
    { conversation_token: teamToken, descending: descending, limit: limit },
    null,
    generateAuthorizationHeaders(userToken),
  );
};

export const sendMessageToTeam= (
  userToken: string,
  teamToken: string,
  /* eslint-disable-next-line @typescript-eslint/no-explicit-any */
  content: any,
) => async (openAPIClient: OpenAPIClientAxios) => {
  const client: Client = await openAPIClient.getClient<Client>();

  return await client.conversations_messages_create(
    { conversation_token: teamToken },
    content,
    generateAuthorizationHeaders(userToken),
  );
};

export const uploadFileToTeam = (
  userToken: string,
  teamToken: string,
  /* eslint-disable-next-line @typescript-eslint/no-explicit-any */
  content: any,
) => async (openAPIClient: OpenAPIClientAxios) => {
  const client: Client = await openAPIClient.getClient<Client>();

  return await client.conversations_files_create(
    { conversation_token: teamToken },
    content,
    generateAuthorizationHeaders(userToken),
  );
};

export const downloadFile = (
  userToken: string,
  fileId: number,
) => async (openAPIClient: OpenAPIClientAxios) => {
  const client: Client = await openAPIClient.getClient<Client>();

  return await client.downloads_retrieve(
    { file_id: fileId },
    null,
    generateAuthorizationHeaders(userToken),
  );
};

export const deleteFile = (
  userToken: string,
  fileId: number,
) => async (openAPIClient: OpenAPIClientAxios) => {
  const client: Client = await openAPIClient.getClient<Client>();

  return await client.files_destroy(
    { file_id: fileId },
    null,
    generateAuthorizationHeaders(userToken),
  );
};
