import OpenAPIClientAxios from 'openapi-client-axios';
import { Client } from '../api/utilities/Definitions';
import { generateAuthorizationHeaders } from '../utils/logics/authUtils.ts';

export const getUserData = (
  userId: number,
  userToken: string,
) => async (openAPIClient: OpenAPIClientAxios) => {
  const client: Client = await openAPIClient.getClient<Client>();

  return await client.users_retrieve(
    { id: userId },
    null,
    generateAuthorizationHeaders(userToken),
  );
};

export const updateUserData = (
  userId: number,
  userToken: string,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  data: any,
) => async (openAPIClient: OpenAPIClientAxios) => {
  const client: Client = await openAPIClient.getClient<Client>();

  return await client.users_partial_update(
    { id: userId },
    data,
    generateAuthorizationHeaders(userToken),
  );
};

export const getUserList = (
  userToken: string,
) => async (openAPIClient: OpenAPIClientAxios) => {
  const client: Client = await openAPIClient.getClient<Client>();

  return await client.users_list(
    null,
    null,
    generateAuthorizationHeaders(userToken),
  );
};