import {
  generateAuthData,
  generateAuthorizationHeaders,
} from '../utils/logics/authUtils.ts';
import { Client, Paths } from '../api/utilities/Definitions';
import { AxiosResponse } from 'axios';
import { buildAuthToken } from '../api/Client.ts';
import OpenAPIClientAxios from 'openapi-client-axios';

export const authLogin = (
  username: string,
  password: string,
) => async (openAPIClient: OpenAPIClientAxios): Promise<AxiosResponse> => {

  const authenticationData = generateAuthData(username, password);
  const client: Client = await openAPIClient.getClient<Client>();

  return await client.auth_login_create(null, authenticationData)
      .then(async (loginResponse: AxiosResponse) => {
        const headers = generateAuthorizationHeaders(loginResponse.data.access);

        await client.organizations_byuser_retrieve(null, null, headers)
          .then((organizationResponse) => {
            loginResponse.data.user.organizationId = organizationResponse.data.id;
          })
          .catch((error) => {
            loginResponse.data.user.organizationId = null;
            console.error(error);
          });
        loginResponse.data.user.roles = [loginResponse.data.user.group.name];

        localStorage.setItem('user', JSON.stringify(loginResponse.data));

        return loginResponse;
      })
      .catch((error) => {
        console.error(error);
        throw error;
      })
};

export const authSignup = (
  username: string,
  password: string,
  parameters: Paths.ConfirmInvitation.PathParameters,
) => async (openAPIClient: OpenAPIClientAxios) => {
  const client: Client = await openAPIClient.getClient<Client>();

  return await client.confirm_invitation(parameters, {
    username: username,
    password: password
  });
};

export const authLogout = (
  accessToken: string
) => async (openAPIClient: OpenAPIClientAxios) => {
  const client: Client = await openAPIClient.getClient<Client>();

  return await client.auth_logout_create(null, null, {
    headers: { Authorization: buildAuthToken(accessToken) },
  });
}

export const authPasswordReset = (
  email: string,
) => async (openAPIClient: OpenAPIClientAxios) => {
  const client: Client = await openAPIClient.getClient<Client>();

  return await client.auth_password_reset_create(null, {
    email
  });
}

export const authPasswordResetConfirm = (
  userId: string,
  token: string,
  newPassword: string,
  newPasswordConfirm: string,
) => async (openAPIClient: OpenAPIClientAxios) => {
  const client: Client = await openAPIClient.getClient<Client>();

  return await client.auth_password_reset_confirm_create(null, {
    uid: userId,
    password: newPassword,
    password_confirm: newPasswordConfirm,
    token,
  });
};

export const authRefreshToken = (
  refreshToken: string,
) => async (openAPIClient: OpenAPIClientAxios) => {
  const client: Client = await openAPIClient.getClient<Client>();

  return await client.auth_token_refresh_create(null, {
    refresh: refreshToken
  });
}

export const authVerifyToken = (
  token: string,
) => async (openAPIClient: OpenAPIClientAxios) => {
  const client: Client = await openAPIClient.getClient<Client>();

  return await client.auth_token_verify_create(null, {
    token: token
  });
}