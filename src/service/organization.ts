import { generateAuthorizationHeaders } from '../utils/logics/authUtils.ts';
import { Client } from '../api/utilities/Definitions';
import OpenAPIClientAxios from 'openapi-client-axios';

export const sendOrganizationInvitations = (
  organization_id: number,
  userToken: string,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  invitations: any,
) => async (openAPIClient: OpenAPIClientAxios) => {
  const client: Client = await openAPIClient.getClient<Client>();

  return await client.invitations_organizationmember_create(
    null,
    {
      organization_id: organization_id,
      new_users: invitations,
    },
    generateAuthorizationHeaders(userToken),
  );
};

export const getOrganization = (
  organizationId: number,
  userToken: string,
) => async (openAPIClient: OpenAPIClientAxios) => {
  const client: Client = await openAPIClient.getClient<Client>();

  return await client.organizations_retrieve(
    { id: organizationId },
    null,
    generateAuthorizationHeaders(userToken),
  );
};

export const getMembersOrganizations = (
  organizationId: number,
  userToken: string,
) => async (openAPIClient: OpenAPIClientAxios) => {
  const client: Client = await openAPIClient.getClient<Client>();

  return await client.get_organization_members(
    { organization_id: organizationId },
    null,
    generateAuthorizationHeaders(userToken),
  );
};

export const updateOrganization = (
  organizationId: number,
  userToken: string,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  data: any,
) => async (openAPIClient: OpenAPIClientAxios) => {
  const client: Client = await openAPIClient.getClient<Client>();

  return await client.organizations_partial_update(
    { id: organizationId },
    data,
    generateAuthorizationHeaders(userToken),
  );
};

export const getAllInvitations = (
  organizationId: number,
  userToken: string,
) => async (openAPIClient: OpenAPIClientAxios) => {
  const client: Client = await openAPIClient.getClient<Client>();

  return await client.list_invitations(
    { organization_id: organizationId },
    null,
    generateAuthorizationHeaders(userToken),
  );
};
