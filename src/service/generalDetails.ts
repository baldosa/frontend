import { generateAuthorizationHeaders } from '../utils/logics/authUtils.ts';
import OpenAPIClientAxios from 'openapi-client-axios';
import { Client } from '../api/utilities/Definitions';

export const getLanguages = (
  userToken: string,
) => async (openAPIClient: OpenAPIClientAxios) => {
  const client: Client = await openAPIClient.getClient<Client>();

  return await client.languages_list(
    null,
    null,
    generateAuthorizationHeaders(userToken),
  );
};

export const getCountries = (
  userToken: string,
) => async (openAPIClient: OpenAPIClientAxios) => {
  const client: Client = await openAPIClient.getClient<Client>();

  return await client.countries_list(
    null,
    null,
    generateAuthorizationHeaders(userToken),
  );
};

export const getGroups = (
  userToken: string,
) => async (openAPIClient: OpenAPIClientAxios) => {
  const client: Client = await openAPIClient.getClient<Client>();

  return await client.groups_list(
    null,
    null,
    generateAuthorizationHeaders(userToken),
  );
};