import {
  AudioExportTypesEnum,
  StatesInteractionWaveformEnum,
  HandleInteractionWaveformEnum,
} from '../enums';

export type AudioStateProps =
  | 'NONE'
  | 'RECORDING'
  | 'STOPPED'
  | 'PAUSED'
  | 'PLAYING';

export type NXTagsProps = {
  id?: number;
  title: string;
};

export type AudioExportTypesProps =
  | AudioExportTypesEnum.MP3
  | AudioExportTypesEnum.WAV
  | AudioExportTypesEnum.WEBA
  | AudioExportTypesEnum.OPUS
  | AudioExportTypesEnum.AAC
  | AudioExportTypesEnum.ZIP;

export type StatesInteractionWaveformProps =
  | StatesInteractionWaveformEnum.CURSOR
  | StatesInteractionWaveformEnum.FADEIN
  | StatesInteractionWaveformEnum.FADEOUT
  | StatesInteractionWaveformEnum.SELECT
  | StatesInteractionWaveformEnum.SHIFT;

export type HandleInteractionWaveformProps =
  | HandleInteractionWaveformEnum.CUT
  | HandleInteractionWaveformEnum.SPLIT
  | HandleInteractionWaveformEnum.TRIM;
