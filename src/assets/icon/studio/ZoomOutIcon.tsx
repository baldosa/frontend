import * as React from 'react';

export const ZoomOutIcon = ({
  fill = 'white',
  height = '20',
  style = {},
  width = '20',
}: {
  fill?: string;
  height?: string;
  outline?: boolean;
  style?: object;
  width?: string;
}): React.ReactElement => {
  return (
    <svg
      width={width}
      style={style}
      height={height}
      viewBox="0 0 54 54"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M30.3135 30.4307C31.0208 29.6187 32.2533 29.5345 33.0665 30.2428L53.2211 47.7971C54.0342 48.5053 54.1201 49.7378 53.4129 50.5499C52.7056 51.3619 51.4731 51.4461 50.6599 50.7378L30.5053 33.1836C29.6921 32.4753 29.6063 31.2428 30.3135 30.4307Z"
        fill={fill}
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M0.0309331 21.5785C-0.564971 11.133 7.52112 2.2294 18.0381 1.64412C28.5551 1.05884 37.6027 9.00898 38.1986 19.4545C38.7945 29.9 30.7084 38.8036 20.1914 39.3889C9.67441 39.9742 0.626837 32.0241 0.0309331 21.5785ZM12.9882 18.0688C12.1261 18.0688 11.4272 18.7676 11.4272 19.6297V21.4598C11.4272 22.3219 12.1261 23.0208 12.9882 23.0208H25.1007C25.9628 23.0208 26.6617 22.3219 26.6617 21.4598V19.6297C26.6617 18.7676 25.9628 18.0688 25.1007 18.0688H12.9882Z"
        fill={fill}
      />
    </svg>
  );
};
