import * as React from 'react';

export const SearchIcon = ({
  className,
}: {
  className: string;
}): React.ReactElement => {
  return (
    <svg
      className={className}
      fill="currentColor"
      width="24"
      height="24"
      viewBox="0 0 24 24"
      aria-hidden="true"
      role="img"
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M16.5604 8.33905C16.5604 12.9121 12.8532 16.6193 8.28021 16.6193C3.70718 16.6193 0 12.9121 0 8.33905C0 3.76602 3.70718 0.0588379 8.28021 0.0588379C12.8532 0.0588379 16.5604 3.76602 16.5604 8.33905Z"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M13.5799 13.645C13.8506 13.3708 14.2924 13.368 14.5667 13.6387L23.7925 22.747C24.0667 23.0177 24.0695 23.4595 23.7988 23.7337C23.5281 24.008 23.0863 24.0108 22.812 23.7401L13.5862 14.6318C13.312 14.3611 13.3091 13.9193 13.5799 13.645Z"
      />
    </svg>
  );
};
