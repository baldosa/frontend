import {
  Dispatch,
  FunctionComponent,
  ReactElement,
  useEffect,
  useState,
} from 'react';
import {
  AlertGroup,
  Alert,
  AlertActionCloseButton,
} from '@patternfly/react-core';
import { useDispatch, useSelector } from 'react-redux';
import { AlertContent, getAllAlerts } from '../redux/reducers/alert.ts';
import { removeAlert } from '../redux/actions/alert.ts';
import { AnyAction } from '@reduxjs/toolkit';

export const AlertManager: FunctionComponent = () => {
  const [getAlerts, setAlerts] = useState<ReactElement[]>([]);
  const getAlertsFromStore: Record<number, AlertContent> =
    useSelector(getAllAlerts);
  const dispatch: Dispatch<AnyAction> = useDispatch();

  useEffect(() => {
    setAlerts(
      Object.entries(getAlertsFromStore).map((alert) =>
        renderNewAlert(parseInt(alert[0]), alert[1]),
      ),
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [getAlertsFromStore]);

  const renderNewAlert = (alertId: number, alertContent: AlertContent) => {
    return (
      <Alert
        style={{ width: 'fit-content' }}
        key={alertId}
        id={`alter_${alertId}`}
        title={alertContent.message}
        variant={alertContent.variant}
        timeout={5000}
        onTimeout={() => removeAlert(alertId)(dispatch)}
        actionClose={
          <AlertActionCloseButton
            label={`close_button_alert_${alertId}`}
            onClose={() => removeAlert(alertId)(dispatch)}
          />
        }
      />
    );
  };

  return (
    <AlertGroup
      isToast
      isLiveRegion
      style={{ width: 'auto' }}
      children={getAlerts}
    />
  );
};
