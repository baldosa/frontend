import { MessageParameters } from '../pages/Team/Chat/Message/Message.tsx';

export const calculateFullDateTime = (
  timestamp: number,
  language: string,
): string => {
  return (
    new Date(timestamp * 1000).toLocaleDateString(language.replace('_', '-')) +
    ' ' +
    new Date(timestamp * 1000).toLocaleTimeString(language.replace('_', '-'))
  );
};

export const calculateSimplifiedDateTime = (
  timestamp: number,
  language: string,
): string => {
  const options: Intl.DateTimeFormatOptions = {
    hour: 'numeric',
    minute: 'numeric',
    hour12: false,
  };

  return new Date(timestamp * 1000).toLocaleTimeString(
    language.replace('_', '-'),
    options,
  );
};

export const parseMessage = (
  message: string,
  messageParams: MessageParameters | undefined,
) => {
  for (const property in messageParams) {
    if (messageParams[property]) {
      message = message.replace(
        new RegExp(`{${property}}`, 'g'),
        messageParams[property].name,
      );
    }
  }

  return message;
};
