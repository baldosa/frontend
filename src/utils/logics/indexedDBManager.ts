import {
  ServerCoreDataType
} from '../../components/Auth/servers/ServerDataType.ts';

export class IndexedDBManager {
  private static databases: { [name: string]: { version: number } } = {};

  public static createObjectStore(
    dataBaseName: string,
    objectStoreName: string,
    autoIncrement = false,
  ): Promise<IDBDatabase> {
    return new Promise((resolve, reject) => {
      const currentVersion =
        IndexedDBManager.getCurrentVersion(dataBaseName) + 1;
      IndexedDBManager.createDataBase(dataBaseName, currentVersion)
        .then((database: IDBDatabase) => {
          database.createObjectStore(objectStoreName, { autoIncrement: autoIncrement });
          resolve(database);
        })
        .catch((error) => reject(error));
    });
  }

  public static addElementToStore(
    dataBaseName: string,
    objectStoreName: string,
    key: number | undefined,
    /* eslint-disable-next-line @typescript-eslint/no-explicit-any */
    value: any,
    /* eslint-disable-next-line @typescript-eslint/no-explicit-any */
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      IndexedDBManager.getDataBase(dataBaseName)
        .then((database: IDBDatabase) => {
          const request = database
            .transaction(objectStoreName, 'readwrite')
            .objectStore(objectStoreName)
            .put(value, key);

          request.onsuccess = () => {
            resolve(request.result);
          }

        })
        .catch((error) => reject(error));
    });
  }
  public static deleteElementFromStore(
    dataBaseName: string,
    objectStoreName: string,
    key: number,
    /* eslint-disable-next-line @typescript-eslint/no-explicit-any */
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      IndexedDBManager.getDataBase(dataBaseName)
        .then((database: IDBDatabase) => {
          const request = database
            .transaction(objectStoreName, 'readwrite')
            .objectStore(objectStoreName)
            .delete(key);

          request.onsuccess = () => {
            resolve(request.result);
          }

        })
        .catch((error) => reject(error));
    });
  }

  public static getElementFromStore(
    dataBaseName: string,
    objectStoreName: string,
    key: number | string,
    /* eslint-disable-next-line @typescript-eslint/no-explicit-any */
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      IndexedDBManager.getDataBase(dataBaseName)
        .then((database: IDBDatabase) => {
          const request = database
            .transaction(objectStoreName, 'readonly')
            .objectStore(objectStoreName)
            .get(key);

          request.onsuccess = () => {
            if (request.result !== undefined) {
              resolve(request.result);
            } else {
              reject(`Error: Element with key ${key} not found`);
            }
          };

          request.onerror = () => {
            reject(`Error: Could not get ${key} element`);
          };
        })
        .catch((error) => error);
    });
  }

  public static getAllElementsFromStore(
    dataBaseName: string,
    objectStoreName: string,
  ): Promise<Map<number, ServerCoreDataType>> {
    return new Promise((resolve, reject) => {
      IndexedDBManager.getDataBase(dataBaseName)
        .then((database: IDBDatabase) => {
          const object_store = database
            .transaction(objectStoreName, 'readonly')
            .objectStore(objectStoreName)

          const cursorRequest = object_store.openCursor();
          const result = new Map<number, ServerCoreDataType>();

          cursorRequest.onsuccess = (event) => {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            const cursor = event.target.result;
            if (cursor) {
              result.set(cursor.key, cursor.value)
              cursor.continue();
            } else {
              resolve(result);
            }
          };

          cursorRequest.onerror = () => {
            reject(`Error: Could not get elements`);
          };
        })
        .catch((error) => error);
    });
  }

  private static getDataBase(name: string): Promise<IDBDatabase> {
    return new Promise((resolve, reject) => {
      const request = window.indexedDB.open(name);

      request.onsuccess = (event: Event) => {
        resolve((event.target as IDBOpenDBRequest).result);
      };

      request.onerror = () => {
        reject(`Error: Could not connect to database ${name}`);
      };
    });
  }

  private static createDataBase(
    name: string,
    version: number,
  ): Promise<IDBDatabase> {
    return new Promise((resolve, reject) => {
      const request = window.indexedDB.open(name, version);

      request.onupgradeneeded = (event: Event) => {
        const database: IDBDatabase = (event.target as IDBOpenDBRequest).result;
        IndexedDBManager.databases[name] = { version: database.version };
        resolve(database);
      };

      request.onerror = () => {
        reject(`Error: The database ${name} could not be created`);
      };
    });
  }

  private static getCurrentVersion(databaseName: string): number {
    const databaseInfo = IndexedDBManager.databases[databaseName];
    return databaseInfo ? databaseInfo.version : 0;
  }
}

export const mediaDataBase = 'Media';
export const audioStore = 'Audio';
export const visualStore = 'Visual';
export const documentStore = 'Document';
export const serverDataBase = 'Server';
export const serverStore = 'Server';

