import { NavigateFunction } from 'react-router-dom';
import { saveUserData } from '../../redux/actions/auth.ts';
import { signupTACFullPath } from '../../routes.tsx';
import { Dispatch } from 'react';
import { AnyAction } from '@reduxjs/toolkit';
import {
  IndexedDBManager,
  serverDataBase,
  serverStore,
} from './indexedDBManager.ts';
import { addAlert } from '../../redux/actions/alert.ts';
import { TFunction } from 'i18next';
import {
  ServerCoreDataType,
} from '../../components/Auth/servers/ServerDataType.ts';
import { buildAuthToken } from '../../api/Client.ts';

export const comesFromAccountCreationToTac = (
  username: string | null,
  password: string | null,
  dispatch: Dispatch<AnyAction>,
  navigate: NavigateFunction,
) => {
  saveUserData(username, password)(dispatch);
  navigate(signupTACFullPath());
};

export const saveServerData = (
  id: number | undefined,
  serverName: string,
  serverUrl: string,
  t: TFunction,
  dispatch: Dispatch<AnyAction>
) => {
  return IndexedDBManager.addElementToStore(serverDataBase, serverStore, id, { name: serverName, url: serverUrl })
    .then((serverId) => {
      addAlert('success', t('auth.server_list.alert.success.new_server'))(dispatch);
      return serverId
    })
    .catch(() => {
      addAlert('danger', t('auth.server_list.alert.danger.general'))(dispatch);
    });
};

export const getAllServers = async (): Promise<Map<number, ServerCoreDataType>> => {
  return await IndexedDBManager.getAllElementsFromStore(serverDataBase, serverStore)
    .then((value) => value)
    .catch((error) => error);
};

export const getServerData = async (id: number) => {
  return await IndexedDBManager.getElementFromStore(serverDataBase, serverStore, id)
    .then((value) => value)
    .catch((error) => error);
};

export const getTokenAndUID = (fullToken: string) => {
  const uid = fullToken.substring(0, fullToken.indexOf('-'));
  const token = fullToken.replace(uid + '-', '');
  return { token, uid };
};

export const generateAuthorizationHeaders = (accessToken: string) => {
  return { headers: { Authorization: buildAuthToken(accessToken) } }
};

export const generateAuthData = (username: string, password: string) => {
  return username.includes('@')
    ? { email: username, password: password }
    : { username: username, password: password };
}

export const deleteServerData = async (key:number) => {
  return await IndexedDBManager.deleteElementFromStore(serverDataBase, serverStore, key)
  .then((value) =>  value)
  .catch((error) => error)
}