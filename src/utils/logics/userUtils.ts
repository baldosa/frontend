import {
  changeUserProfileAvatar,
  changeUserProfileEmail,
  changeUserProfileUsername,
  userProfileLoadedCompletely,
} from '../../redux/actions/configuration/initial/userProfileDetails.ts';
import { Dispatch } from 'react';
import { AnyAction } from '@reduxjs/toolkit';
import { updateAccessToken } from '../../redux/actions/auth.ts';
import { authRefreshToken, authVerifyToken } from '../../service/authentication.ts';
import { Role } from './types.ts';
import OpenAPIClientAxios from 'openapi-client-axios';
import { getUserData } from '../../service/user.ts';

export const saveUserData = (
  userId: number,
  userToken: string,
) =>  (dispatch: Dispatch<AnyAction>, openAPIClient: OpenAPIClientAxios) => {
  getUserData(userId, userToken)(openAPIClient)
    .then((response) => {
      const avatar: string | undefined = response.data.avatar;
      changeUserProfileUsername(response.data.full_name)(dispatch);
      changeUserProfileEmail(response.data.email)(dispatch);
      changeUserProfileAvatar(
        avatar !== undefined && avatar.length > 0 ? avatar : null,
      )(dispatch);
      userProfileLoadedCompletely()(dispatch);
    })
    .catch((error) => {
      console.error(error);
    });
};

export const hasAnyRole = (userRoles: string[], requiredRoles: string[]) => {
  return userRoles.some((role: string): boolean => {
    return requiredRoles.length == 0 || requiredRoles.includes(role);
  });
};

export const isAdministrator = (userRoles: string[]) => {
  return (
    userRoles &&
    userRoles.some((role: string): boolean => {
      return role == Role.Admin || role == Role.OrgOwner;
    })
  );
};

export const verifyAndRefreshToken = (
  refreshToken: string,
  accessToken: string,
) => async (dispatch: Dispatch<AnyAction>, client: OpenAPIClientAxios) => {
  return await authVerifyToken(refreshToken)(client)
    .then(async () => {
      return await authVerifyToken(accessToken)(client)
        .then(async () => {
          return await authRefreshToken(refreshToken)(client)
            .then(async (response) => {
              updateAccessToken(response.data.access)(dispatch);
            })
            .catch((error) => {
              console.error(error);
            });
        })
        .catch((error) => {
          console.error(error);
        });
    })
    .catch((error) => {
      console.error(error);
    });
};
