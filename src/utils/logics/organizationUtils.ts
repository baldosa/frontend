import {
  changeOrganizationDetailsEmail,
  changeOrganizationDetailsName,
  changeOrganizationLogo,
} from '../../redux/actions/configuration/initial/organizationDetails.ts';
import { Dispatch } from 'react';
import { AnyAction } from '@reduxjs/toolkit';
import OpenAPIClientAxios from 'openapi-client-axios';
import { getOrganization } from '../../service/organization.ts';

export const saveOrganizationData = (
  organizationId: number,
  userToken: string,
) =>  (dispatch: Dispatch<AnyAction>, openAPIClient: OpenAPIClientAxios) => {

  getOrganization(organizationId, userToken)(openAPIClient)
    .then((response) => {
      const logo: string | undefined = response.data.logo;
      changeOrganizationDetailsName(response.data.name)(dispatch);
      changeOrganizationDetailsEmail(response.data.email)(dispatch);
      changeOrganizationLogo(
        logo !== undefined && logo.length > 0 ? logo : null,
      )(dispatch);
    })
    .catch((error) => {
      console.error(error);
    });
};

export const convertToKeyValueList = (
  listOfElements: (
    | { name: string; iso_code?: string }
    | { name: string; id: number }
    )[],
): { key: string | number; value: string }[] => {
  return listOfElements.map((item) => {
    if ('id' in item) {
      return { key: item.id, value: item.name };
    } else {
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      return { key: item.iso_code!, value: item.name };
    }
  });
};