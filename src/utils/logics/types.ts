export enum Role {
  Collaborator = 'Collaborator',
  Admin = 'Admin',
  OrgOwner = 'OrgOwner',
}