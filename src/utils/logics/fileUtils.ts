import { createAudioPreview, createImagePreview } from '../FileUtils.tsx';
import { ReactElement } from 'react';
import {
  audioStore,
  documentStore,
  IndexedDBManager,
  mediaDataBase,
  visualStore,
} from './indexedDBManager.ts';
import { deleteFile, downloadFile } from '../../service/team.ts';
import OpenAPIClientAxios from 'openapi-client-axios';
import { getFilesContentTypes } from '../../service/recorder.ts';

export const getContentTypes = (
  userToken: string,
) => async (openAPIClient: OpenAPIClientAxios): Promise<string> => {
  return await getFilesContentTypes(userToken)(openAPIClient)
    .then((response) => {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      return response.data.map((value) => value['content_type']).join(', ');
    })
    .catch((error) => {
      console.error(error);
    });
};

export const onDelete = (
  fileId: number,
  userToken: string,
) => async (openAPIClient: OpenAPIClientAxios): Promise<void> => {
  await deleteFile(userToken, fileId)(openAPIClient);
};

export const getBlob = (
  fileId: number,
  userToken: string,
) => async (openAPIClient: OpenAPIClientAxios): Promise<Blob> => {
  return await downloadFile(userToken, fileId)(openAPIClient)
    .then((response): Blob => {
      const bytes = Uint8Array.from(
        atob(response.data as unknown as string),
        (c) => c.charCodeAt(0),
      );
      return new Blob([bytes], { type: 'application/octet-stream' });
    })
    .catch((error) => error);
};

export const saveBlobInStore = (
  database: string,
  store: string,
  key: number,
  userToken: string,
) => async (openAPIClient: OpenAPIClientAxios): Promise<Blob> => {
  return await getBlob(key, userToken)(openAPIClient)
    .then((response) => {
      IndexedDBManager.addElementToStore(database, store, key, response);
      return response;
    })
    .catch((error) => {
      return error;
    });
};

export const getTagByExtension = (extension: string): string | undefined => {
  const tagByExtensions = new Map([
    ['img', ['bmp', 'gif', 'jpg', 'jpeg', 'png', 'svg', 'tiff']],
    ['video', ['avi', 'flv', 'mov', 'mp4', 'mpeg']],
    ['doc', ['csv', 'doc', 'docx', 'dwg', 'mdb', 'mid', 'pdf', 'ppt', 'xsl']],
    ['audio', ['mp3', 'wav', 'wma', 'weba']],
    ['compressed', ['rar', 'zip']],
  ]);

  return [...tagByExtensions].find(([, extensions]) =>
    extensions.includes(extension),
  )?.[0];
};

export const getStoreByTag = (extension: string): string | undefined => {
  const tagByExtensions = new Map([
    [
      visualStore,
      [
        'avi',
        'flv',
        'mov',
        'mp4',
        'mpeg',
        'bmp',
        'gif',
        'jpg',
        'jpeg',
        'png',
        'svg',
        'tiff',
      ],
    ],
    [
      documentStore,
      [
        'csv',
        'doc',
        'docx',
        'dwg',
        'mdb',
        'mid',
        'pdf',
        'ppt',
        'xsl',
        'rar',
        'zip',
      ],
    ],
    [audioStore, ['mp3', 'wav', 'wma', 'weba']],
  ]);

  return [...tagByExtensions].find(([, extensions]) =>
    extensions.includes(extension),
  )?.[0];
};

export const createPreview = (
  fileId: number,
  userToken: string,
  extension: string,
  alternativeText: string,
) => async (openAPIClient: OpenAPIClientAxios): Promise<ReactElement> => {
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  const storeByTag: string = getStoreByTag(extension.replace('.', ''))!;

  return IndexedDBManager.getElementFromStore(mediaDataBase, storeByTag, fileId)
    .then((blob: Blob) =>
      createPreviewByExtension(
        extension,
        convertBlobToBlobURL(blob),
        alternativeText,
      ),
    )
    .catch(async () => {
      return await saveBlobInStore(mediaDataBase, storeByTag, fileId, userToken)
      (openAPIClient)
        .then((blob: Blob) =>
          createPreviewByExtension(
            extension,
            convertBlobToBlobURL(blob),
            alternativeText,
          ),
        )
        .catch((error) => error);
    });
};

const createPreviewByExtension = (
  extension: string,
  url: string,
  alternativeText: string,
) => {
  switch (getTagByExtension(extension.replace('.', ''))) {
    case 'img':
      return createImagePreview(url, alternativeText);
    case 'audio':
      return createAudioPreview(url, alternativeText);
    //TODO: In the future when we support more file types we should add them here.
    default:
      break;
  }
};

export const getNameData = (fileName: string): string[] => {
  const lastPoint: number = fileName.lastIndexOf('.');
  const name: string = fileName.slice(0, lastPoint);
  const extension: string = fileName.slice(lastPoint);

  return [name, extension];
};

export const getSizeHumanized = (
  size: number,
): { size: number; unit: string } => {
  const units: string[] = ['b', 'kb', 'mb', 'gb'];
  let unit: string = units[0];

  for (let i = 1; i < units.length; i++) {
    if (Math.floor(size / 1024) !== 0) {
      size = size / 1024;
      unit = units[i];
    } else {
      break;
    }
  }

  return { size: Math.floor(size), unit: unit };
};

export const getFilename = (fileName: string): string => {
  return getNameData(fileName)[0];
};

export const getFileExtension = (fileName: string): string => {
  return getNameData(fileName)[1].slice(1).toLowerCase();
};

export const renderFileSize = (size: number, unit: string): string => {
  return size + ' ' + unit;
};

export const renderFullName = (name: string, extension: string): string => {
  return name + extension;
};

export const isFilePreviewAvailable = (extension: string): boolean => {
  //TODO: In the future when we support more file types we should add here the functions to check videos, documents, audios.
  return [
    'bmp',
    'png',
    'svg',
    'gif',
    'jpeg',
    'jpg',
    'tiff',
    'mp3',
    'wav',
    'wma',
    'weba',
  ].includes(extension.replace('.', ''));
};

export const convertBlobURLToBase64 = async (
  blobUrl: string,
): Promise<string> => {
  const response = await fetch(blobUrl);
  const blob = await response.blob();

  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onload = () => {
      resolve((reader.result as string).split(',')[1]);
    };
    reader.onerror = (error: ProgressEvent<FileReader>) => {
      reject(error);
    };
    reader.readAsDataURL(blob);
  });
};

export const convertBase64ToBlobURL = (base64: string): string => {
  const byteCharacters = atob(base64);
  const byteArray = new Uint8Array(byteCharacters.length);

  for (let i = 0; i < byteCharacters.length; i++) {
    byteArray[i] = byteCharacters.charCodeAt(i);
  }

  return URL.createObjectURL(new Blob([byteArray], { type: 'image/jpg' }));
};

export const convertBlobToBlobURL = (blob: Blob): string => {
  return URL.createObjectURL(blob);
};

export const blobFromBase64 = (base64: string): Blob => {
  const bytes = Uint8Array.from(
    atob(base64),
    (c) => c.charCodeAt(0),
  );
  return new Blob([bytes], { type: 'application/octet-stream' });
}
