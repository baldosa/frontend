export const isNotNullNotUndefinedOrEqual = (
  firstValue: string | null | undefined,
  secondValue: string | null | undefined,
) => {
  return firstValue !== secondValue && secondValue !== undefined && secondValue !== null;
};
