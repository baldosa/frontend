import ResourcesFullIcon from '@patternfly/react-icons/dist/esm/icons/resources-full-icon';
import { Fragment, ReactElement } from 'react';

export const isLandscape = (): boolean => {
  return window.innerWidth >= 1024;
};

export const isPortrait = (): boolean => {
  return window.innerWidth <= 500;
};

export const getProgressStepIconByState = (
  state:
    | 'default'
    | 'danger'
    | 'warning'
    | 'pending'
    | 'success'
    | 'info'
    | undefined,
): ReactElement => {
  switch (state) {
    case 'success':
      return <ResourcesFullIcon color={'var(--violet-1-color)'} />;
    case 'info':
      return <ResourcesFullIcon color={'var(--yellow-1-color)'} />;
    default:
      return <Fragment />;
  }
};
