import * as React from 'react';
import { PNGFile } from '../assets/icon/chat/files/PNGFile.tsx';
import { AVIFile } from '../assets/icon/chat/files/AVIFile.tsx';
import { SVGFile } from '../assets/icon/chat/files/SVGFile.tsx';
import { CSVFile } from '../assets/icon/chat/files/CSVFile.tsx';
import { DOCFile } from '../assets/icon/chat/files/DOCFile.tsx';
import { DOCXFile } from '../assets/icon/chat/files/DOCXFile.tsx';
import { DWGFile } from '../assets/icon/chat/files/DWGFile.tsx';
import { EPSFile } from '../assets/icon/chat/files/EPSFile.tsx';
import { FLVFile } from '../assets/icon/chat/files/FLVFile.tsx';
import { GIFFile } from '../assets/icon/chat/files/GIFFile.tsx';
import { JPGFile } from '../assets/icon/chat/files/JPGFile.tsx';
import { MDBFile } from '../assets/icon/chat/files/MDBFile.tsx';
import { MIDFile } from '../assets/icon/chat/files/MIDFile.tsx';
import { MOVFile } from '../assets/icon/chat/files/MOVFile.tsx';
import { MP3File } from '../assets/icon/chat/files/MP3File.tsx';
import { MP4File } from '../assets/icon/chat/files/MP4File.tsx';
import { MPEGFile } from '../assets/icon/chat/files/MPEGFile.tsx';
import { PDFFile } from '../assets/icon/chat/files/PDFFile.tsx';
import { PPTFile } from '../assets/icon/chat/files/PPTFile.tsx';
import { RARFile } from '../assets/icon/chat/files/RARFile.tsx';
import { ZIPFile } from '../assets/icon/chat/files/ZIPFile.tsx';
import { TIFFFile } from '../assets/icon/chat/files/TIFFFile.tsx';
import { TXTFile } from '../assets/icon/chat/files/TXTFile.tsx';
import { WAVFile } from '../assets/icon/chat/files/WAVFile.tsx';
import { WMAFile } from '../assets/icon/chat/files/WMAFile.tsx';
import { XSLFile } from '../assets/icon/chat/files/XSLFile.tsx';
import { BMPFile } from '../assets/icon/chat/files/BMPFile.tsx';
import { WEBAFile } from '../assets/icon/chat/files/WEBAFile.tsx';
import { JPEGFile } from '../assets/icon/chat/files/JPEGFile.tsx';
import { GenericFile } from '../assets/icon/chat/files/GenericFile.tsx';
import { ODSFile } from '../assets/icon/chat/files/ODSFile.tsx';
import { ODTFile } from '../assets/icon/chat/files/ODTFile.tsx';
import { ODPFile } from '../assets/icon/chat/files/ODPFile.tsx';
import {
  convertBlobToBlobURL,
  getBlob,
  getFileExtension,
} from './logics/fileUtils.ts';
import OpenAPIClientAxios from 'openapi-client-axios';

const fileIcon: Record<string, React.ReactElement> = {
  bmp: <BMPFile />,
  png: <PNGFile />,
  avi: <AVIFile />,
  svg: <SVGFile />,
  csv: <CSVFile />,
  doc: <DOCFile />,
  docx: <DOCXFile />,
  dwg: <DWGFile />,
  eps: <EPSFile />,
  flv: <FLVFile />,
  gif: <GIFFile />,
  jpeg: <JPEGFile />,
  jpg: <JPGFile />,
  mdb: <MDBFile />,
  mid: <MIDFile />,
  mov: <MOVFile />,
  mp3: <MP3File />,
  mp4: <MP4File />,
  mpeg: <MPEGFile />,
  pdf: <PDFFile />,
  ppt: <PPTFile />,
  rar: <RARFile />,
  zip: <ZIPFile />,
  tiff: <TIFFFile />,
  txt: <TXTFile />,
  wav: <WAVFile />,
  wma: <WMAFile />,
  xsl: <XSLFile />,
  weba: <WEBAFile />,
  ods: <ODSFile />,
  odt: <ODTFile />,
  odp: <ODPFile />,
};

export const defineIcon = (fileName: string): React.ReactElement => {
  const extension: string = getFileExtension(fileName);
  return extension in fileIcon ? fileIcon[extension] : <GenericFile />;
};

export const onDownload = (
  fileId: number,
  userToken: string,
  fileFullname: string,
) => async (client: OpenAPIClientAxios) => {
  return getBlob(fileId, userToken)(client)
    .then((blob: Blob): void => {
      const a = document.createElement('a');
      const blobURL = convertBlobToBlobURL(blob);
      a.setAttribute('href', blobURL);
      a.setAttribute('download', fileFullname);
      a.click();
      window.URL.revokeObjectURL(blobURL);
    });
};

export const createImagePreview = (
  blobUrl: string,
  alternativeText: string,
) => {
  return (
    <img
      src={blobUrl}
      alt={alternativeText}
      style={{ maxHeight: window.innerHeight - 100 }}
    />
  );
};

export const createAudioPreview = (
  blobUrl: string,
  alternativeText: string,
) => {
  return (
    <audio
      controls
      controlsList="nodownload"
      src={blobUrl}
      style={{ height: '50px' }}
      aria-label={alternativeText}
    />
  );
};
