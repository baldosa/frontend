/* eslint-disable @typescript-eslint/no-explicit-any */
import { arrayBufferToBlob } from 'blob-util';
import { format } from 'date-fns';
import { enUS, es, ptBR } from 'date-fns/locale';
import JSZip from 'jszip';
import getBlobDuration from 'get-blob-duration';

declare global {
  interface Window {
    webkitAudioContext: any;
  }
}

export function clockFormatWaveformPlaylist(seconds: number, decimals = 3) {
  const hours: number = parseInt(String(seconds / 3600), 10) % 24;
  const minutes: number = parseInt(String(seconds / 60), 10) % 60;
  const secs = Number((seconds % 60).toFixed(decimals));

  return `${hours < 10 ? `0${hours}` : hours}:${
    minutes < 10 ? `0${minutes}` : minutes
  }:${secs < 10 ? `0${secs}` : secs}`;
}

export async function getMicrophonePermission(): Promise<string> {
  const permission = await localStorage.getItem('micPermission');

  if (!permission) return 'yes';

  return permission;
}

export async function setMicrophonePermission(permission: 'yes' | 'no') {
  await localStorage.setItem('micPermission', permission);
}

export function createBlobFromAudioBuffer(
  abuffer: AudioBuffer,
  totalSamples: number,
): Blob {
  // Generate audio file and assign URL
  return bufferToWave(abuffer, totalSamples);
}

// Convert AudioBuffer to a Blob using WAVE representation
function bufferToWave(abuffer: AudioBuffer, len: number) {
  const numOfChan = abuffer.numberOfChannels;
  const length = len * numOfChan * 2 + 44;
  const buffer = new ArrayBuffer(length);
  const view = new DataView(buffer);
  const channels: Float32Array[] = [];
  let i;
  let sample;
  let offset = 0;
  let pos = 0;

  // write WAVE header
  setUint32(0x46464952); // "RIFF"
  setUint32(length - 8); // file length - 8
  setUint32(0x45564157); // "WAVE"

  setUint32(0x20746d66); // "fmt " chunk
  setUint32(16); // length = 16
  setUint16(1); // PCM (uncompressed)
  setUint16(numOfChan);
  setUint32(abuffer.sampleRate);
  setUint32(abuffer.sampleRate * 2 * numOfChan); // avg. bytes/sec
  setUint16(numOfChan * 2); // block-align
  setUint16(16); // 16-bit (hardcoded in this demo)

  setUint32(0x61746164); // "data" - chunk
  setUint32(length - pos - 4); // chunk length

  // write interleaved data
  for (i = 0; i < abuffer.numberOfChannels; i++)
    channels.push(abuffer.getChannelData(i));

  while (pos < length) {
    for (i = 0; i < numOfChan; i++) {
      // interleave channels
      sample = Math.max(-1, Math.min(1, channels[i][offset])); // clamp
      sample = (0.5 + sample < 0 ? sample * 32768 : sample * 32767) | 0; // scale to 16-bit signed int
      view.setInt16(pos, sample, true); // write 16-bit sample
      pos += 2;
    }
    offset++; // next source sample
  }

  // create Blob
  return new Blob([buffer], { type: 'audio/wav' });

  function setUint16(data: any) {
    view.setUint16(pos, data, true);
    pos += 2;
  }

  function setUint32(data: any) {
    view.setUint32(pos, data, true);
    pos += 4;
  }
}

export function prepareBlobUrl(blob: Blob | ArrayBuffer | File) {
  let blobUrl: string;
  if (blob instanceof ArrayBuffer) {
    const blobFile = arrayBufferToBlob(blob);
    blobUrl = URL.createObjectURL(blobFile);
  } else blobUrl = URL.createObjectURL(blob);

  return blobUrl;
}

export function downloadFile(
  data: Blob | File | ArrayBuffer | null | undefined,
  name = 'file',
) {
  if (!data) return false;

  const {
    URL: { revokeObjectURL },
    setTimeout,
  } = window;

  const url = prepareBlobUrl(data);

  const anchor = document.createElement('a');
  anchor.setAttribute('href', url);
  anchor.setAttribute('download', name);
  anchor.click();

  setTimeout(() => {
    revokeObjectURL(url);
  }, 100);

  return true;
}

export function getDefaultAudioName() {
  const tempFileName = new Date().toISOString();
  return tempFileName.substr(0, tempFileName.length - 5);
}

export async function getFilenameRecorder(): Promise<string> {
  const res = await localStorage.getItem('filenameRecorder');

  if (!res) return '';

  return res;
}

export async function getIDBEnable(): Promise<string> {
  const res = await localStorage.getItem('idbEnable');

  if (!res) return 'no';

  return res;
}

export async function getPathRecorder(): Promise<string> {
  const res = await localStorage.getItem('pathRecorder');

  if (!res) return '';

  return res;
}

export async function setFilenameRecorder(filename: string) {
  document.dispatchEvent(
    new CustomEvent('update-filename-recorder', {
      detail: { filename },
    }),
  );
  await localStorage.setItem('filenameRecorder', filename);
}

export function shortStringByNumber(str: string, num: number) {
  if (str.length > num) return `${str.substring(0, num)}...`;

  return str;
}

export const isDisplayed = (flag: boolean) => {
  return flag ? 'block' : 'none';
};

export const hideComponent = (
  parentId: string,
  classnameToBeHidden: string,
) => {
  const teamFilesFilterElement = document.getElementById(parentId);

  if (teamFilesFilterElement) {
    for (const child of teamFilesFilterElement.children) {
      const element = Array.from(child.children).find(
        (value) => value.className === classnameToBeHidden,
      );
      if (element) {
        (element as HTMLElement).style.display = 'none';
      }
    }
  }
};

export const screenWidthIsLessThan = (pixels: number) => {
  return window.innerWidth <= pixels;
};

const convertLanguageToDateFnsLocale = (language: string) => {
  switch (language) {
    case 'es':
      return es;
    case 'en':
      return enUS;
    case 'pt_BR':
      return ptBR;
    default:
      return enUS;
  }
};

export const generateRecordingName = (language: string) => {
  const date = new Date();
  return format(date, 'MMM dd,yyyy', {
    locale: convertLanguageToDateFnsLocale(language),
  });
};

export const isValidFilename = (name: string) => {
  const invalidCharacters = /[/?<>\\:*|"]/g;

  if (invalidCharacters.test(name)) {
    return false;
  }

  return true;
};

export const isAudioEncoderAvailable = () => {
  return 'AudioEncoder' in window;
};

export const isAudioFile = (mime: string | undefined) => {
  if (!mime) return false;

  return /^audio/.test(mime);
};

export const isVideoFile = (mime: string | undefined) => {
  if (!mime) return false;

  return /^video/.test(mime);
};

export const isAudioZipFile = (name: string) => {
  const fileName = name.toLowerCase();

  return fileName.endsWith('.zip');
};

export const isValidAudioZipFile = async (blob: Blob) => {
  const zip = await JSZip.loadAsync(blob);
  const files = Object.keys(zip.files);

  return files.includes('project.json');
};

export const getAmountBlobsFromZip = async (blob: Blob) => {
  const zip = await JSZip.loadAsync(blob);
  const files = Object.keys(zip.files);

  return files.length - 2;
};

export const getLongestDurationOfBlobs = async (blob: Blob) => {
  const zip = await JSZip.loadAsync(blob);
  const blobFileKeys = Object.keys(zip.files).filter((item) =>
    item.match('blob.'),
  );

  const recordingsDurationList: number[] = [];

  await Promise.all(
    blobFileKeys.map(async (item) => {
      const blobFile = await zip.file(item)?.async('blob');
      if (blobFile) {
        const duration = await getBlobDuration(blobFile);
        recordingsDurationList.push(duration);
      }
    }),
  );

  return Math.max(...recordingsDurationList);
};
