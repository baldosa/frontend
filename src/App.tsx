import './App.css';
import '@patternfly/react-core/dist/styles/base.css';
import { RouterProvider } from 'react-router-dom';
import { Router } from './routes.tsx';
import { FunctionComponent, useEffect, useState } from 'react';
import { ContextClientProvider } from './pages/AppLayout/Contexts.tsx';
import OpenAPIClientAxios from 'openapi-client-axios';
import { useSelector } from 'react-redux';
import { generateOpenAPIClient } from './api/Client.ts';
import { getServer } from './redux/reducers/auth.ts';

const App: FunctionComponent = () => {
  const [getClient, setClient] = useState<OpenAPIClientAxios | undefined>(undefined);
  const serverData = useSelector(getServer);

  useEffect(() => {
    if(!getClient && serverData.url) {
      setClient(generateOpenAPIClient(serverData.url));
    }
  }, [getClient, serverData]);

  return (
    <ContextClientProvider
      contextProps={{getOpenAPIClient: getClient, setOpenAPIClient: setClient}}
      children={ <RouterProvider router={Router} /> }
    />
  );
}

export default App;
