import { Button, Title } from '@patternfly/react-core';
import React, { Fragment } from 'react';
import { TimesIcon } from '@patternfly/react-icons/dist/esm/icons/times-icon';

export const TourStep: React.FunctionComponent<{
  title: string;
  className: string;
  belowTheImage: boolean;
  image: React.ReactElement;
  content: string;
  closeModal: () => void;
}> = (props: {
  title: string;
  className: string;
  belowTheImage: boolean;
  image: React.ReactElement;
  content: string;
  closeModal: () => void;
}) => {
  return (
    <Fragment>
      <div className={props.className}>
        <Button
          className={'close_button'}
          variant={'plain'}
          children={<TimesIcon onClick={props.closeModal} />}
        />
        <Fragment>
          {props.image}
          {props.belowTheImage && (
            <Title
              className={'wizard-step-title'}
              style={{ fontSize: '26px' }}
              headingLevel={'h1'}
              children={props.title}
            />
          )}
        </Fragment>
      </div>
      <p
        className={'wizard-step-body'}
        dangerouslySetInnerHTML={{ __html: props.content }}
      />
      {!props.belowTheImage && (
        <Title
          className={'wizard-step-title'}
          style={{ fontSize: '28px', paddingTop: '25px' }}
          headingLevel={'h1'}
          children={props.title}
        />
      )}
    </Fragment>
  );
};
