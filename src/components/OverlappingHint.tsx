import {
  Button,
  Hint,
  HintBody,
  HintFooter,
  HintTitle,
} from '@patternfly/react-core';
import * as React from 'react';
import { useNavigate } from 'react-router-dom';

export const OverlappingHint: React.FunctionComponent<{
  title: string;
  body: string;
  footer: string;
  buttonContentTag: string;
  route: string;
  isDisplayed: boolean;
}> = (props: {
  title: string;
  body: string;
  footer: string;
  buttonContentTag: string;
  route: string;
  isDisplayed: boolean;
}) => {
  const navigate = useNavigate();

  return (
    <div className={'overlapping-view'} hidden={!props.isDisplayed}>
      <Hint className={'component'}>
        <HintTitle
          children={<span dangerouslySetInnerHTML={{ __html: props.title }} />}
        />
        <HintBody
          children={<span dangerouslySetInnerHTML={{ __html: props.body }} />}
        />
        <HintFooter
          children={<span dangerouslySetInnerHTML={{ __html: props.footer }} />}
        />
        <Button
          className={'button'}
          variant={'plain'}
          children={props.buttonContentTag}
          onClick={() => navigate(props.route)}
        />
      </Hint>
    </div>
  );
};
