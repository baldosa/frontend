import React, { useState } from 'react';
import { Breadcrumb, BreadcrumbItem } from '@patternfly/react-core';
import { useNavigate, useOutletContext } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import { PlaylistContextEnum, PlaylistStatusEnum } from '../enums';
import { getActiveRecordingState } from '../redux/reducers/recordings';
import AbortAudioRecordingModal from '../components/Waveform/ui/AbortAudioRecordingModal';
import { useWaveformEmitter } from '../hooks/useWaveformEmitter';
import { toolsFullPath } from '../routes.tsx';
import { ContextPropsType } from '../pages/AppLayout/types.ts';

type Props = {
  context: PlaylistContextEnum.RECORDER | PlaylistContextEnum.EDITOR;
  currentPage: string;
};

export const BreadcrumbToolsContext = React.memo(
  ({ context, currentPage }: Props) => {
    const navigate = useNavigate();
    const [isModalOpen, setModalOpen] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const { emitter } = useOutletContext<ContextPropsType>();
    const audioState: PlaylistStatusEnum = useSelector(getActiveRecordingState);
    const { emitterClear } = useWaveformEmitter({
      emitter,
    });
    const { t } = useTranslation();

    function navigateToToolsPage() {
      navigate(toolsFullPath());
    }

    function handleNavigate() {
      if (![PlaylistStatusEnum.NONE].includes(audioState)) setModalOpen(true);
      else navigateToToolsPage();
    }

    function handleClose() {
      setModalOpen(false);
    }

    function onAbortAudioRecording() {
      setIsLoading(true);
      emitterClear();

      setTimeout(() => {
        setModalOpen(false);
        setIsLoading(false);
        navigateToToolsPage();
      }, 1000);
    }

    return (
      <>
        <AbortAudioRecordingModal
          isModalOpen={isModalOpen}
          handleClose={handleClose}
          onConfirm={onAbortAudioRecording}
          isLoading={isLoading}
        />
        <Breadcrumb
          ouiaId="breadcrumb-tools"
          className="breadcrumb-tools"
          style={{ cursor: 'pointer' }}
        >
          <BreadcrumbItem onClick={handleNavigate}>
            {t('tools.header')}
          </BreadcrumbItem>
          {context === PlaylistContextEnum.EDITOR && (
            <BreadcrumbItem onClick={handleNavigate}>
              {t('title.tools.recorder')}
            </BreadcrumbItem>
          )}
          <BreadcrumbItem isActive>{currentPage}</BreadcrumbItem>
        </Breadcrumb>
      </>
    );
  },
);
