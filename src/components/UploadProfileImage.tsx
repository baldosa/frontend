import {
  ChangeEvent,
  Dispatch,
  FunctionComponent,
  useEffect,
  useRef,
  useState,
} from 'react';
import { Avatar, Button, FormGroup, Title } from '@patternfly/react-core';
import { useTranslation } from 'react-i18next';
import avatar from '../assets/pages/User/Configuration/avatar.png';
import {
  convertBase64ToBlobURL,
  convertBlobURLToBase64,
} from '../utils/logics/fileUtils.ts';
import { addAlert } from '../redux/actions/alert.ts';
import { AnyAction } from '@reduxjs/toolkit';
import { useDispatch } from 'react-redux';

export const UploadProfileImage: FunctionComponent<{
  title: string;
  base64Image: string | null;
  onUpload(base64: string | null): void;
}> = (props) => {
  const [getCurrentImage, setCurrentImage] = useState<string | null>(null);
  const [getNewImage, setNewImage] = useState<string | null>(null);

  const fileInputRef = useRef<HTMLInputElement>(null);

  const { t } = useTranslation();

  const dispatch: Dispatch<AnyAction> = useDispatch();

  useEffect(() => {
    if (props.base64Image !== null) {
      const blobUrl = convertBase64ToBlobURL(props.base64Image);
      setCurrentImage(blobUrl);
      setNewImage(blobUrl);
    } else {
      setCurrentImage(avatar);
      setNewImage(avatar);
    }
  }, [props.base64Image]);

  const onImageUpload = (): void => {
    if (fileInputRef.current) {
      fileInputRef.current.click();
    }
  };

  const onImageUploaded = (
    event: ChangeEvent<HTMLInputElement>,
  ): void => {
    const selectedFile = event.target.files?.[0];
    if (selectedFile && selectedFile.size < 2097152) {
      const imageUrl = URL.createObjectURL(selectedFile);
      setNewImage(imageUrl);
      convertBlobURLToBase64(imageUrl)
        .then((response) => props.onUpload(response))
        .catch((error) => console.error(error));
    } else {
      addAlert('danger', t('user.configuration.organization_form.logo.alert.error'))(dispatch);
      setNewImage(getCurrentImage);
    }
  };

  const onClearImage = (): void => {
    props.onUpload(null);
  };

  return (
    <FormGroup style={{ marginBottom: '25px' }}>
      <Title headingLevel={'h6'} children={props.title} />
      <div style={{ display: 'flex', marginTop: '0.5rem' }}>
        <Button
          className={'image-profile-charger'}
          onClick={onImageUpload}
          /* eslint-disable-next-line @typescript-eslint/no-non-null-assertion */
          children={<Avatar src={getNewImage!} alt="avatar" size="lg" />}
        />
        <input
          ref={fileInputRef}
          type={'file'}
          accept={'image/*'}
          onChange={onImageUploaded}
          style={{ display: 'none' }}
        />
        <div style={{ display: 'flex', flexDirection: "column" }}>
          <Button
            className={'profile-image-upload-button'}
            variant={'secondary'}
            onClick={onImageUpload}
            children={t('update_image_profile.upload_image_button')}
          />
          <Button
            className={'profile-image-upload-button'}
            style={{ marginTop: '1rem'}}
            variant={'secondary'}
            onClick={onClearImage}
            children={t('update_image_profile.clear_image_button')}
          />
        </div>
      </div>
      <Title
        headingLevel={'h6'}
        style={{ color: 'gray' }}
        children={t('update_image_profile.max_image_size')}
      />
    </FormGroup>
  );
};
