import {
  getValidation,
  ValidationResult,
  ValidationRule,
} from './Auth/ValidationRules.ts';
import React, { Dispatch, useEffect, useState } from 'react';
import { HelperText, HelperTextItem } from '@patternfly/react-core';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import {
  updateBlockingValidationRequest,
  updateNonBlockingValidationRequest,
} from '../redux/actions/generalActions.ts';
import {
  getBlockingValidationRequest,
  getNonBlockingValidationRequest,
  HelperTextData,
} from '../redux/reducers/generalActions.ts';
import { AnyAction } from '@reduxjs/toolkit';

export interface ExtendedHelperText {
  className?: string;
  origin: string;
  fieldNames: string[];
  validationRules: ValidationRule[];
  defaultMessage?: string | undefined;
}

export const ExtendedHelperText: React.FunctionComponent<ExtendedHelperText> = (
  props: ExtendedHelperText,
) => {
  const [validationStatus, setValidationStatus] = useState<
    'default' | 'success' | 'warning' | 'error' | undefined
  >('default');
  const [validationMessage, setValidationMessage] = useState('');
  const blockingErrors: Record<string, HelperTextData> = useSelector(
    getBlockingValidationRequest,
  );
  const nonBlockingErrors: Record<string, HelperTextData> = useSelector(
    getNonBlockingValidationRequest,
  );
  const { t } = useTranslation();
  const dispatch: Dispatch<AnyAction> = useDispatch();

  useEffect(() => {
    setValidationMessage(props.defaultMessage ? props.defaultMessage : '');
    setValidationStatus('default');
  }, [props.defaultMessage]);

  useEffect(() => {
    validation(blockingErrors, updateBlockingValidationRequest);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [blockingErrors]);

  useEffect(() => {
    validation(nonBlockingErrors, updateNonBlockingValidationRequest);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [nonBlockingErrors]);

  const resetStatus = () => {
    setValidationMessage('');
    setValidationStatus('default');
  };

  const validation = (
    record: Record<string, HelperTextData>,
    updateValidationRequest: (
      fieldNames: string[],
      statusCode: string,
      errorCode: string | undefined,
      status: 'error' | 'success',
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      value: any,
    ) => (dispatch: Dispatch<AnyAction>) => void,
  ): void => {
    const validationData: HelperTextData | undefined =
      record[props.fieldNames.join(' ')];
    if (validationData) {
      if (validationData.variant === 'warning') {
        const errorCode: string | undefined = props.validationRules
          .map((rule: ValidationRule) =>
            // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
            rule(props.origin, props.fieldNames, validationData.value!),
          )
          .find((value: string | undefined) => value);

        if (errorCode) {
          updateValidationRequest(
            props.fieldNames,
            validationData.status_code,
            errorCode,
            'error',
            validationData.value,
          )(dispatch);
        } else {
          updateValidationRequest(
            props.fieldNames,
            validationData.status_code,
            undefined,
            'success',
            validationData.value,
          )(dispatch);
        }
      }

      if (validationData.variant === 'error') {
        const result: ValidationResult | undefined = getValidation(
          validationData.status_code,
          validationData.error_code,
        );
        if (result) {
          setValidationMessage(result.key_message);
          setValidationStatus(result.status);
        } else {
          updateValidationRequest(
            props.fieldNames,
            validationData.status_code,
            undefined,
            'success',
            validationData.value,
          )(dispatch);
        }
      }

      if (validationData.variant === 'success') {
        resetStatus();
      }
    }
  };

  return (
    <HelperText className={props.className}>
      <HelperTextItem variant={validationStatus}>
        <div style={{ height: '42px' }}>{t(validationMessage)}</div>
      </HelperTextItem>
    </HelperText>
  );
};
