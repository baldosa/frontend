import React, { Dispatch, useEffect, useState } from 'react';
import { FormGroup, TextInput } from '@patternfly/react-core';
import { INTERNAL_ERROR } from './Auth/ValidationRules.ts';
import { ExtendedHelperText } from './ExtendedHelperText.tsx';
import {
  getBlockingValidationRequest,
  getNonBlockingValidationRequest,
  HelperTextData,
} from '../redux/reducers/generalActions.ts';
import { useDispatch, useSelector } from 'react-redux';
import { AnyAction } from '@reduxjs/toolkit';
import {
  addBlockingValidationRequest,
  addNonBlockingValidationRequest,
  updateBlockingValidationRequest,
  updateNonBlockingValidationRequest,
} from '../redux/actions/generalActions.ts';

export interface ExtendedTextInput {
  id: string;
  origin: string;
  fieldNames: string[];
  className?: string;
  isRequired: boolean | undefined;
  type: 'text' | 'email' | 'number' | 'password' | 'url';
  label: string | undefined;
  labelIcon?: React.ReactElement;
  placeholder: string | undefined;
  helperText: string | undefined;
  setValue: (value: string) => void;
  getValue: string | null;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  rules: any[];
}

export const ExtendedTextInput: React.FunctionComponent<ExtendedTextInput> = (
  props: ExtendedTextInput,
) => {
  const [validationStatus, setValidationStatus] = useState<
    'default' | 'success' | 'warning' | 'error' | undefined
  >('default');
  const [isFirstLoad, setIsFirstLoad] = useState(true);
  const [getValue, setValue] = useState<string | null>(null);
  const blockingValidationRequest: Record<string, HelperTextData> = useSelector(
    getBlockingValidationRequest,
  );
  const nonBlockingValidationRequest: Record<string, HelperTextData> =
    useSelector(getNonBlockingValidationRequest);

  const dispatch: Dispatch<AnyAction> = useDispatch();

  useEffect(() => {
    if (props.isRequired) {
      addBlockingValidationRequest(
        props.fieldNames,
        INTERNAL_ERROR,
        'default',
        undefined,
      )(dispatch);
    } else {
      addNonBlockingValidationRequest(
        props.fieldNames,
        INTERNAL_ERROR,
        'default',
        undefined,
      )(dispatch);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (isFirstLoad && props.getValue) {
      setIsFirstLoad(false);
      setValue(props.getValue);
      props.setValue(props.getValue);
        if(props.isRequired) {
          addBlockingValidationRequest(
            props.fieldNames,
            INTERNAL_ERROR,
            'warning',
            props.getValue,
          )(dispatch);
        } else {
          addNonBlockingValidationRequest(
            props.fieldNames,
            INTERNAL_ERROR,
            'warning',
            props.getValue,
          )(dispatch);
        }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.getValue]);

  useEffect(() => {
    if (props.fieldNames) {
      const validationData: HelperTextData | undefined =
        blockingValidationRequest[props.fieldNames.join(' ')];
      validationData &&
        validationData.variant &&
        setValidationStatus(validationData.variant);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [blockingValidationRequest]);

  useEffect(() => {
    if (props.fieldNames) {
      const validationData: HelperTextData | undefined =
        nonBlockingValidationRequest[props.fieldNames.join(' ')];
      validationData &&
        validationData.variant &&
        setValidationStatus(validationData.variant);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [nonBlockingValidationRequest]);

  const handleTextInputChange = (value: string): void => {
    if (props.isRequired) {
      updateBlockingValidationRequest(
        props.fieldNames,
        INTERNAL_ERROR,
        undefined,
        'warning',
        value,
      )(dispatch);
    } else {
      updateNonBlockingValidationRequest(
        props.fieldNames,
        INTERNAL_ERROR,
        undefined,
        'warning',
        value,
      )(dispatch);
    }
    setValue(value);
    props.setValue(value);
  };

  return (
    <FormGroup
      className={props.className}
      label={props.label}
      isRequired={props.isRequired}
      validated={validationStatus}
    >
      <div style={{ display: 'flex' }}>
        <TextInput
          isRequired={props.isRequired}
          type={props.type}
          id={props.id}
          placeholder={props.placeholder}
          onChange={handleTextInputChange}
          value={getValue == null ? '' : getValue}
          validated={validationStatus}
          autoComplete={props.type == 'password' ? 'current-password' : 'on'}
        />
        {props.labelIcon}
      </div>
      <ExtendedHelperText
        origin={props.origin}
        fieldNames={props.fieldNames}
        validationRules={props.rules}
        defaultMessage={props.helperText}
      />
    </FormGroup>
  );
};
