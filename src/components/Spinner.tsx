import { Spinner as PatternflySpinner } from '@patternfly/react-core';
import { useTranslation } from 'react-i18next';

type Props = {
  ariaLabel: string
}

export default function Spinner({
  ariaLabel = "",
  ...props
}: Props) {
  const { t } = useTranslation();
  const _ariaLabel = ariaLabel === "" ? t("general.loading") : ariaLabel

  return (
    <PatternflySpinner isSVG aria-label={_ariaLabel} {...props} />
  )
}
