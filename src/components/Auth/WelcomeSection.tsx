import { Button, CardBody, TextContent, Title } from '@patternfly/react-core';
import { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';
import { NavigateFunction, useNavigate } from 'react-router-dom';
import { loginFullPath } from '../../routes.tsx';
import cardBackground from '../../assets/pages/Auth/auth_page_main_section_body_card_successfull_background.png';

export const WelcomeSection: FunctionComponent = () => {
  const { t } = useTranslation();
  const navigate: NavigateFunction = useNavigate();

  return (
    <CardBody>
      <TextContent
        className={'auth-page-body-card-successful'}
        style={{ backgroundImage: `url(${cardBackground})` }}
      >
        <Title className={'align-center-text'} headingLevel="h1">
          {t('auth_page.body_card.successful.creation')}
        </Title>
        <br />
        <Title className={'align-center-text'} headingLevel="h2">
          {t('auth_page.body_card.successful.congratulations')}
        </Title>
        <br />
        <p className={'align-center-text'}>
          {t('auth_page.body_card.successful.message')}
        </p>
        <br />
        <Title className={'align-center-text'} headingLevel="h2">
          {t('auth_page.body_card.successful.welcome_to_colmena')}
        </Title>
        <Button
          className={'authButton'}
          style={{ marginTop: '15px', width: 'auto' }}
          children={t('auth_page.body_card.successful.login_button')}
          onClick={() => navigate(loginFullPath())}
        />
      </TextContent>
    </CardBody>
  );
};
