export class ProfileProperties {
  usernameLabel: string;
  passwordLabel: string;
  buttonContent: string;
  usernamePlaceholder: string;
  passwordPlaceholder: string;
  usernameHelperText: string;
  passwordHelperText: string;
  title: string;

  constructor(
    usernameLabel: string,
    passwordLabel: string,
    buttonContent: string,
    usernamePlaceholder: string,
    passwordPlaceholder: string,
    usernameHelperText: string,
    passwordHelperText: string,
    title: string,
  ) {
    this.usernameLabel = usernameLabel;
    this.passwordLabel = passwordLabel;
    this.buttonContent = buttonContent;
    this.usernamePlaceholder = usernamePlaceholder;
    this.passwordPlaceholder = passwordPlaceholder;
    this.usernameHelperText = usernameHelperText;
    this.passwordHelperText = passwordHelperText;
    this.title = title;
  }
}
