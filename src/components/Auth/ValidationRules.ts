export const INTERNAL_ERROR = 'INTERNAL_ERROR';
export const BAD_REQUEST_ERROR = 'ERR_BAD_REQUEST';
export const NETWORK_ERROR = 'ERR_NETWORK';

export const LOGIN = 'LOGIN';
export const SIGNUP = 'SIGNUP';
export const PROFILE = 'PROFILE';
export const ORGANIZATION = 'ORGANIZATION';
export const INVITATION = 'INVITATION';
export const PASSWORD_RESET = 'PASSWORD_RESET';
export const SERVER = 'SERVER';

export const ACCOUNT_TYPE = 'ACCOUNT_TYPE';
export const COUNTRIES = 'COUNTRIES';
export const FULLNAME = 'FULLNAME';
export const USERNAME = 'USERNAME';
export const PASSWORD = 'PASSWORD';
export const PASSWORD_CONFIRM = 'PASSWORD_CONFIRM';
export const GENERAL = 'GENERAL';
export const EMAIL = 'EMAIL';
export const LANGUAGES = 'LANGUAGES';
export const SERVER_NAME = 'SERVER_NAME';
export const SERVER_ADDRESS = 'SERVER_ADDRESS';

const statusCodeMap: Map<string, string[]> = new Map();
statusCodeMap.set(BAD_REQUEST_ERROR, [USERNAME, PASSWORD, EMAIL, FULLNAME]);
statusCodeMap.set(INTERNAL_ERROR, [USERNAME, PASSWORD, EMAIL, FULLNAME, SERVER_ADDRESS, SERVER_NAME]);
statusCodeMap.set(NETWORK_ERROR, [GENERAL, COUNTRIES, LANGUAGES, ACCOUNT_TYPE]);

export const getValidationKey = (
  statusCode: string,
  errorCode: string | undefined,
): string => {
  if (errorCode) {
    const key: string | undefined = statusCodeMap
      .get(statusCode)
      ?.find((key) => errorCode.includes(key));
    return key ? key : GENERAL;
  } else {
    return GENERAL;
  }
};

export interface ValidationResult {
  key_message: string;
  status: 'default' | 'success' | 'warning' | 'error' | undefined;
}

export type ValidationRule = (
  origin: string,
  fieldName: string[],
  value: string,
) => string | undefined;

const errorDictionary: Map<string[], Map<string, ValidationResult>> = new Map();
const valueErrorDictionary: Map<string, ValidationResult> = new Map();
const networkErrorDictionary: Map<string, ValidationResult> = new Map();

const valueErrorKey: string[] = [BAD_REQUEST_ERROR, INTERNAL_ERROR];
const networkErrorKey: string[] = [NETWORK_ERROR];

errorDictionary.set(valueErrorKey, valueErrorDictionary);
errorDictionary.set(networkErrorKey, networkErrorDictionary);

// SERVER - ERROR SECTION
valueErrorDictionary.set(`ERROR_${SERVER}_${SERVER_NAME}_INVALID_EXTENSION`, {
  key_message: 'error_code.server.server_name.invalid_extension',
  status: 'error',
});

valueErrorDictionary.set(`ERROR_${SERVER}_${SERVER_NAME}_EMPTY`, {
  key_message: 'error_code.server.server_name.empty',
  status: 'error',
});

valueErrorDictionary.set(`ERROR_${SERVER}_${SERVER_ADDRESS}_INVALID_FORMAT`, {
  key_message: 'error_code.server.server_address.invalid_format',
  status: 'error',
});

valueErrorDictionary.set(`ERROR_${SERVER}_${SERVER_ADDRESS}_EMPTY`, {
  key_message: 'error_code.server.server_address.empty',
  status: 'error',
});

// LOGIN - ERROR SECTION
valueErrorDictionary.set(`ERROR_${LOGIN}_${USERNAME}_INCORRECT`, {
  key_message: 'error_code.login.username.incorrect',
  status: 'error',
});
valueErrorDictionary.set(`ERROR_${LOGIN}_${PASSWORD}_INCORRECT`, {
  key_message: 'error_code.login.password.incorrect',
  status: 'error',
});

valueErrorDictionary.set(`ERROR_${LOGIN}_${USERNAME}_EMPTY`, {
  key_message: 'error_code.login.username.empty',
  status: 'error',
});

valueErrorDictionary.set(`ERROR_${LOGIN}_${PASSWORD}_EMPTY`, {
  key_message: 'error_code.login.password.empty',
  status: 'error',
});

// SIGNUP - ERROR SECTION
valueErrorDictionary.set(`ERROR_${SIGNUP}_${USERNAME}_EMPTY`, {
  key_message: 'error_code.signup.username.empty',
  status: 'error',
});
valueErrorDictionary.set(`ERROR_${SIGNUP}_${PASSWORD}_EMPTY`, {
  key_message: 'error_code.signup.password.empty',
  status: 'error',
});
valueErrorDictionary.set(`ERROR_${SIGNUP}_${USERNAME}_INVALID_FORMAT`, {
  key_message: 'error_code.signup.username.invalid_format',
  status: 'error',
});
valueErrorDictionary.set(`ERROR_${SIGNUP}_${PASSWORD}_INVALID_FORMAT`, {
  key_message: 'error_code.signup.password.invalid_format',
  status: 'error',
});
valueErrorDictionary.set(`ERROR_${SIGNUP}_${PASSWORD}_INVALID_EXTENSION`, {
  key_message: 'error_code.signup.password.invalid_extension',
  status: 'error',
});
valueErrorDictionary.set(`ERROR_${SIGNUP}_${PASSWORD_CONFIRM}_INVALID_FORMAT`, {
  key_message: 'error_code.signup.password.invalid_format',
  status: 'error',
});
valueErrorDictionary.set(
  `ERROR_${SIGNUP}_${PASSWORD_CONFIRM}_INVALID_EXTENSION`,
  {
    key_message: 'error_code.signup.password.invalid_extension',
    status: 'error',
  },
);
valueErrorDictionary.set(`ERROR_${SIGNUP}_${PASSWORD_CONFIRM}_EMPTY`, {
  key_message: 'error_code.signup.password.empty',
  status: 'error',
});

// USER PROFILE - ERROR SECTION
valueErrorDictionary.set(`ERROR_${PROFILE}_${FULLNAME}_EMPTY`, {
  key_message: 'error_code.profile.fullname.empty',
  status: 'error',
});
valueErrorDictionary.set(`ERROR_${PROFILE}_${EMAIL}_EMPTY`, {
  key_message: 'error_code.profile.email.empty',
  status: 'error',
});
valueErrorDictionary.set(`ERROR_${PROFILE}_${FULLNAME}_INVALID_FORMAT`, {
  key_message: 'error_code.profile.fullname.invalid_format',
  status: 'error',
});

valueErrorDictionary.set(`ERROR_${PROFILE}_${EMAIL}_INVALID_FORMAT`, {
  key_message: 'error_code.profile.email.invalid_format',
  status: 'error',
});

// ORGANIZATION DETAILS - ERROR SECTION
valueErrorDictionary.set(`ERROR_${ORGANIZATION}_${FULLNAME}_EMPTY`, {
  key_message: 'error_code.organization.fullname.empty',
  status: 'error',
});
valueErrorDictionary.set(`ERROR_${ORGANIZATION}_${EMAIL}_EMPTY`, {
  key_message: 'error_code.organization.email.empty',
  status: 'error',
});
valueErrorDictionary.set(`ERROR_${ORGANIZATION}_${FULLNAME}_INVALID_FORMAT`, {
  key_message: 'error_code.organization.fullname.invalid_format',
  status: 'error',
});
valueErrorDictionary.set(`ERROR_${ORGANIZATION}_${EMAIL}_INVALID_FORMAT`, {
  key_message: 'error_code.organization.email.invalid_format',
  status: 'error',
});
valueErrorDictionary.set(`ERROR_${INVITATION}_OWN_${EMAIL}`, {
  key_message: 'user.configuration.invitation_form.hint.error_own_email',
  status: 'error',
});
valueErrorDictionary.set(`ERROR_${INVITATION}_${EMAIL}_ALREADY_INVITED`, {
  key_message:
    'user.configuration.invitation_form.hint.error_email_already_invited',
  status: 'error',
});
valueErrorDictionary.set(`ERROR_${PASSWORD_RESET}_${PASSWORD}_MISMATCH`, {
  key_message: 'password_reset_confirm.form.password_mismatch',
  status: 'error',
});

// FIXME: When we finish generating the errors in the back, this section will be modified.

networkErrorDictionary.set(`ERROR_NOT_REGISTERED`, {
  key_message: 'error_code.general.connection_refused',
  status: 'error',
});

networkErrorDictionary.set(`ERROR_${ORGANIZATION}_UNOBTAINABLE_${COUNTRIES}`, {
  key_message:
    'user.configuration.organization_form.language_select.message_error',
  status: 'error',
});

networkErrorDictionary.set(`ERROR_${ORGANIZATION}_UNOBTAINABLE_${LANGUAGES}`, {
  key_message:
    'user.configuration.organization_form.country_select.message_error',
  status: 'error',
});

networkErrorDictionary.set(`ERROR_${INVITATION}_UNOBTAINABLE_${ACCOUNT_TYPE}`, {
  key_message:
    'user.configuration.invitation_form.account_type_select.general_error',
  status: 'error',
});

valueErrorDictionary.set(`ERRORS_EMAIL_NOT_FOUND`, {
  key_message: 'error_code.login.username.incorrect',
  status: 'error',
});

valueErrorDictionary.set(`ERRORS_USERNAME_NOT_FOUND`, {
  key_message: 'error_code.login.username.incorrect',
  status: 'error',
});

valueErrorDictionary.set(`ERRORS_EMAIL_NOT_FOUND`, {
  key_message: 'error_code.login.username.incorrect',
  status: 'error',
});

valueErrorDictionary.set(`ERRORS_WRONG_PASSWORD`, {
  key_message: 'error_code.login.password.incorrect',
  status: 'error',
});

valueErrorDictionary.set(`ERRORS_INVALID_INVITATION_TOKEN`, {
  key_message: 'error_code.signup.general_error.invalid_token',
  status: 'error',
});

export const getValidation = (
  statusCode: string,
  error_code: string,
): ValidationResult | undefined => {
  const key: string[] | undefined = Array.from(errorDictionary.keys()).find(
    (array: string[]) => array.includes(statusCode),
  );

  if (key) {
    const potentialErrorDictionary: Map<string, ValidationResult> | undefined =
      errorDictionary.get(key);
    if (potentialErrorDictionary) {
      const potentialValidation: ValidationResult | undefined =
        potentialErrorDictionary.get(error_code);
      if (potentialValidation) {
        return potentialValidation;
      }
    }
  }
  return undefined;
};

export const isEmpty: ValidationRule = (
  origin: string,
  fieldsName: string[],
  value: string,
): string | undefined => {
  return fieldsName.length > 0 && value.length === 0
    ? `ERROR_${origin}_${fieldsName[0]}_EMPTY`
    : undefined;
};

export const isAnInvalidFormat: ValidationRule = (
  origin: string,
  fieldsName: string[],
  value: string,
): string | undefined => {
  const fieldName: string | undefined = fieldsName.find((fieldName) => {
    switch (fieldName) {
      case EMAIL:
        return !/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/.test(value)
          ? EMAIL
          : undefined;
      case USERNAME:
        return !/^\S+$/.test(value) ? USERNAME : undefined;
      case PASSWORD:
        return !/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[\W_]).+$/.test(value)
          ? PASSWORD
          : undefined;
      case PASSWORD_CONFIRM:
        return !/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[\W_]).+$/.test(value)
          ? PASSWORD_CONFIRM
          : undefined;
      case FULLNAME:
        return !/^[a-zA-Z\s\u00C0-\u00FF.,'-]+$/.test(value)
          ? FULLNAME
          : undefined;
      case SERVER_NAME:
        return !/^\S+$/.test(value)
          ? SERVER_NAME
          : undefined;
      case SERVER_ADDRESS:
        return !/^(https?:\/\/)[a-zA-Z0-9.:]+$/.test(value)
          ? SERVER_ADDRESS
          : undefined;
    }
  });

  return fieldName ? `ERROR_${origin}_${fieldName}_INVALID_FORMAT` : undefined;
};

export const isAnInvalidExtension: ValidationRule = (
  origin: string,
  fieldsName: string[],
  value: string,
): string | undefined => {
  const fieldName: string | undefined = fieldsName.find((fieldName) => {
    switch (fieldName) {
      case USERNAME:
        return undefined;
      case PASSWORD:
        return !/^.{12,}$/.test(value) ? PASSWORD : undefined;
      case PASSWORD_CONFIRM:
        return !/^.{12,}$/.test(value) ? PASSWORD_CONFIRM : undefined;
      case SERVER_NAME:
        return /^.{20,}$/.test(value) ? SERVER_NAME : undefined;
    }
  });

  return fieldName
    ? `ERROR_${origin}_${fieldName}_INVALID_EXTENSION`
    : undefined;
};
