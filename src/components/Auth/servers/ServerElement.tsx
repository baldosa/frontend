import { Dispatch, FunctionComponent, useEffect, useState } from 'react';
import {
  Dropdown,
  DropdownItem,
  DropdownPosition,
  KebabToggle,
  DataListItem,
  DataListCell,
  DataListItemRow,
  DataListItemCells,
  DataListAction,
} from '@patternfly/react-core';
import { useTranslation } from 'react-i18next';
import { useServerContext } from '../../../pages/Auth/ServersPage.tsx';
import { ServerElementDataType } from './ServerDataType.ts';
import { serverIdKey } from '../../../redux/reducers/auth.ts';
import { updateServerData } from '../../../redux/actions/auth.ts';
import { AnyAction } from '@reduxjs/toolkit';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { StatusIcon } from './StatusIcon.tsx';
import { getPreviousUrl } from '../../../redux/reducers/auth.ts';
import { useSelector } from 'react-redux/es/hooks/useSelector';
import { loginFullPath } from '../../../routes.tsx';
import { useContextClient } from '../../../pages/AppLayout/Contexts.tsx';
import { getServerStatus as serverStatus} from '../../../service/server.ts';
import { generateOpenAPIClient } from '../../../api/Client.ts';
import OpenAPIClientAxios from 'openapi-client-axios';

import { savePreviousAuthUrl } from '../../../redux/actions/auth.ts';
export const ServerElement: FunctionComponent<ServerElementDataType> = (props) => {
  const [getServerStatus, setServerStatus] = useState<boolean>(false);
  const [getClient, setClient] = useState<OpenAPIClientAxios | undefined>(undefined);
  const [isOpen, setIsOpen] = useState<boolean>(false);

  const { setOpenAPIClient } = useContextClient();
  const dispatch: Dispatch<AnyAction> = useDispatch();
  const navigate = useNavigate();

  const { setActualServerData, toggleModal, deleteServer } = useServerContext();
  const { t } = useTranslation();
  const previousUrl = useSelector(getPreviousUrl)

  const onToggle = (isOpen: boolean) => {
    setIsOpen(isOpen);
  };

  const onSelect = () => {
    setIsOpen(!isOpen);
  };

  const retrieveServerStatus = () => async (client: OpenAPIClientAxios): Promise<void> => {
    setServerStatus(await serverStatus()(client));
  }

  useEffect(() => {
    if(props.url) {
      setClient(generateOpenAPIClient(props.url));
    }
  }, [props.url]);


  useEffect(() => {
    let getServerStatusInterval: NodeJS.Timeout;

    if(getClient) {
      retrieveServerStatus()(getClient);

      getServerStatusInterval = setInterval(() => {
        retrieveServerStatus()(getClient);
      }, 10000);

      return (): void => {
        clearInterval(getServerStatusInterval);
      };
    }
  }, [getClient]);

  const handleEdit = (): void => {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    setActualServerData!(props.id, props.name, props.url);
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    toggleModal!();
  };

  const handleConnect = () => (client: OpenAPIClientAxios): void => {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    const serverId: number = props.id!;
    localStorage.setItem(serverIdKey, String(serverId));
    updateServerData(serverId, props.name, props.url)(dispatch);
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    setOpenAPIClient!(client);
    if(previousUrl){
      savePreviousAuthUrl("")(dispatch)
      navigate(previousUrl)
    }
    else{
      navigate(loginFullPath())
    }
  }

  const handleDelete = () => {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    deleteServer!(props.id!)
  }

  return (
    <DataListItem>
      <DataListItemRow>
        <DataListItemCells
          style={{display: 'flex', flexDirection: 'row'}}
          dataListCells={[
            <DataListCell
              key={'server-status'}
              children={<StatusIcon status={getServerStatus}/>}
              width={1}
            />,
            <DataListCell
              key={'server-name'}
              style={{
                overflow: 'hidden',
                whiteSpace: 'nowrap',
                textOverflow: 'ellipsis',
                padding: '0.5rem',
              }}
              children={props.name}
              width={5}
            />,
          ]}
        />
        <DataListAction
          id={`${props.name}-action`}
          aria-label={`${props.name}-action`}
          aria-labelledby={`${props.name}-action`}
          isPlainButtonAction
          children={
            <Dropdown
              isPlain
              position={DropdownPosition.right}
              isOpen={isOpen}
              onSelect={onSelect}
              toggle={<KebabToggle onToggle={onToggle} />}
              // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
              menuAppendTo={() => document.getElementById(props.parentId)!}
              dropdownItems={[
                <DropdownItem
                  key={'connect_action'}
                  children={t('auth.server_list.element.action.connect')}
                  isDisabled={!getServerStatus}
                  /* eslint-disable-next-line @typescript-eslint/no-non-null-assertion */
                  onClick={() => handleConnect()(getClient!)}
                />,
                <DropdownItem
                  key={'edit_action'}
                  children={t('auth.server_list.element.action.edit')}
                  onClick={handleEdit}
                />,
                <DropdownItem
                key={'remove_action'}
                children={t('auth.server_list.element.action.delete')}
                onClick={handleDelete}
              />,
              ]}
            />
          }
        />
      </DataListItemRow>
    </DataListItem>
  );
};
