import {FunctionComponent} from 'react';
import {
  Icon, Tooltip,
} from '@patternfly/react-core';
import { useTranslation } from 'react-i18next';
import ExclamationCircleIcon
  from '@patternfly/react-icons/dist/esm/icons/exclamation-circle-icon';
import CheckCircleIcon from '@patternfly/react-icons/dist/esm/icons/check-circle-icon';

interface StatusIconPropsType {
    status: boolean;
}

export const StatusIcon: FunctionComponent<StatusIconPropsType> = (props) => {
    const { t } = useTranslation();
    return(
        <span>
           {props.status ?
                <Tooltip
                content={t('auth.server_list.element.status.success')}
                children={
                    <Icon
                    status={'success'}
                    size={'md'}
                    children={<CheckCircleIcon />}
                    />
                }
                />
                :
                <Tooltip
                content={t('auth.server_list.element.status.error')}
                children={
                    <Icon
                    status={'danger'}
                    size={'md'}
                    children={<ExclamationCircleIcon />}
                    />
                }
                />}
        </span>
    )
}