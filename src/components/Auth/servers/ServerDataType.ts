export type ServerCoreDataType = {
    name: string;
    url: string;
};
export type ServerDataType = ServerCoreDataType & {
    id: number | undefined;
}
export type ServerElementDataType = ServerDataType & {
    parentId:string;
}

export type ServerContextType = ServerDataType & {
    isOpen: boolean;
    toggleModal: (() => void) | undefined;
    clearServerData: (() => void) | undefined;
    updateIsRequired: (() => void) | undefined;
    setActualServerData: ((id: number | undefined, name: string, url: string) => void) | undefined;
    deleteServer: ((id:number) => void) | undefined;
};
