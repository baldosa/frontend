import { FunctionComponent, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useLocation } from 'react-router-dom';
import { getServer } from '../../../redux/reducers/auth';
import { useSelector } from 'react-redux/es/hooks/useSelector';
import { savePreviousAuthUrl } from '../../../redux/actions/auth.ts';
import { useDispatch } from 'react-redux'
import { Card, CardActions, CardHeader, Button} from '@patternfly/react-core';
import { StatusIcon } from './StatusIcon';
import { useTranslation } from 'react-i18next';
import { serverListFullPath } from '../../../routes';
import { useContextClient } from '../../../pages/AppLayout/Contexts.tsx';
import { getServerStatus as serverStatus } from '../../../service/server.ts';

export const ServerStatus: FunctionComponent = () => {
    const [getCurrentServer, setCurrentServer] = useState<{'name':string, 'url': string} | null>(null)
    const [getServerStatus, setServerStatus] = useState<boolean>(false)
    const serverData = useSelector(getServer);
    const currentUrl = useLocation().pathname 
    const serverName = getCurrentServer? getCurrentServer.name : ""

  const navigate = useNavigate();
  const dispatch = useDispatch();

  const { getOpenAPIClient } = useContextClient();
  const {t} = useTranslation();


  const retrieveServerStatus = async () => {
      if (getCurrentServer){
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        setServerStatus(await serverStatus()(getOpenAPIClient!));
      }
    }

    useEffect(() => {
      setCurrentServer(serverData);
      retrieveServerStatus()
      const getServerStatusInterval = setInterval(() => {
        retrieveServerStatus()
          .catch((error) => console.error(error));
      }, 10000);
  
      return (): void => {
        clearInterval(getServerStatusInterval);
      };
    // eslint-disable-next-line
    }, [getCurrentServer]);

    const handleServerChange = () =>{
      savePreviousAuthUrl(currentUrl)(dispatch)
      navigate(serverListFullPath());
    }

    return (
      <Card isPlain>
        <CardHeader style={{paddingLeft:0, paddingRight:0}}>
          <div style={{display:"flex", flexDirection:"row", gap:4}}>
            <p>{t('auth_page.server_status.status', {name: serverName})}</p> 
            <StatusIcon status={getServerStatus}/>
          </div>
          <CardActions 
            children={              
              <Button
                className={'serverButton'}
                variant={'primary'}
                isSmall
                onClick={handleServerChange}
                children={t('auth_page.server_status.back_button')}
              />
            }
          />  
        </CardHeader>
      </Card>
    );
}