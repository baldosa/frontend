import {
    FunctionComponent,
    useEffect,
    useState,
} from 'react';
import { Button, Modal, ModalVariant, FormGroup } from '@patternfly/react-core';
import { useTranslation } from 'react-i18next';
import { ExtendedTextInput } from '../../ExtendedTextInput.tsx';
import {
    isAnInvalidExtension,
    isAnInvalidFormat,
    isEmpty, SERVER, SERVER_ADDRESS, SERVER_NAME,
} from '../ValidationRules.ts';
import { useSelector } from 'react-redux';
import {
    allBlockingRequestsWereValidated
} from '../../../redux/reducers/generalActions.ts';
import { useServerContext } from '../../../pages/Auth/ServersPage.tsx';

export const ServerModal: FunctionComponent = () => {
    const { id, url, name, isOpen, toggleModal, setActualServerData, clearServerData, updateIsRequired } = useServerContext();

    const [isSubmitDisabled, setSubmitState] = useState<boolean>(true);

    const { t } = useTranslation();

    const everythingWasValidated: boolean = useSelector(allBlockingRequestsWereValidated);

    useEffect(() => {
        setSubmitState(!everythingWasValidated);
    }, [everythingWasValidated]);

    const setServerNameValue = (name: string) => {
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        setActualServerData!(id, name, url)
    }

    const setServerUrlValue = (url: string) => {
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        setActualServerData!(id, name, url)
    }

    const handleServerSubmit = () => {
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        updateIsRequired!();
    }

    const handleServerCancel = () => {
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        toggleModal!();
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        clearServerData!();
    }

    useEffect(() => {
        if (isOpen) {
            setServerNameValue(name);
            setServerUrlValue(url);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isOpen]);

    return (
        <Modal
            variant={ModalVariant.small}
            isOpen={isOpen}
            onClose={toggleModal}
            showClose={false}
            aria-label={'server-data-modal'}
        >
            <FormGroup>
                <ExtendedTextInput
                    id={'server_name_text_input'}
                    origin={SERVER}
                    fieldNames={[SERVER_NAME]}
                    type={'url'}
                    label={t('auth.server_list.element.input.label.server_name').toString()}
                    isRequired={true}
                    setValue={setServerNameValue}
                    getValue={name}
                    placeholder={''}
                    helperText={''}
                    rules={[isEmpty, isAnInvalidFormat, isAnInvalidExtension]}
                />
                <ExtendedTextInput
                    id={'server_address_text_input'}
                    origin={SERVER}
                    fieldNames={[SERVER_ADDRESS]}
                    type={'text'}
                    label={t('auth.server_list.element.input.label.server_address').toString()}
                    isRequired={true}
                    setValue={setServerUrlValue}
                    getValue={url}
                    placeholder={''}
                    helperText={''}
                    rules={[isEmpty, isAnInvalidFormat]}
                />
            </FormGroup>
            <FormGroup>
                <br />
                <div style={{ display: "flex", alignItems: "center", justifyContent: "space-evenly" }}>
                    <Button
                        key={'confirm_button'}
                        className={'serverButton'}
                        variant={'primary'}
                        isDisabled={isSubmitDisabled}
                        onClick={handleServerSubmit}
                        children={t('auth.server_list.element.action.confirm')}
                    />
                    <Button
                        key={'cancel_button'}
                        className={'serverButton'}
                        variant={'primary'}
                        onClick={handleServerCancel}
                        children={t('auth.server_list.element.action.cancel')}
                    />
                </div>
            </FormGroup>
        </Modal>
    )
}