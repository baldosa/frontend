import React, { Dispatch, FunctionComponent, useEffect, useState } from 'react';
import { GENERAL, LOGIN, SIGNUP } from './ValidationRules.ts';
import { Button, Text } from '@patternfly/react-core';
import { Trans, useTranslation } from 'react-i18next';
import {
  generalPath,
  loginFullPath,
  passwordResetFullPath,
  successFullPath,
  welcomeFullPath,
} from '../../routes.tsx';
import { ExtendedHelperText } from '../ExtendedHelperText.tsx';
import { useAuthContext } from '../../pages/Auth/AuthPage.tsx';
import {
  defineValidationRequestLocation,
  endAction,
  initiateAction,
  resolveNonBlockingValidationRequest,
} from '../../redux/actions/generalActions.ts';
import { login, signup } from '../../redux/actions/auth.ts';
import { Paths } from '../../api/utilities/Definitions';
import { useDispatch, useSelector } from 'react-redux';
import {
  allBlockingRequestsWereValidated,
  getActionState,
  getBlockingValidationRequest,
  HelperTextData,
} from '../../redux/reducers/generalActions.ts';
import {
  NavigateFunction,
  Params,
  useNavigate,
  useParams,
} from 'react-router-dom';
import { AnyAction } from '@reduxjs/toolkit';
import {
  getEmail,
  getPassword,
  isLoggedIn,
} from '../../redux/reducers/auth.ts';
import { isCompleted } from '../../redux/reducers/configuration/preConfiguration.ts';
import { comesFromAccountCreationToTac } from '../../utils/logics/authUtils.ts';
import { addAlert } from '../../redux/actions/alert.ts';
import { useContextClient } from '../../pages/AppLayout/Contexts.tsx';

export const BottomSection: FunctionComponent<{
  username: string | null;
  password: string | null;
}> = (props) => {
  const [isSubmitDisabled, setSubmitState] = useState<boolean>(true);

  const blockingValidationRequest: Record<string, HelperTextData> = useSelector(getBlockingValidationRequest);
  const everythingWasValidated: boolean = useSelector(allBlockingRequestsWereValidated);
  const getConfigurationStatus: boolean = useSelector(isCompleted);
  const isActionInProgress: boolean = useSelector(getActionState);
  const getLoggedStatus: boolean = useSelector(isLoggedIn);
  const savedPassword: string = useSelector(getPassword);
  const savedUsername: string = useSelector(getEmail);

  const { getOpenAPIClient } = useContextClient();
  const dispatch: Dispatch<AnyAction> = useDispatch();
  const navigate: NavigateFunction = useNavigate();
  const parameters: Readonly<Params> = useParams();

  const { flag, button_label } = useAuthContext();
  const { t } = useTranslation();

  useEffect(() => {
    setSubmitState(!everythingWasValidated);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [blockingValidationRequest]);

  const onActionButtonClick = async (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    event.preventDefault();

    initiateAction()(dispatch);

    resolveNonBlockingValidationRequest([GENERAL])(dispatch);

    if (flag == LOGIN) {
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      await login(props.username!, props.password!)
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      (dispatch, getOpenAPIClient!)
        .then(() => {
          if (getLoggedStatus && getConfigurationStatus) {
            navigate(generalPath);
          } else {
            navigate(welcomeFullPath());
          }
          addAlert('success', t('login.alert.success'))(dispatch);
        })
        .catch((error) => {
          defineValidationRequestLocation(error)(dispatch);
          addAlert('danger', t('login.alert.error'))(dispatch);
        });
    }

    const invitationToken = getParameters(parameters?.token);

    if(flag == SIGNUP && invitationToken) {
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      await signup(props.username!, props.password!, invitationToken)
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      (dispatch, getOpenAPIClient!)
        .then(() => {
          navigate(successFullPath());
          addAlert('success', t('signup.alert.success'))(dispatch);
        })
        .catch((error) => {
          defineValidationRequestLocation(error)(dispatch);
          addAlert('danger', t('signup.alert.error'))(dispatch);
        });
    }

    endAction()(dispatch);
  };

  const getParameters = (
    parameter: string | undefined,
  ): Paths.ConfirmInvitation.PathParameters | null => {
    if (parameter) {
      return { invitation_id: parameter };
    }
    return null;
  };

  return (
    <div className={'bottomSection'}>
      <Button
        variant={'link'}
        style={{
          display: flag == LOGIN ? 'inline' : 'none',
          paddingBottom: '10px',
        }}
        onClick={() => navigate(passwordResetFullPath())}
        children={t('login.form.bottom_section.forgot_password')}
      />
      <Button
        isDisabled={isSubmitDisabled}
        className={'authButton'}
        onClick={onActionButtonClick}
        isLoading={isActionInProgress}
        spinnerAriaValueText={t('general.awaiting_response').toString()}
        id={'sign_in_up_submit_button'}
        children={button_label}
      />
      <Text
        style={{
          display: flag == SIGNUP ? 'inline' : 'none',
          marginTop: '20px',
        }}
      >
        <Trans
          i18nKey={'signup.form.bottom_section.already_registered'}
          components={{
            b: <strong />,
            a: <a onClick={() => navigate(loginFullPath())} />,
          }}
        />
      </Text>
      <Trans
        i18nKey={'signup.form.bottom_section.use_and_privacy_agreement'}
        components={{
          a: (
            <a
              className={'bottomSectionElement'}
              style={{ display: flag == SIGNUP ? 'inline' : 'none' }}
              onClick={() =>
                comesFromAccountCreationToTac(
                  savedUsername,
                  savedPassword,
                  dispatch,
                  navigate,
                )
              }
            />
          ),
        }}
      />
      <ExtendedHelperText
        origin={GENERAL}
        fieldNames={[GENERAL]}
        validationRules={[]}
      />
    </div>
  );
};
