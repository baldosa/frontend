import { Dispatch, FunctionComponent, useEffect, useState } from 'react';
import { CardBody, Form } from '@patternfly/react-core';
import { ProfileProperties } from './ProfileProperties.ts';
import { ExtendedTextInput } from '../ExtendedTextInput.tsx';
import { PasswordInput } from '../PasswordInput.tsx';
import { useDispatch, useSelector } from 'react-redux';
import {
  getEmail,
  getPassword,
  selectAcceptTermsAndConditions,
} from '../../redux/reducers/auth.ts';
import {
  NavigateFunction,
  useNavigate,
  useSearchParams,
} from 'react-router-dom';
import {
  isAnInvalidFormat,
  isAnInvalidExtension,
  isEmpty,
  USERNAME,
  PASSWORD,
  EMAIL,
  SIGNUP,
  LOGIN
} from './ValidationRules.ts';
import { useAuthContext } from '../../pages/Auth/AuthPage.tsx';
import { BottomSection } from './BottomSection.tsx';
import { comesFromAccountCreationToTac } from '../../utils/logics/authUtils.ts';
import { AnyAction } from '@reduxjs/toolkit';
import { ServerStatus } from './servers/ServerStatus.tsx';

export const AuthContent: FunctionComponent = () => {
  const [username, setUsername] = useState<string | null>(null);
  const [password, setPassword] = useState<string | null>(null);
  const [getProfile, setProfile] = useState<ProfileProperties | undefined>(
    undefined,
  );

  const isTermsAndConditionsAccepted: boolean = useSelector(
    selectAcceptTermsAndConditions,
  );
  const savedUsername: string = useSelector(getEmail);
  const savedPassword: string = useSelector(getPassword);

  const dispatch: Dispatch<AnyAction> = useDispatch();
  const navigate: NavigateFunction = useNavigate();
  const [searchParams] = useSearchParams();
  const { flag, profile_properties } = useAuthContext();

  useEffect(() => {
    const username: string | null = searchParams.get('username');
    if (!savedUsername && username) setUsername(username);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (username && flag == SIGNUP && !isTermsAndConditionsAccepted) {
      comesFromAccountCreationToTac(username, password, dispatch, navigate);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [username]);

  useEffect(() => {
    if (profile_properties) {
      setProfile(profile_properties);
    }
  }, [profile_properties]);

  useEffect(() => {
    if (savedUsername) {
      setUsername(savedUsername);
    }
    if (savedPassword) {
      setPassword(savedPassword);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <CardBody>
      {flag == LOGIN?
      <ServerStatus />
      : <></>}
      <Form id={'auth_form'}>
        <ExtendedTextInput
          id={'username_text_input'}
          origin={flag}
          fieldNames={[USERNAME, EMAIL]}
          isRequired={true}
          type={'text'}
          label={getProfile?.usernameLabel}
          getValue={username}
          setValue={setUsername}
          placeholder={getProfile?.usernamePlaceholder}
          helperText={getProfile?.usernameHelperText}
          rules={[isEmpty]}
        />
        <PasswordInput
          id={'password_text_input'}
          className="password-input"
          origin={flag}
          fieldNames={[PASSWORD]}
          label={getProfile?.passwordLabel}
          type={'password'}
          isRequired={true}
          getValue={password}
          setValue={setPassword}
          placeholder={getProfile?.passwordPlaceholder}
          helperText={getProfile?.passwordHelperText}
          rules={[isEmpty, isAnInvalidFormat, isAnInvalidExtension]}
        />
      </Form>
      <BottomSection username={username} password={password} />
    </CardBody>
  );
};
