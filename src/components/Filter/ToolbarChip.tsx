import {
  ToolbarChip,
  ToolbarChipGroup,
  ToolbarFilter,
} from '@patternfly/react-core';
import { Fragment } from 'react';
import * as React from 'react';

export const ChipSection: React.FunctionComponent<{
  categoryName: string;
  chips: string[];
  onDelete(key: string, element: string): void;
  onDeleteGroup(key: string): void;
  element?: React.ReactNode;
}> = (props: {
  categoryName: string;
  chips: string[];
  onDelete(key: string, element: string): void;
  onDeleteGroup(key: string): void;
  element?: React.ReactNode;
}) => {
  return (
    <ToolbarFilter
      chips={props.chips}
      categoryName={props.categoryName}
      deleteChip={(
        category: string | ToolbarChipGroup,
        chip: string | ToolbarChip,
      ) => props.onDelete(category as string, chip as string)}
      deleteChipGroup={(category: string | ToolbarChipGroup) =>
        props.onDeleteGroup(category as string)
      }
      children={props.element ? props.element : <Fragment />}
    />
  );
};
