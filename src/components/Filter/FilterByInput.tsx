import {
  FunctionComponent,
  ReactElement,
  SyntheticEvent,
  useEffect,
  useState,
} from 'react';
import {
  Button,
  SearchInput,
  Select,
  SelectOption,
  SelectOptionObject,
  Toolbar,
  ToolbarChip,
  ToolbarChipGroup,
  ToolbarContent,
  ToolbarGroup,
  ToolbarItem,
} from '@patternfly/react-core';
import { useTranslation } from 'react-i18next';
import { ChipSection } from './ToolbarChip.tsx';
import {
  hideComponent,
  isDisplayed,
  screenWidthIsLessThan,
} from '../../utils/utils.ts';
import FilterIcon from '@patternfly/react-icons/dist/esm/icons/filter-icon';
import { useFilesContext } from '../../pages/Team/Parts/File/types.ts';

type FiltersState = { [key: string]: string[] };
type Filter = { name: string; value: string };

export const FilterByInput: FunctionComponent<{
  id: string;
  options: ReactElement[];
  filters: Filter[];
  changeFileOrder: (fileOrder: boolean) => void;
}> = (props) => {
  const [getFilters, setFilters] = useState<FiltersState>({ name: [], extension: [] });
  const [filesInDescendingOrder, setFilesInDescendingOrder] = useState<boolean>(false);
  const [getSelectedFilterKey, setSelectedFilterKey] = useState<string>('name');
  const [getFilteredValues, setFilteredValues] = useState<ReactElement[]>([]);
  const [isFilterDisplayed, setIsFilterDisplayed] = useState<boolean>(false);
  const [filtersIsExpanded, setFiltersIsExpanded] = useState<boolean>(false);
  const [getSearchValue, setSearchValue] = useState<string>('');

  const { containsRealElements } = useFilesContext();
  const { t } = useTranslation();

  useEffect((): void => {
    if (containsRealElements && filtersAreApplied()) {
      let filteredValues: ReactElement[] = props.options.slice();

      Object.entries(getFilters).forEach((filter: [string, string[]]): void => {
        filteredValues = filteredValues.filter((value: ReactElement) => {
          return filter[1].every((filterValue: string) => {
            return value.props[filter[0]].includes(filterValue);
          });
        });
      });

      setFilteredValues(filteredValues);
    } else {
      setFilteredValues(props.options);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [getFilters, props.options]);

  useEffect((): void => {
    if (numberOfAppliedFilters() == 1 && screenWidthIsLessThan(992)) {
      hideComponent('team-files-filter', 'pf-c-toolbar__group');
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [getFilteredValues, window.innerWidth]);

  const onSelect = (key: string, selection: string): void => {
    if (!getFilters[key].includes(selection)) {
      setFilters((prevFilters: FiltersState) => {
        const updatedFilters: { [p: string]: string[] } = { ...prevFilters };
        const currentFilter: string[] = Array.isArray(updatedFilters[key])
          ? updatedFilters[key]
          : [];

        updatedFilters[key] = [...currentFilter, selection];

        return updatedFilters;
      });
    }
  };

  const onDelete = (key: string, element: string): void => {
    const filters: FiltersState = { ...getFilters };
    if (key && filters[key]) {
      filters[key] = filters[key].filter(
        (item: string): boolean => item !== element,
      );
      setFilters(filters);
    }
  };

  const onDeleteGroup = (key: string): void => {
    const filters: FiltersState = { ...getFilters };
    if (key && filters[key]) {
      filters[key] = [];
      setFilters(filters);
    }
  };

  const onDeleteAll = (): void => {
    setFilters({ name: [], extension: [] });
    setSearchValue('');
  };

  const numberOfAppliedFilters = () => {
    return Object.values(getFilters).reduce(
      (accumulator: number, currentValue: string[]): number =>
        currentValue.length > 0 ? accumulator + 1 : accumulator,
      0,
    );
  };

  const filtersAreApplied = (): boolean => {
    return numberOfAppliedFilters() > 0;
  };

  const chipGroupSection = (): ReactElement => {
    return (
      <ToolbarItem>
        <Button
          style={{ paddingTop: '10px' }}
          isInline
          variant="link"
          onClick={onDeleteAll}
          aria-label={t(
            'structure_menu.teams.files.filters.clear-all.aria-label',
          ).toString()}
          children={t(
            'structure_menu.teams.files.filters.clear-all.description',
          )}
        />
      </ToolbarItem>
    );
  };

  const toggleFilter = () => setIsFilterDisplayed(!isFilterDisplayed);

  const changeOrderOfFiles = () => {
    setFilesInDescendingOrder(!filesInDescendingOrder);
    props.changeFileOrder(!filesInDescendingOrder);
  };

  return (
    <div className={'structure-menu'}>
      <div className={'filter-container'}>
        <div className={'filter-buttons'}>
          <Button
            variant="control"
            children={<FilterIcon />}
            style={{ margin: '10px' }}
            onClick={toggleFilter}
            isDisabled={!containsRealElements}
          />
          <Button
            variant="plain"
            children={t('structure_menu.teams.files.most_recent')}
            onClick={changeOrderOfFiles}
            isDisabled={!containsRealElements}
          />
        </div>
        <Toolbar
          id={props.id}
          className={'filter-section'}
          clearAllFilters={onDeleteAll}
          customChipGroupContent={chipGroupSection()}
          style={{ display: isDisplayed(isFilterDisplayed) }}
        >
          <ToolbarContent>
            <ToolbarGroup variant="filter-group">
              <ToolbarItem
                style={{
                  minWidth: '35%',
                }}
              >
                <Select
                  className={'filter-selector'}
                  variant={'single'}
                  isOpen={filtersIsExpanded}
                  selections={getSelectedFilterKey}
                  id="filter-selector-available"
                  aria-label={t(
                    'structure_menu.teams.files.filters.selector.aria-label',
                  ).toString()}
                  onToggle={(isExpanded: boolean) =>
                    setFiltersIsExpanded(isExpanded)
                  }
                  onSelect={(
                    _event: SyntheticEvent,
                    value: string | SelectOptionObject,
                  ) => setSelectedFilterKey(value.toString())}
                  children={props.filters.map(
                    (filter: Filter, index: number) => (
                      <SelectOption
                        key={`name-${index}`}
                        value={filter.value}
                        children={filter.name}
                        aria-label={t(
                          'structure_menu.teams.files.filters.selector.aria-label',
                          { field: filter.value },
                        ).toString()}
                      />
                    ),
                  )}
                />
              </ToolbarItem>
              <ChipSection
                categoryName={'name'}
                chips={getFilters.name}
                onDelete={(
                  category: string | ToolbarChipGroup,
                  chip: string | ToolbarChip,
                ) => onDelete(category as string, chip as string)}
                onDeleteGroup={(category: string | ToolbarChipGroup) =>
                  onDeleteGroup(category as string)
                }
              />
              <ChipSection
                categoryName={'extension'}
                chips={getFilters.extension}
                onDelete={(
                  category: string | ToolbarChipGroup,
                  chip: string | ToolbarChip,
                ) => onDelete(category as string, chip as string)}
                onDeleteGroup={(category: string | ToolbarChipGroup) =>
                  onDeleteGroup(category as string)
                }
              />
              <SearchInput
                style={{
                  minWidth: '65%',
                }}
                isDisabled={getSelectedFilterKey.length === 0}
                value={getSearchValue}
                onChange={(_event: SyntheticEvent, value: string) =>
                  setSearchValue(value)
                }
                onSearch={() => onSelect(getSelectedFilterKey, getSearchValue)}
                aria-label={t(
                  'structure_menu.teams.files.filters.search-input.aria-label',
                ).toString()}
              />
            </ToolbarGroup>
          </ToolbarContent>
        </Toolbar>
        <ol className={'filter-results'} children={getFilteredValues} />
      </div>
    </div>
  );
};
