import { Button, Card, CardBody, CardTitle } from '@patternfly/react-core';
import TimesIcon from '@patternfly/react-icons/dist/esm/icons/times-icon';
import * as React from 'react';
import { CSSProperties } from 'react';

export const FloatingCard: React.FunctionComponent<{
  title: string;
  alt: string;
  message: string;
  closeable: boolean;
  style?: CSSProperties | undefined;
}> = (props: {
  title: string;
  alt: string;
  message: string;
  closeable: boolean;
  style?: CSSProperties | undefined;
}) => {
  const [isCardDisplayed, setIsCardDisplayed] = React.useState(true);

  const closeWelcomeCard = () => {
    setIsCardDisplayed(!isCardDisplayed);
  };

  return (
    <Card
      isRounded
      className={'welcome-modal'}
      style={{ display: isCardDisplayed ? 'inherit' : 'none', ...props.style }}
    >
      <CardTitle
        component={'h1'}
        children={
          <div className={'display-flex-row-between no-margin'}>
            {props.title}
            {props.closeable && (
              <Button
                variant={'plain'}
                icon={<TimesIcon />}
                style={{ marginLeft: 'auto' }}
                onClick={closeWelcomeCard}
                aria-label={props.alt}
              />
            )}
          </div>
        }
        style={{ margin: 'auto' }}
      />
      <CardBody children={props.message} />
    </Card>
  );
};
