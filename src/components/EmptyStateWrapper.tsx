import React from 'react';
import {
  Title,
  EmptyState,
  EmptyStateIcon,
  EmptyStateBody,
} from '@patternfly/react-core';

type Props = {
  title: string;
  description: string;
  icon?: React.ComponentType;
  action?: React.ReactNode;
};

export default function EmptyStateWrapper({
  title,
  description,
  icon,
  action = null,
}: Props) {
  return (
    <EmptyState variant="full">
      <EmptyStateIcon icon={icon} />
      <Title headingLevel="h1" size="lg">
        {title}
      </Title>
      <EmptyStateBody>{description}</EmptyStateBody>
      {action}
    </EmptyState>
  );
}
