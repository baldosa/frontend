/* eslint-disable @typescript-eslint/no-explicit-any */
import { useState, useEffect, useRef } from 'react';
import { useTranslation } from 'react-i18next';
import SmallModal from '../../SmallModal';
import { Button, Alert } from '@patternfly/react-core';
import { DownloadIcon } from '../../../assets/icon/studio/DownloadIcon';
import { UploadIcon } from '../../../assets/icon/studio/UploadIcon';
import {
  isAudioFile,
  isVideoFile,
  isAudioZipFile,
  isValidAudioZipFile,
  getAmountBlobsFromZip,
} from '../../../utils/utils';
import { arrayBufferToBlob } from 'blob-util';
import { AudioTypeImportEnum, PlaylistStatusEnum } from '../../../enums';
import { useDispatch, useSelector } from 'react-redux';
import ee from 'event-emitter';
import {
  setEditorOptionsVisibility,
  setRecordingCounter,
  updateRecordingState,
} from '../../../redux/actions/recordings';
import { getRecordingCounter } from '../../../redux/reducers/recordings';
import { useWaveformEmitter } from '../../../hooks/useWaveformEmitter';

import ImportProjectFromServer from './ImportAudioOptions/ImportProjectFromServer';
import { AxiosError } from 'axios';

type Props = {
  isOpen: boolean;
  handleClose: () => void;
  emitter: ee.Emitter;
};

export default function ImportAudioModal({
  isOpen,
  handleClose,
  emitter,
}: Props) {
  const { t } = useTranslation();
  const inputFileRef = useRef(null);
  const [errorMessage, setErrorMessage] = useState('');
  const [isModalProjectImportOpen, setIsModalProjectImportOpen] =
    useState(false);

  const [isSelectProjectModalOpen, setIsSelectProjectModalOpen] = useState(false)

  const dispatch = useDispatch();
  const recordingCounter: number = useSelector(getRecordingCounter);
  const [blob, setBlob] = useState<Blob>();

  const { emitterImportAudio, emitterImportProject } = useWaveformEmitter({
    emitter,
  });

  const handleImportAudioFromDevice = () => {
    if (!inputFileRef || !inputFileRef.current) return;
    const clk: { click: () => void } = inputFileRef?.current;
    clk.click();
  };

  const handleImportAudio = (type: string, data: Blob) => {
    if (type === AudioTypeImportEnum.SINGLE_AUDIO) emitterImportAudio(data);
    else if (type === AudioTypeImportEnum.AUDIO_PROJECT)
      emitterImportProject(data);

    dispatch(setEditorOptionsVisibility(true));
    handleClose();
  };

  const onSelectFile = async (e: any) => {
    if (e.target.files && e.target.files.length > 0) {
      let blob_: Blob;
      const file = e.target.files[0];
      const { type, name } = file;

      try {
        if (isAudioFile(type) || isVideoFile(type)) {
          blob_ = arrayBufferToBlob(file, 'audio/*');
          setBlob(blob_);
          handleImportAudio(AudioTypeImportEnum.SINGLE_AUDIO, blob_);
          dispatch(updateRecordingState(PlaylistStatusEnum.STOPPED));
          dispatch(setRecordingCounter(recordingCounter + 1));
        } else if (isAudioZipFile(name)) {
          blob_ = arrayBufferToBlob(file);
          setBlob(blob_);
          const isValidAZF = await isValidAudioZipFile(blob_);
          if (!isValidAZF)
            throw new Error(
              String(
                t(
                  'tools.modal_import_audio.validation_messages.invalid_zip_file',
                ),
              ),
            );

          if (recordingCounter === 0) await handleImportProject(blob_);
          else setIsModalProjectImportOpen(true);
        } else
          throw new Error(
            String(
              t('tools.modal_import_audio.validation_messages.invalid_file'),
            ),
          );
      } catch (e: any) {
        setErrorMessage(String(e.message));
      }
    }
  };

  const handleImportProject = async (blob: Blob | undefined) => {
    if (!blob) return;

    handleImportAudio(AudioTypeImportEnum.AUDIO_PROJECT, blob);
    const rc = await getAmountBlobsFromZip(blob);

    /* Updating amount of recordings */
    dispatch(setRecordingCounter(rc + recordingCounter));
    dispatch(updateRecordingState(PlaylistStatusEnum.IMPORTED_AUDIO));
    handleCloseModalProjectImportOpen();
  };

  const handleImportProjectError = async (error: AxiosError<{error_code: string}, any>) => {
    switch (error.code) {
      case "ERR_BAD_REQUEST":
        setErrorMessage(String(t('errors.project_not_found')))
        break

      case "ERR_NETWORK":
        setErrorMessage(String(t('service_unavailable')))
        break

      default:
        break
    }
    handleSelectProjectClose();
  }

  const handleCloseModalProjectImportOpen = () => {
    setIsModalProjectImportOpen(false);
  };

  useEffect(() => {
    setErrorMessage('');
  }, [isOpen]);

  const handleSelectProjectOpen = () => setIsSelectProjectModalOpen(true)
  const handleSelectProjectClose = () => setIsSelectProjectModalOpen(false)

  const content = (
    <div className="buttons-import-audio-from">
      <input
        type="file"
        ref={inputFileRef}
        onChange={onSelectFile}
        style={{ display: 'none' }}
      />
      {errorMessage && <Alert variant="danger" title={errorMessage} />}
      <SmallModal
        className="rouded-modal-md bg-opacity-md"
        description={String(t('tools.modal_import_project.title'))}
        isModalOpen={isModalProjectImportOpen}
        handleClose={handleCloseModalProjectImportOpen}
        actions={[
          <Button
            key="confirm"
            variant="primary"
            onClick={() => handleImportProject(blob)}
          >
            {t('tools.modal_import_project.confirm_button')}
          </Button>,
          <Button
            key="cancel"
            variant="link"
            onClick={handleCloseModalProjectImportOpen}
          >
            {t('tools.modal_import_project.cancel_button')}
          </Button>,
        ]}
      />
      <ImportProjectFromServer
        isOpen={isSelectProjectModalOpen}
        handleClose={handleSelectProjectClose}
        onSelectProject={handleImportProject}
        onImportError={handleImportProjectError}
      />
      <Button
        key="device-button"
        variant="link"
        onClick={handleImportAudioFromDevice}
        isBlock
        icon={<DownloadIcon fill="#0C0A21" />}
      >
        {t('tools.modal_import_audio.device_button')}
      </Button>
      <Button
        className="text-red-500"
        key="server-button"
        variant="link"
        isBlock
        onClick={handleSelectProjectOpen}
        icon={<UploadIcon fill="#0C0A21" />}
      >
        {t('tools.modal_import_audio.server_button')}
      </Button>
    </div>
  );

  return (
    <SmallModal
      className="rouded-modal-md bg-opacity-md"
      title={String(t('tools.modal_import_audio.title'))}
      isModalOpen={isOpen}
      handleClose={handleClose}
      content={content}
      width={300}
    />
  );
}
