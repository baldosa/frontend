import { useState } from 'react';
import { Button } from '@patternfly/react-core';
import { useSelector } from 'react-redux';
import { useWaveformEmitter } from '../../../hooks/useWaveformEmitter';
import { PlaylistStatusEnum } from '../../../enums';
import SmallModal from '../../../components/SmallModal';
import { useTranslation } from 'react-i18next';
import { getActiveRecordingState } from '../../../redux/reducers/recordings';
import { UploadIcon } from '../../../assets/icon/studio/UploadIcon';
import { DownloadIcon } from '../../../assets/icon/studio/DownloadIcon';
import { TrashIcon } from '../../../assets/icon/studio/TrashIcon';
import { ContextPropsType } from '../../../pages/AppLayout/types.ts';

export default function RecorderActions({ emitter }: ContextPropsType) {
  const { t } = useTranslation();
  const [isModalOpen, setModalOpen] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const audioState: PlaylistStatusEnum = useSelector(getActiveRecordingState);
  const { emitterClear } = useWaveformEmitter({
    emitter,
  });

  const handleCloseModal = () => {
    setModalOpen(false);
  };

  const handleDiscardRecording = () => {
    setIsLoading(true);
    emitterClear();

    setTimeout(() => {
      setModalOpen(false);
      setIsLoading(false);
    }, 1000);
  };

  if (
    [
      PlaylistStatusEnum.STOPPED,
      PlaylistStatusEnum.PLAYING,
      PlaylistStatusEnum.PAUSED,
    ].includes(audioState)
  ) {
    return (
      <>
        <SmallModal
          description={String(t('tools.modal_remove_audio.description'))}
          isModalOpen={isModalOpen}
          handleClose={handleCloseModal}
          actions={[
            <Button
              key="confirm"
              variant="primary"
              isLoading={isLoading}
              onClick={handleDiscardRecording}
            >
              {t('tools.modal_remove_audio.confirm_button')}
            </Button>,
            <Button key="cancel" variant="link" onClick={handleCloseModal}>
              {t('tools.modal_remove_audio.discard_button')}
            </Button>,
          ]}
        />
        <div id="recorder-action-menu" className="recorder-actions-menu">
          <Button variant="plain" isDisabled aria-label="action-download">
            <DownloadIcon fill="white" width="20" height="20" />
          </Button>
          <Button variant="plain" isDisabled aria-label="action-upload">
            <UploadIcon fill="white" width="20" height="20" />
          </Button>
          <Button
            variant="plain"
            aria-label="action-discard"
            onClick={() => setModalOpen(true)}
          >
            <TrashIcon fill="white" width="20" height="20" />
          </Button>
        </div>
      </>
    );
  }

  return <div></div>;
}
