import React, { useState, useEffect } from 'react';
import { Nav, NavItem, NavList } from '@patternfly/react-core';
import { useTranslation } from 'react-i18next';
import { PlaybackOperationsNavMenu } from './PlaybackOperationsNavMenu';
import { BasicOperationsNavMenu } from './BasicOperationsNavMenu';
import { SelectionMoveNavMenu } from './SelectionMoveNavMenu';
import { EffectsNavMenu } from './EffectsNavMenu';
import { useSelector } from 'react-redux';
import { PlaylistStatusEnum } from '../../../../../../enums';
import {
  getEditorOptionsVisibility,
  getActiveRecordingState,
} from '../../../../../../redux/reducers/recordings';
import { isBrowser } from 'react-device-detect';
import { useVerifyScreenOrientation } from '../../../../../../hooks/useVerifyScreenOrientation';

export const HorizontalNavEditor: React.FunctionComponent = () => {
  const editorOptionsVisibility: boolean = useSelector(
    getEditorOptionsVisibility,
  );
  const audioState: PlaylistStatusEnum = useSelector(getActiveRecordingState);
  const [isDisabled, setIsDisabled] = useState(false);
  const { isPortraitView } = useVerifyScreenOrientation();
  const [activeItem, setActiveItem] = React.useState('playback-operations');
  const { t } = useTranslation();

  const [currentSelectionOps, setCurrentSelectionOps] = useState('');
  const [currentBasicOps, setCurrentBasicOps] = useState('');

  useEffect(() => {
    const res = [
      PlaylistStatusEnum.RECORDING,
      PlaylistStatusEnum.RECORDING_PAUSED,
    ].includes(audioState);
    setIsDisabled(res);
  }, [audioState]);

  const onSelect = (result: { itemId: number | string }) => {
    if (isDisabled) return;

    if (result.itemId !== activeItem) setActiveItem(result.itemId as string);
  };

  const operations = [
    {
      id: 'playback-operations',
      name: t('tools.items.editor.actions.option4.title'),
    },
    {
      id: 'selection-move',
      name: t('tools.items.editor.actions.option2.title'),
    },
    {
      id: 'basic-operations',
      name: t('tools.items.editor.actions.option1.title'),
    },
    {
      id: 'effects',
      name: t('tools.items.editor.actions.option3.title'),
    },
  ];

  const desktopCSSVersion =
    isBrowser || !isPortraitView
      ? { width: 'fit-content', margin: '0 auto' }
      : {};

  if (editorOptionsVisibility) {
    return (
      <>
        <Nav
          onSelect={onSelect}
          variant="horizontal-subnav"
          className="horizontal-subnav-audio-editor"
          aria-label="Horizontal subnav audio editor"
          style={desktopCSSVersion}
        >
          <NavList>
            {operations.map(({ id, name }) => (
              <NavItem
                preventDefault
                key={id}
                itemId={id}
                isActive={activeItem === id}
                id={`horizontal-subnav-${id}`}
                to={`#horizontal-subnav-${id}`}
                className={`${
                  isDisabled && id !== 'playback-operations'
                    ? 'horizontal-navmenu-inactive'
                    : ''
                }`}
              >
                {name}
              </NavItem>
            ))}
          </NavList>
        </Nav>
        {activeItem === 'playback-operations' && <PlaybackOperationsNavMenu />}
        {activeItem === 'basic-operations' && (
          <BasicOperationsNavMenu
            activeItem={currentBasicOps}
            updateActiveItem={(value: string) => setCurrentBasicOps(value)}
          />
        )}
        {activeItem === 'selection-move' && (
          <SelectionMoveNavMenu
            activeItem={currentSelectionOps}
            updateActiveItem={(value: string) => setCurrentSelectionOps(value)}
          />
        )}
        {activeItem === 'effects' && (
          <EffectsNavMenu
            activeItem={currentSelectionOps}
            updateActiveItem={(value: string) => setCurrentSelectionOps(value)}
          />
        )}
      </>
    );
  }

  return <div></div>;
};
