import React from 'react';
import { Nav, NavItem, NavList } from '@patternfly/react-core';
import { useTranslation } from 'react-i18next';
import { CutIcon } from '../../../../../../assets/icon/studio/CutIcon';
import { TrimIcon } from '../../../../../../assets/icon/studio/TrimIcon';
import { SplitIcon } from '../../../../../../assets/icon/studio/SplitIcon';
import { UndoIcon } from '../../../../../../assets/icon/studio/UndoIcon';
import { RedoIcon } from '../../../../../../assets/icon/studio/RedoIcon';
import { useOutletContext } from 'react-router-dom';
import { useWaveformEmitter } from '../../../../../../hooks/useWaveformEmitter';
import { HandleInteractionWaveformEnum } from '../../../../../../enums';
import { isBrowser } from 'react-device-detect';
import { useVerifyScreenOrientation } from '../../../../../../hooks/useVerifyScreenOrientation';
import { ContextPropsType } from '../../../../../../pages/AppLayout/types.ts';

type Props = {
  activeItem: string;
  updateActiveItem: (value: string) => void;
};

export const BasicOperationsNavMenu: React.FunctionComponent<Props> = ({
  activeItem,
  updateActiveItem,
}) => {
  const { t } = useTranslation();
  const { isPortraitView } = useVerifyScreenOrientation();
  const { emitter } = useOutletContext<ContextPropsType>();

  const { emitterHandleTrack, emitterUndo, emitterRedo } = useWaveformEmitter({
    emitter,
  });

  const onSelect = (result: { itemId: number | string }) => {
    if (result.itemId !== activeItem) updateActiveItem(result.itemId as string);
  };

  const options = [
    {
      key: 'cut',
      title: t('tools.items.editor.actions.option1.cut'),
      icon: <CutIcon />,
      onClick: () => emitterHandleTrack(HandleInteractionWaveformEnum.CUT),
    },
    {
      key: 'trim',
      title: t('tools.items.editor.actions.option1.trim'),
      icon: <TrimIcon />,
      onClick: () => emitterHandleTrack(HandleInteractionWaveformEnum.TRIM),
    },
    {
      key: 'split',
      title: t('tools.items.editor.actions.option1.split'),
      icon: <SplitIcon />,
      onClick: () => emitterHandleTrack(HandleInteractionWaveformEnum.SPLIT),
    },
    {
      key: 'undo',
      title: t('tools.items.editor.actions.option1.undo'),
      icon: <UndoIcon />,
      onClick: emitterUndo,
    },
    {
      key: 'redo',
      title: t('tools.items.editor.actions.option1.redo'),
      icon: <RedoIcon />,
      onClick: emitterRedo,
    },
  ];

  const desktopCSSVersion =
    isBrowser || !isPortraitView
      ? { width: 'fit-content', margin: '0 auto' }
      : {};

  return (
    <Nav
      onSelect={onSelect}
      variant="horizontal-subnav"
      className="horizontal-subnav-audio-editor"
      aria-label="Horizontal subnav audio editor basic ops"
      style={desktopCSSVersion}
    >
      <NavList>
        {options.map(({ key, icon, title, onClick }) => (
          <NavItem
            preventDefault
            key={key}
            itemId={key}
            isActive={activeItem === key}
            id={`horizontal-basic-ops-subnav-${key}`}
            to={`#horizontal-basic-ops-subnav-${key}`}
            onClick={onClick}
          >
            {icon}
            {title}
          </NavItem>
        ))}
      </NavList>
    </Nav>
  );
};
