import React from 'react';
import { Nav, NavItem, NavList } from '@patternfly/react-core';
import { useTranslation } from 'react-i18next';
import { FadeInIcon } from '../../../../../../assets/icon/studio/FadeInIcon';
import { FadeOutIcon } from '../../../../../../assets/icon/studio/FadeOutIcon';
import { useOutletContext } from 'react-router-dom';
import { useWaveformEmitter } from '../../../../../../hooks/useWaveformEmitter';
import { StatesInteractionWaveformEnum } from '../../../../../../enums';
import { isBrowser } from 'react-device-detect';
import { useVerifyScreenOrientation } from '../../../../../../hooks/useVerifyScreenOrientation';
import { ContextPropsType } from '../../../../../../pages/AppLayout/types.ts';

type Props = {
  activeItem: string;
  updateActiveItem: (value: string) => void;
};

export const EffectsNavMenu: React.FunctionComponent<Props> = ({
  activeItem,
  updateActiveItem,
}) => {
  const { t } = useTranslation();
  const { isPortraitView } = useVerifyScreenOrientation();
  const onSelect = (result: { itemId: number | string }) => {
    if (result.itemId !== activeItem) updateActiveItem(result.itemId as string);
  };

  const { emitter } = useOutletContext<ContextPropsType>();

  const { emitterStateChange } = useWaveformEmitter({
    emitter,
  });

  const options = [
    {
      key: 'fadeIn',
      title: t('tools.items.editor.actions.option3.fadeIn'),
      icon: <FadeInIcon />,
      onClick: () => emitterStateChange(StatesInteractionWaveformEnum.FADEIN),
    },
    {
      key: 'fadeOut',
      title: t('tools.items.editor.actions.option3.fadeOut'),
      icon: <FadeOutIcon />,
      onClick: () => emitterStateChange(StatesInteractionWaveformEnum.FADEOUT),
    },
  ];

  const desktopCSSVersion =
    isBrowser || !isPortraitView
      ? { width: 'fit-content', margin: '0 auto' }
      : {};

  return (
    <Nav
      onSelect={onSelect}
      variant="horizontal-subnav"
      className="horizontal-subnav-audio-editor"
      aria-label="Horizontal subnav audio editor effects"
      style={desktopCSSVersion}
    >
      <NavList>
        {options.map(({ key, icon, title, onClick }) => (
          <NavItem
            preventDefault
            key={key}
            itemId={key}
            isActive={activeItem === key}
            id={`horizontal-effects-subnav-${key}`}
            to={`#horizontal-effects-subnav-${key}`}
            onClick={activeItem === key ? undefined : onClick}
          >
            {icon}
            {title}
          </NavItem>
        ))}
      </NavList>
    </Nav>
  );
};
