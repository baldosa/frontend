import React from 'react';
import { Nav, NavItem, NavList } from '@patternfly/react-core';
import { useTranslation } from 'react-i18next';
import { CursorIcon } from '../../../../../../assets/icon/studio/CursorIcon';
import { ShiftIcon } from '../../../../../../assets/icon/studio/ShiftIcon';
import { ZoomInIcon } from '../../../../../../assets/icon/studio/ZoomInIcon';
import { ZoomOutIcon } from '../../../../../../assets/icon/studio/ZoomOutIcon';
import { SelectIcon } from '../../../../../../assets/icon/studio/SelectIcon';
import { useOutletContext } from 'react-router-dom';
import { useWaveformEmitter } from '../../../../../../hooks/useWaveformEmitter';
import { StatesInteractionWaveformEnum } from '../../../../../../enums';
import { isBrowser } from 'react-device-detect';
import { useVerifyScreenOrientation } from '../../../../../../hooks/useVerifyScreenOrientation';
import { ContextPropsType } from '../../../../../../pages/AppLayout/types.ts';

type Props = {
  activeItem: string;
  updateActiveItem: (value: string) => void;
};

export const SelectionMoveNavMenu: React.FunctionComponent<Props> = ({
  activeItem,
  updateActiveItem,
}) => {
  const { t } = useTranslation();
  const { emitter } = useOutletContext<ContextPropsType>();
  const { isPortraitView } = useVerifyScreenOrientation();
  const { emitterStateChange, emitterZoomIn, emitterZoomOut } =
    useWaveformEmitter({
      emitter,
    });

  const onSelect = (result: { itemId: number | string }) => {
    if (result.itemId !== activeItem) {
      updateActiveItem(result.itemId as string);
    }
  };

  const options = [
    {
      key: 'cursor',
      title: t('tools.items.editor.actions.option2.cursor'),
      icon: <CursorIcon />,
      onClick: () => emitterStateChange(StatesInteractionWaveformEnum.CURSOR),
    },
    {
      key: 'audioRegion',
      title: t('tools.items.editor.actions.option2.audioRegion'),
      icon: <SelectIcon />,
      onClick: () => emitterStateChange(StatesInteractionWaveformEnum.SELECT),
    },
    {
      key: 'shift',
      title: t('tools.items.editor.actions.option2.shift'),
      icon: <ShiftIcon />,
      onClick: () => emitterStateChange(StatesInteractionWaveformEnum.SHIFT),
    },
    {
      key: 'zoomin',
      title: t('tools.items.editor.actions.option2.zoomin'),
      icon: <ZoomInIcon />,
      onClick: emitterZoomIn,
    },
    {
      key: 'zoomout',
      title: t('tools.items.editor.actions.option2.zoomout'),
      icon: <ZoomOutIcon />,
      onClick: emitterZoomOut,
    },
  ];

  const desktopCSSVersion =
    isBrowser || !isPortraitView
      ? { width: 'fit-content', margin: '0 auto' }
      : {};

  return (
    <Nav
      onSelect={onSelect}
      variant="horizontal-subnav"
      className="horizontal-subnav-audio-editor"
      aria-label="Horizontal subnav audio editor select move"
      style={desktopCSSVersion}
    >
      <NavList>
        {options.map(({ key, icon, title, onClick }) => (
          <NavItem
            preventDefault
            key={key}
            itemId={key}
            isActive={activeItem === key}
            id={`horizontal-selection-subnav-${key}`}
            to={`#horizontal-selection-subnav-${key}`}
            onClick={
              activeItem === key && key !== 'zoomin' && key !== 'zoomout'
                ? undefined
                : onClick
            }
          >
            {icon}
            {title}
          </NavItem>
        ))}
      </NavList>
    </Nav>
  );
};
