import React from 'react';
import { Nav, NavItem, NavList } from '@patternfly/react-core';
import { useTranslation } from 'react-i18next';
import { RewindIcon } from '../../../../../../assets/icon/studio/RewindIcon';
import { ForwardIcon } from '../../../../../../assets/icon/studio/ForwardIcon';
import FaPlayIcon from '@patternfly/react-icons/dist/esm/icons/play-icon';
import FaPauseIcon from '@patternfly/react-icons/dist/esm/icons/pause-icon';
import FaStopIcon from '@patternfly/react-icons/dist/esm/icons/stop-icon';
import { useOutletContext } from 'react-router-dom';
import { useWaveformEmitter } from '../../../../../../hooks/useWaveformEmitter';
import { useSelector } from 'react-redux';
import { getActiveRecordingState } from '../../../../../../redux/reducers/recordings';
import { PlaylistStatusEnum } from '../../../../../../enums';
import { isBrowser } from 'react-device-detect';
import { useVerifyScreenOrientation } from '../../../../../../hooks/useVerifyScreenOrientation';
import { ContextPropsType } from '../../../../../../pages/AppLayout/types.ts';

export const PlaybackOperationsNavMenu: React.FunctionComponent = () => {
  const { emitter } = useOutletContext<ContextPropsType>();
  const { isPortraitView } = useVerifyScreenOrientation();
  const audioState: PlaylistStatusEnum = useSelector(getActiveRecordingState);
  const [activeItem, setActiveItem] = React.useState('');
  const { t } = useTranslation();

  const {
    emitterStop,
    emitterForward,
    emitterRewind,
    emitterPlay,
    emitterPause,
    emitterPauseRecording,
  } = useWaveformEmitter({
    emitter,
  });

  const onSelect = (result: { itemId: number | string }) => {
    if (result.itemId !== activeItem) setActiveItem(result.itemId as string);
  };

  const handlePause = () => {
    if (
      [
        PlaylistStatusEnum.RECORDING,
        PlaylistStatusEnum.RECORDING_PAUSED,
      ].includes(audioState)
    )
      emitterPauseRecording();
    else emitterPause();
  };

  const options = [
    {
      key: 'play',
      title: t('tools.items.editor.actions.option4.play'),
      icon: <FaPlayIcon />,
      onClick: emitterPlay,
      isDisabled: [
        PlaylistStatusEnum.RECORDING,
        PlaylistStatusEnum.RECORDING_PAUSED,
      ].includes(audioState),
    },
    {
      key: 'pause',
      title: t('tools.items.editor.actions.option4.pause'),
      icon: <FaPauseIcon />,
      onClick: handlePause,
      isDisabled: [PlaylistStatusEnum.STOPPED].includes(audioState),
    },
    {
      key: 'stop',
      title: t('tools.items.editor.actions.option4.stop'),
      icon: <FaStopIcon />,
      onClick: emitterStop,
      isDisabled: false,
    },
    {
      key: 'rewind',
      title: t('tools.items.editor.actions.option4.rewind'),
      icon: <RewindIcon fill="white" height="15" width="15" />,
      onClick: () => emitterRewind(10),
      isDisabled: [
        PlaylistStatusEnum.RECORDING,
        PlaylistStatusEnum.RECORDING_PAUSED,
      ].includes(audioState),
    },
    {
      key: 'forward',
      title: t('tools.items.editor.actions.option4.forward'),
      icon: <ForwardIcon fill="white" height="15" width="15" />,
      onClick: () => emitterForward(10),
      isDisabled: [
        PlaylistStatusEnum.RECORDING,
        PlaylistStatusEnum.RECORDING_PAUSED,
      ].includes(audioState),
    },
  ];

  const desktopCSSVersion =
    isBrowser || !isPortraitView
      ? { width: 'fit-content', margin: '0 auto' }
      : {};

  return (
    <Nav
      onSelect={onSelect}
      variant="horizontal-subnav"
      className="horizontal-subnav-audio-editor"
      aria-label="Horizontal subnav audio editor playback ops"
      style={desktopCSSVersion}
    >
      <NavList>
        {options.map(({ key, onClick, isDisabled, icon, title }) => (
          <NavItem
            preventDefault
            key={key}
            itemId={key}
            isActive={activeItem === key}
            id={`horizontal-playback-subnav-${key}`}
            to={`#horizontal-playback-subnav-${key}`}
            onClick={
              isDisabled || (activeItem === key && activeItem !== 'play')
                ? undefined
                : onClick
            }
            className={`${isDisabled ? 'horizontal-navmenu-inactive' : ''}`}
          >
            {icon}
            {title}
          </NavItem>
        ))}
      </NavList>
    </Nav>
  );
};
