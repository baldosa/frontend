import { useCallback } from 'react';
import { PlaylistMemoized } from '../WaveformPlaylist';
import { AudioControlsMemoized } from './AudioControls';
import Timer from '../Timer';
import { Panel } from '@patternfly/react-core';
import RecorderStatus from './RecorderStatus';
import { BreadcrumbToolsContext } from '../../../../BreadcrumbToolsContext';
import { useTranslation } from 'react-i18next';
import { HorizontalNavEditor } from '../Editor/HorizontalNavEditor';
import { useDispatch, useSelector } from 'react-redux';
import { setAudioRendering } from '../../../../../redux/actions/recordings';
import {
  getActiveRecordingState,
  getEditorContext,
  getRecordingCounter,
} from '../../../../../redux/reducers/recordings';
import {
  AudioExportTypesEnum,
  AudioRenderingContextEnum,
  PlaylistStatusEnum,
  PlaylistContextEnum,
} from '../../../../../enums';
import { saveAs } from 'file-saver';
import {
  getTextInputValue,
  RECORDING_NAME,
  AUDIO_RENDERING_CONTEXT,
} from '../../../../../utils/waveformUtils';
import { initAudioExporter } from '../../../../../utils/audioExporter';

export default function RecorderWrapper() {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const audioState = useSelector(getActiveRecordingState);
  const editorContext = useSelector(getEditorContext);
  const recordingCounter = useSelector(getRecordingCounter);

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const onAudioRenderingFinished = useCallback(async (type: any, data: any) => {
    try {
      const audioRenderingContextInput = getTextInputValue(
        AUDIO_RENDERING_CONTEXT,
      );
      const recordingNameInput = getTextInputValue(RECORDING_NAME);

      if (audioRenderingContextInput === AudioRenderingContextEnum.UPLOAD) {
        if (type == AudioExportTypesEnum.WAV)
          dispatch(setAudioRendering(type, data));
      } else if (
        audioRenderingContextInput === AudioRenderingContextEnum.DOWNLOAD
      ) {
        if (type == AudioExportTypesEnum.WAV)
          saveAs(data, `${recordingNameInput}.${type}`);
        else await initAudioExporter(data, String(recordingNameInput), type);
      }
    } catch (e) {
      console.log('Error', e);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const onZipProjectExported = useCallback((blob: any) => {
    const audioRenderingContextInput = getTextInputValue(
      AUDIO_RENDERING_CONTEXT,
    );
    const recordingNameInput = getTextInputValue(RECORDING_NAME);

    if (audioRenderingContextInput === AudioRenderingContextEnum.UPLOAD) {
      /* ADD HERE LOGIC TO UPLOAD PROJECT TO THE SERVER */
      dispatch(setAudioRendering(AudioExportTypesEnum.ZIP, blob));
    } else if (
      audioRenderingContextInput === AudioRenderingContextEnum.DOWNLOAD
    ) {
      saveAs(blob, `${recordingNameInput}.${AudioExportTypesEnum.ZIP}`);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Panel
      isScrollable
      style={{
        backgroundColor: '#201C34',
        display: 'flex',
        flex: 1,
        paddingLeft: 15,
        paddingRight: 10,
        height: 'calc(100vh - 70px)',
        justifyContent: 'space-between',
        flexDirection: 'column',
        overflow: 'auto',
      }}
    >
      <div>
        <HorizontalNavEditor />

        {PlaylistStatusEnum.NONE !== audioState &&
          editorContext === PlaylistContextEnum.RECORDER && <Timer />}

        {editorContext === PlaylistContextEnum.RECORDER && <RecorderStatus />}

        {editorContext === PlaylistContextEnum.EDITOR &&
          [
            PlaylistStatusEnum.RECORDING,
            PlaylistStatusEnum.RECORDING_PAUSED,
          ].includes(audioState) &&
          recordingCounter === 1 && <RecorderStatus />}

        <PlaylistMemoized
          onAudioRenderingFinished={onAudioRenderingFinished}
          onZipProjectExported={onZipProjectExported}
        />
      </div>
      <div>
        <AudioControlsMemoized />
        <BreadcrumbToolsContext
          context={editorContext}
          currentPage={
            editorContext === PlaylistContextEnum.RECORDER
              ? t('title.tools.recorder')
              : t('title.tools.editor')
          }
        />
      </div>
    </Panel>
  );
}
