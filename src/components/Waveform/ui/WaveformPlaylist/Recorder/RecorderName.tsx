import { TextContent, Text, TextVariants } from '@patternfly/react-core';
import { useSelector } from 'react-redux';
import { PlaylistStatusEnum } from '../../../../../enums/index';
import {
  getActiveRecordingState,
  getRecordingName,
  getRecordingCounter,
} from '../../../../../redux/reducers/recordings';

export default function RecorderName() {
  const audioState: PlaylistStatusEnum = useSelector(getActiveRecordingState);
  const recordingCounter: number = useSelector(getRecordingCounter);
  const recordingName: string = useSelector(getRecordingName);

  if (
    recordingCounter > 0 &&
    [
      PlaylistStatusEnum.PAUSED,
      PlaylistStatusEnum.PLAYING,
      PlaylistStatusEnum.STOPPED,
    ].includes(audioState)
  ) {
    return (
      <TextContent
        style={{
          textAlign: 'center',
          paddingTop: 20,
          marginBottom: 10,
        }}
      >
        <Text component={TextVariants.small} style={{ color: 'white' }}>
          {recordingName}
        </Text>
      </TextContent>
    );
  }

  return <div></div>;
}
