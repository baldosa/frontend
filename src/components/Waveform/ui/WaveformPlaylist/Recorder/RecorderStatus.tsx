import { TextContent, Text, TextVariants } from '@patternfly/react-core';
import { useSelector } from 'react-redux';
import {
  PlaylistStatusEnum,
  PlaylistContextEnum,
} from '../../../../../enums/index';
import {
  getActiveRecordingState,
  getEditorContext,
} from '../../../../../redux/reducers/recordings';
import { useTranslation } from 'react-i18next';

export default function RecorderStatus() {
  const audioState: PlaylistStatusEnum = useSelector(getActiveRecordingState);
  const editorContext: PlaylistContextEnum = useSelector(getEditorContext);
  const { t } = useTranslation();

  function getColor() {
    switch (audioState) {
      case PlaylistStatusEnum.RECORDING:
        return '#ff4700';
      case PlaylistStatusEnum.RECORDING_PAUSED:
        return '#ff8000';
      case PlaylistStatusEnum.STOPPED:
        return '#87da87';
      case PlaylistStatusEnum.IMPORTED_AUDIO:
        return '#a4c4ff';
      default:
        return '#87da87';
    }
  }

  function getStatus() {
    switch (audioState) {
      case PlaylistStatusEnum.RECORDING:
        return t('tools.recording_status.recording');
      case PlaylistStatusEnum.RECORDING_PAUSED:
        return t('tools.recording_status.paused');
      case PlaylistStatusEnum.IMPORTED_AUDIO:
        return t('tools.recording_status.imported_audios');
      case PlaylistStatusEnum.STOPPED:
        return t('tools.recording_status.finished');
      default:
        return t('tools.recording_status.finished');
    }
  }

  if (audioState === PlaylistStatusEnum.NONE) return <div></div>;

  return (
    <TextContent
      style={{
        textAlign: 'center',
        marginTop: editorContext === PlaylistContextEnum.EDITOR ? 20 : 0,
      }}
    >
      <Text style={{ color: getColor() }} component={TextVariants.small}>
        {getStatus()}
      </Text>
    </TextContent>
  );
}
