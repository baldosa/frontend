import React from 'react';
import { Button, Icon } from '@patternfly/react-core';
import FaStopIcon from '@patternfly/react-icons/dist/esm/icons/stop-icon';

export const StopButton = ({
  disabled,
  onClick,
  isRecordingPaused = false,
}: {
  audioState?: string;
  disabled?: boolean;
  onClick: () => void;
  isRecordingPaused?: boolean;
}): React.ReactElement => {
  const classColor = isRecordingPaused
    ? 'audio-controls-group-recorder-stop-disabled'
    : 'audio-controls-group-recorder-stop-red';
  const classColorWrapper = isRecordingPaused
    ? 'audio-controls-group-recorder-stop-wrapper-disabled'
    : '';
  return (
    <div
      className={`audio-controls-group-recorder-stop-wrapper ${classColorWrapper}`}
    >
      <Button
        id="recording-stop-button"
        className={` audio-controls-group-recorder-stop ${classColor}`}
        data-testid="stop-button"
        isDisabled={isRecordingPaused}
        style={{
          alignItems: 'center',
          justifyContent: 'center',
        }}
        {...(!disabled ? { onClick: onClick } : {})}
      >
        <Icon size="md">
          <FaStopIcon style={{ fill: '#201C34' }} />
        </Icon>
      </Button>
    </div>
  );
};
