import { useState } from 'react';
import { Button } from '@patternfly/react-core';
import { UploadIcon } from '../../../../../assets/icon/studio/UploadIcon';
import { ImportAudioIcon } from '../../../../../assets/icon/studio/ImportAudioIcon';
import { DownloadIcon } from '../../../../../assets/icon/studio/DownloadIcon';
import { TrashIcon } from '../../../../../assets/icon/studio/TrashIcon';
import { RecorderIcon } from '../../../../../assets/icon/studio/RecorderIcon.tsx';
import { EditorIcon } from '../../../../../assets/icon/studio/EditorIcon.tsx';
import { useSelector } from 'react-redux';
import { useWaveformEmitter } from '../../../../../hooks/useWaveformEmitter';
import {
  PlaylistStatusEnum,
  AudioRenderingContextEnum,
  AudioExportTypesEnum,
} from '../../../../../enums';
import SmallModal from '../../../../SmallModal';
import ExportAudioModal from '../../ExportAudioModal';
import { useTranslation } from 'react-i18next';
import {
  getActiveRecordingState,
  getEditorOptionsVisibility,
} from '../../../../../redux/reducers/recordings';
import {
  setEditorOptionsVisibility,
  setAudioRenderingContext,
} from '../../../../../redux/actions/recordings';
import { useDispatch } from 'react-redux';
import ImportAudioModal from '../../ImportAudioModal';
import { UploadRecordingActionModal } from './RecorderActions/UploadRecordingAction';
import { ContextPropsType } from '../../../../../pages/AppLayout/types.ts';

export default function RecorderActions({ emitter }: ContextPropsType) {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const [isModalRemoveAudioOpen, setIsModalRemoveAudioOpen] = useState(false);
  const [isExportModalOpen, setExportModalOpen] = useState(false);
  const [isUploadModalOpen, setUploadModalOpen] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [isModalImportAudioOpen, setIsModalImportAudioOpen] = useState(false);
  const audioState: PlaylistStatusEnum = useSelector(getActiveRecordingState);
  const editorOptionsVisibility: boolean = useSelector(
    getEditorOptionsVisibility,
  );
  const {
    emitterClear,
    emitterRenderingWav,
    emitterRenderingOpus,
    emitterExportProject,
  } = useWaveformEmitter({
    emitter,
  });

  const handleOpenExportModal = () => {
    dispatch(setAudioRenderingContext(AudioRenderingContextEnum.DOWNLOAD));
    setExportModalOpen(true);
  };

  const handleCloseModal = () => {
    setIsModalRemoveAudioOpen(false);
  };

  const handleCloseExportModal = () => {
    setExportModalOpen(false);
  };

  const onFinishedExportModal = (type: AudioExportTypesEnum) => {
    switch (type) {
      case AudioExportTypesEnum.WAV:
        emitterRenderingWav();
        break;
      case AudioExportTypesEnum.ZIP:
        emitterExportProject();
        break;
      case AudioExportTypesEnum.OPUS:
        emitterRenderingOpus();
        break;
      default:
        break;
    }
    setExportModalOpen(false);
  };

  const handleDiscardRecording = () => {
    setIsLoading(true);
    emitterClear();

    setTimeout(() => {
      setIsModalRemoveAudioOpen(false);
      setIsLoading(false);
    }, 1000);
  };

  const onUploadStart = (type: AudioExportTypesEnum) => {
    setIsLoading(true);

    switch (type) {
      case AudioExportTypesEnum.WAV:
        emitterRenderingWav();
        break;

      case AudioExportTypesEnum.ZIP:
        emitterExportProject();
        break;

      default:
        setIsLoading(false);
    }
  };

  const onUploadError = () => {
    setTimeout(() => {
      setIsLoading(false);
    }, 1000);
  };

  const onUploadSuccess = () => {
    emitterClear();

    setTimeout(() => {
      setUploadModalOpen(false);
      setIsLoading(false);
    }, 1000);
  };

  const handleOpenUploadModal = () => {
    dispatch(setAudioRenderingContext(AudioRenderingContextEnum.UPLOAD));
    setUploadModalOpen(true);
  };

  const handleCloseUploadModal = () => {
    setUploadModalOpen(false);
    setIsLoading(false);
  };

  const switchEditorOptionsVisibility = () => {
    dispatch(setEditorOptionsVisibility(!editorOptionsVisibility));
  };

  const handleCloseImportAudioModal = () => {
    setIsModalImportAudioOpen(false);
  };

  const handleOpenImportAudioModal = () => {
    setIsModalImportAudioOpen(true);
  };

  const handleOpenRemoveAudioModal = () => {
    setIsModalRemoveAudioOpen(true);
  };

  if (
    [
      PlaylistStatusEnum.STOPPED,
      PlaylistStatusEnum.PLAYING,
      PlaylistStatusEnum.PAUSED,
      PlaylistStatusEnum.IMPORTED_AUDIO,
    ].includes(audioState)
  ) {
    return (
      <>
        <SmallModal
          description={String(t('tools.modal_remove_audio.description'))}
          isModalOpen={isModalRemoveAudioOpen}
          handleClose={handleCloseModal}
          actions={[
            <Button
              key="confirm"
              variant="primary"
              isLoading={isLoading}
              onClick={handleDiscardRecording}
            >
              {t('tools.modal_remove_audio.confirm_button')}
            </Button>,
            <Button key="cancel" variant="link" onClick={handleCloseModal}>
              {t('tools.modal_remove_audio.discard_button')}
            </Button>,
          ]}
        />
        <ExportAudioModal
          isModalOpen={isExportModalOpen}
          handleClose={handleCloseExportModal}
          onFinished={onFinishedExportModal}
        />
        {emitter && (
          <ImportAudioModal
            emitter={emitter}
            isOpen={isModalImportAudioOpen}
            handleClose={handleCloseImportAudioModal}
          />
        )}
        {
          // TODO: Reuse the same modal for different use cases
        }
        <UploadRecordingActionModal
          handleClose={handleCloseUploadModal}
          isOpen={isUploadModalOpen}
          onStart={onUploadStart}
          onSuccess={onUploadSuccess}
          onError={onUploadError}
          isLoading={isLoading}
        />
        <div className="recorder-actions-menu">
          {editorOptionsVisibility && (
            <Button
              variant="plain"
              className="recorder-action-menu-button"
              aria-label="import-audio-upload"
              onClick={handleOpenImportAudioModal}
            >
              <ImportAudioIcon fill="white" />
            </Button>
          )}
          <Button
            variant="plain"
            className="recorder-action-menu-button"
            aria-label="action-upload"
            onClick={handleOpenUploadModal}
          >
            <UploadIcon fill="white" />
          </Button>
          <Button
            variant="plain"
            className="recorder-action-menu-button"
            aria-label="action-download"
            onClick={handleOpenExportModal}
          >
            <DownloadIcon fill="white" />
          </Button>
          <Button
            variant="plain"
            aria-label="action-discard"
            className="recorder-action-menu-button"
            onClick={handleOpenRemoveAudioModal}
          >
            <TrashIcon fill="white" />
          </Button>
          <Button
            variant="plain"
            aria-label="action-equalizer"
            className="recorder-action-menu-button"
            onClick={switchEditorOptionsVisibility}
          >
            {editorOptionsVisibility ? <RecorderIcon /> : <EditorIcon />}
          </Button>
        </div>
      </>
    );
  }

  return <div></div>;
}
