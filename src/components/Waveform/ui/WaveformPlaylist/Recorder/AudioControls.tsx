import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { useWaveformEmitter } from '../../../../../hooks/useWaveformEmitter';
import { useTranslation } from 'react-i18next';
import { PauseButton } from './PauseButton';
import { RecordButtonWrapper } from '../../RecordButton/RecordButtonWrapper';
import { InitialEditorButtonsWrapper } from '../../InitialEditorButtonsWrapper';
import { StopPauseButtonsWrapper } from '../../Wrappers/StopPauseButtonsWrapper';
import { PlaybackButtonsWrapper } from '../../Wrappers/PlaybackButtonsWrapper';
import { StopButton } from './StopButton';
import {
  PlaylistStatusEnum,
  PlaylistContextEnum,
} from '../../../../../enums';
import { useOutletContext } from 'react-router-dom';
import {
  getActiveRecordingState,
  getRecordingCounter,
  getEditorContext,
  getEditorOptionsVisibility,
} from '../../../../../redux/reducers/recordings';
import PlaybackButtons from './PlaybackButtons';
import { RecordButton } from '../../RecordButton/RecordButton';
import { addClassToPlaylistCursor } from '../../../../../utils/waveformUtils';
import DiscardAudioModal from '../../DiscardAudioModal';
import ImportAudioModal from '../../ImportAudioModal';
import { isBrowser } from 'react-device-detect';
import { useVerifyScreenOrientation } from '../../../../../hooks/useVerifyScreenOrientation';
import { ContextPropsType } from '../../../../../pages/AppLayout/types.ts';

export default function AudioControls() {
  const { t } = useTranslation();
  const [isModalOpen, setModalOpen] = useState(false);
  const [isModalImportAudioOpen, setIsModalImportAudioOpen] = useState(false);
  const { emitter } = useOutletContext<ContextPropsType>();
  const audioState: PlaylistStatusEnum = useSelector(getActiveRecordingState);
  const recordingCounter: number = useSelector(getRecordingCounter);
  const editorContext: PlaylistContextEnum = useSelector(getEditorContext);
  const { isPortraitView } = useVerifyScreenOrientation();
  const editorOptionsVisibility: boolean = useSelector(
    getEditorOptionsVisibility,
  );

  const { emitterRecord, emitterStop, emitterPauseRecording } =
    useWaveformEmitter({
      emitter,
    });

  const handleRecording = async () => {
    emitterRecord();
    addClassToPlaylistCursor('cursor-recording');
  };

  function handleStop() {
    emitterStop();
    addClassToPlaylistCursor('cursor-recording-stopped');
  }

  function handlePauseRecording() {
    emitterPauseRecording();
    addClassToPlaylistCursor('cursor-recording-paused');
  }

  const stopPauseButtons = (
    <StopPauseButtonsWrapper>
      <div
        style={{
          display: 'flex',
          width: isBrowser || !isPortraitView ? 'auto' : '50%',
          marginRight: isBrowser || !isPortraitView ? 20 : 0,
          justifyContent: 'center',
        }}
      >
        <PauseButton
          onClick={handlePauseRecording}
          isRecordingPaused={audioState === PlaylistStatusEnum.RECORDING_PAUSED}
        />
      </div>
      <StopButton onClick={handleStop} />
    </StopPauseButtonsWrapper>
  );

  const playbackButtons = (
    <PlaybackButtonsWrapper>
      <PlaybackButtons />
      <RecordButton id="initial-recorder-button" onClick={handleRecording} />
    </PlaybackButtonsWrapper>
  );

  const handleOpenModal = () => {
    setModalOpen(true);
  };

  function handleClose() {
    setModalOpen(false);
  }

  function handleCloseImportAudioModal() {
    setIsModalImportAudioOpen(false);
  }

  const handleOpenImportAudioModal = () => {
    setIsModalImportAudioOpen(true);
  };

  const renderRecorderButton = (withTooltip = '') => {
    const tooltipProperties = {
      withTooltip: true,
      tooltipTitle: t('record-button-tooltip'),
    };
    const props = withTooltip === '' ? {} : tooltipProperties;
    const handleFn = withTooltip === '' ? handleOpenModal : handleRecording;

    if (
      editorContext === PlaylistContextEnum.EDITOR &&
      audioState === PlaylistStatusEnum.NONE
    ) {
      return (
        <InitialEditorButtonsWrapper
          audioState={audioState}
          handleRecording={handleFn}
          handleImportAudio={handleOpenImportAudioModal}
          tooltipTitleRecord={String(t('record-button-tooltip'))}
          tooltipTitleImportAudio={String(t('import-audio-button-tooltip'))}
        />
      );
    }

    return (
      <RecordButtonWrapper
        audioState={audioState}
        handleRecording={handleFn}
        {...props}
      />
    );
  };

  return (
    <div style={{ paddingTop: 10 }}>
      <DiscardAudioModal isOpen={isModalOpen} handleClose={handleClose} />
      {emitter && (
        <ImportAudioModal
          emitter={emitter}
          isOpen={isModalImportAudioOpen}
          handleClose={handleCloseImportAudioModal}
        />
      )}

      {/* Renders the main record button */}
      {recordingCounter === 0 &&
        [PlaylistStatusEnum.NONE, PlaylistStatusEnum.STOPPED].includes(
          audioState,
        ) &&
        renderRecorderButton('with-tooltip')}

      {/* Render the record button after, at least, one recording finished */}
      {recordingCounter > 0 &&
        [
          PlaylistStatusEnum.PAUSED,
          PlaylistStatusEnum.PLAYING,
          PlaylistStatusEnum.STOPPED,
          PlaylistStatusEnum.IMPORTED_AUDIO,
        ].includes(audioState) &&
        !editorOptionsVisibility &&
        playbackButtons}

      {/* Renders controls for the portrait view */}
      {recordingCounter >= 0 &&
        [
          PlaylistStatusEnum.RECORDING,
          PlaylistStatusEnum.RECORDING_PAUSED,
        ].includes(audioState) &&
        !editorOptionsVisibility &&
        stopPauseButtons}
    </div>
  );
}

export const AudioControlsMemoized = React.memo(AudioControls);
