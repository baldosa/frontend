import { useEffect, useState } from 'react';
import { getAccessToken } from '../../../../../../redux/reducers/auth';
import { useSelector } from 'react-redux';
import { AudioExportTypesEnum } from '../../../../../../enums';
import {
  Button,
  HelperText,
  HelperTextItem,
  SelectOptionObject,
  TextInput,
} from '@patternfly/react-core';
import SmallModal from '../../../../../SmallModal';
import { TeamSelectionWrapper } from './TeamSelectionWrapper';
import {
  getAudioRenderingData,
  getAudioRenderingType,
} from '../../../../../../redux/reducers/recordings';
import {
  clearAudioRendering,
  setRecordingName as setRecordingNameAction,
} from '../../../../../../redux/actions/recordings';
import { useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { generateRecordingName } from '../../../../../../utils/utils.ts';
import i18n from 'i18next';
import {
  shareProject,
  shareUpload,
} from '../../../../../../service/recorder.ts';
import {
  useContextClient
} from '../../../../../../pages/AppLayout/Contexts.tsx';

type UploadRecordingActionModalProps = {
  handleClose: () => void;
  isLoading: boolean;
  isOpen: boolean;
  onError: () => void;
  onSuccess: () => void;
  onStart: (type: AudioExportTypesEnum) => void;
};

export const UploadRecordingActionModal = ({
  handleClose,
  isLoading,
  isOpen,
  onError,
  onSuccess,
  onStart,
}: UploadRecordingActionModalProps) => {
  const [getRecordingName, setRecordingName] = useState<string>(generateRecordingName(i18n.language));
  const [getTeamSelection, setTeamSelection] = useState<SelectOptionObject[]>([]);
  const [getUploadErrorMessage, setUploadErrorMessage] = useState<string>('');
  const [getUploadContext, setUploadContext] = useState<string>('');
  const [hasUploadError, setUploadError] = useState<boolean>(false);

  const audioRenderingData = useSelector(getAudioRenderingData);
  const audioRenderingType = useSelector(getAudioRenderingType);
  const userToken: string = useSelector(getAccessToken);

  const dispatch = useDispatch();

  const { getOpenAPIClient } = useContextClient();
  const { t } = useTranslation();

  function setExtension(filename: string, ext: string): string {
    return `${filename}.${ext}`;
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const handleUpload = (renderingType: AudioExportTypesEnum, renderingData: any) => {
    const filename: string = setExtension(
      getRecordingName.replace(/\s/g, ''),
      audioRenderingType,
    );

    const data = new FormData();
    data.append('file', renderingData);
    data.append('filename', filename);
    data.append('teams', getTeamSelection.join());

    switch (renderingType) {
      case AudioExportTypesEnum.WAV:
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        shareUpload(userToken, filename, data)(getOpenAPIClient!)
          .then((): void => {
            resetErrors();
            onSuccess();
            dispatch(clearAudioRendering());
          })
          .catch((): void => {
            setError(t('tools.items.recorder.actions.upload_error'));
            onError();
            dispatch(clearAudioRendering());
          });
        break;
      case AudioExportTypesEnum.ZIP:
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        shareProject(userToken, filename, data)(getOpenAPIClient!)
          .then((): void => {
            resetErrors();
            onSuccess();
            dispatch(clearAudioRendering());
          })
          .catch((): void => {
            setError(t('tools.items.recorder.actions.upload_error'));
            onError();
            dispatch(clearAudioRendering());
          });
        break;
      default:
        break
    }
  }

  useEffect(() => {
    if (audioRenderingData !== null) {
      handleUpload(audioRenderingType, audioRenderingData)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [audioRenderingData]);

  const onTeamSelect = (selection: SelectOptionObject[]) => {
    setTeamSelection(selection);
  };

  function resetErrors() {
    setUploadError(false);
    setUploadErrorMessage('');
  }

  function setError(message: string) {
    setUploadError(true);
    setUploadErrorMessage(message);
  }

  function handleNameChange(message: string) {
    setRecordingName(message);
  }

  function isValidRecordingName(name: string) {
    return name !== '';
  }

  function isTeamSelectionValid(selection: SelectOptionObject[]) {
    return selection.length > 0;
  }

  function isUploadDisabled(): boolean {
    return (
      !isTeamSelectionValid(getTeamSelection) ||
      !isValidRecordingName(getRecordingName)
    );
  }

  function isUploadWAVLoading(): boolean {
    return getUploadContext == AudioExportTypesEnum.WAV && isLoading;
  }

  function isUploadProjectLoading(): boolean {
    return getUploadContext == AudioExportTypesEnum.ZIP && isLoading;
  }

  function isUploadWAVDisabled(): boolean {
    return isUploadDisabled() || isLoading;
  }

  function isUploadProjectDisabled(): boolean {
    return isUploadDisabled() || isLoading;
  }

  function onUploadWAVClick() {
    resetErrors();
    setUploadContext(AudioExportTypesEnum.WAV);
    dispatch(setRecordingNameAction(getRecordingName));
    onStart(AudioExportTypesEnum.WAV);
  }

  function onUploadProjectClick() {
    resetErrors();
    setUploadContext(AudioExportTypesEnum.ZIP);
    dispatch(setRecordingNameAction(getRecordingName));
    onStart(AudioExportTypesEnum.ZIP);
  }

  const renderError = (hasError: boolean, errorMessage: string) => {
    return hasError ? (
      <HelperText
        style={{
          marginTop: '0.5rem',
          marginBottom: '0.5rem',
        }}
      >
        <HelperTextItem variant="error" hasIcon>
          {errorMessage}
        </HelperTextItem>
      </HelperText>
    ) : (
      <></>
    );
  };

  return (
    <SmallModal
      title={String(t('tools.items.recorder.actions.upload_recording_title'))}
      isModalOpen={isOpen}
      handleClose={handleClose}
      actions={[
        <Button
          key="confirm"
          variant="primary"
          isLoading={isUploadWAVLoading()}
          isDisabled={isUploadWAVDisabled()}
          onClick={onUploadWAVClick}
        >
          {t('tools.items.recorder.actions.upload_recording')}
        </Button>,
        <Button
          key="cancel"
          variant="link"
          isLoading={isUploadProjectLoading()}
          isDisabled={isUploadProjectDisabled()}
          onClick={onUploadProjectClick}
        >
          {t('tools.items.recorder.actions.save_project')}
        </Button>,
      ]}
      content={
        <div>
          <TextInput
            value={getRecordingName}
            onChange={(value) => handleNameChange(value)}
            aria-label="select-all"
            placeholder={
              t('tools.items.recorder.placeholder.choose_filename') || ''
            }
            style={{
              marginTop: '1rem',
              marginBottom: '1rem',
            }}
          />
          <TeamSelectionWrapper onSelect={onTeamSelect} />
          {renderError(hasUploadError, getUploadErrorMessage)}
        </div>
      }
    />
  );
};
