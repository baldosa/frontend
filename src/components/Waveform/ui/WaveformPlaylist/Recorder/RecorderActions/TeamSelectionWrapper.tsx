import React, {
  FunctionComponent,
  ReactElement,
  useEffect,
  useState,
} from 'react';
import { getAccessToken } from '../../../../../../redux/reducers/auth';
import { useSelector } from 'react-redux';
import {
  Select,
  SelectOption,
  SelectOptionObject,
  SelectVariant,
} from '@patternfly/react-core';
import { useTranslation } from 'react-i18next';
import { getAllTeams } from '../../../../../../service/team.ts';
import {
  useContextClient
} from '../../../../../../pages/AppLayout/Contexts.tsx';

export const TeamSelectionWrapper: FunctionComponent<
  TeamSelectionWrapperProps
> = (props) => {
  const [getTeamSelectionsChecked, setTeamSelectionsChecked] = useState<SelectOptionObject[]>([]);
  const [getTeamSelectOptions, setTeamSelectOptions] = useState<ReactElement[]>([]);
  const [isTeamSelectionOpen, setTeamSelectionOpen] = useState<boolean>(false);

  const userToken: string = useSelector(getAccessToken);

  const { getOpenAPIClient } = useContextClient();
  const { t } = useTranslation();

  useEffect(() => {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    getAllTeams(userToken)(getOpenAPIClient!)
      .then((response): void => {
        setTeamSelectOptions(
          response.data.map((team, index) => {
            let teamName = team.name;
            if (team.is_personal_workspace) {
              teamName = t('tools.items.recorder.options.personal_workspace');
              onSelect(null, team.id);
            }
            return (
              <SelectOption key={index} value={team.id} children={teamName} />
            );
          }),
        );
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userToken]);

  function onToggle(isOpen: boolean) {
    setTeamSelectionOpen(isOpen);
  }

  function onSelect(
    _event: React.MouseEvent<Element, MouseEvent> | React.ChangeEvent | null,
    selection: SelectOptionObject | number,
  ) {
    let selections: SelectOptionObject[];
    if (getTeamSelectionsChecked.includes(selection)) {
      selections = getTeamSelectionsChecked.filter(
        (item) => selection !== item,
      );
    } else {
      selections = [...getTeamSelectionsChecked, selection];
    }

    setTeamSelectionsChecked(selections);
    props.onSelect(selections);
  }

  function renderPlaceholderHint(selectionsCheckedAmount: number) {
    return selectionsCheckedAmount
      ? t('tools.items.recorder.options.select_label')
      : t('tools.items.recorder.options.empty_options_error');
  }

  return (
    <Select
      className={'teams-modal-selector'}
      variant={SelectVariant.checkbox}
      menuAppendTo={'parent'}
      onToggle={onToggle}
      onSelect={onSelect}
      selections={getTeamSelectionsChecked}
      isOpen={isTeamSelectionOpen}
      placeholderText={renderPlaceholderHint(getTeamSelectOptions.length)}
      aria-label={t('tools.items.recorder.options.select_label').toString()}
      aria-labelledby={t(
        'tools.items.recorder.options.select_label',
      ).toString()}
      children={getTeamSelectOptions}
    />
  );
};

type TeamSelectionWrapperProps = {
  onSelect: (selection: SelectOptionObject[]) => void;
};