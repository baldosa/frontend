import React from 'react';
import { Button } from '@patternfly/react-core';
import { ForwardIcon } from '../../../../../assets/icon/studio/ForwardIcon';

export const ForwardButton = ({
  onClick,
  disabled,
}: {
  audioState?: string;
  disabled?: boolean;
  onClick: () => void;
}): React.ReactElement => {
  const classes = 'audio-controls-group-recorder-finished';
  return (
    <Button
      className={classes}
      data-testid="forward-button"
      {...(!disabled ? { onClick: onClick } : {})}
    >
      <ForwardIcon fill="white" />
    </Button>
  );
};
