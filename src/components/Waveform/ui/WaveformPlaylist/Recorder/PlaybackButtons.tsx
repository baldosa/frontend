import { useSelector } from 'react-redux';
import { useWaveformEmitter } from '../../../../../hooks/useWaveformEmitter';
import { PauseButton } from './PauseButton';
import { PlayButton } from './PlayButton';
import { FinishedRecordingsButtonsWrapper } from '../../Wrappers/FinishedRecordingsButtonsWrapper';
import { RewindButton } from './RewindButton';
import { ForwardButton } from './ForwardButton';
import { PlaylistStatusEnum } from '../../../../../enums';
import { useOutletContext } from 'react-router-dom';
import { getActiveRecordingState } from '../../../../../redux/reducers/recordings';
import { ContextPropsType } from '../../../../../pages/AppLayout/types.ts';

export default function PlaybackButtons() {
  const { emitter } = useOutletContext<ContextPropsType>();
  const audioState: PlaylistStatusEnum = useSelector(getActiveRecordingState);
  const { emitterRewind, emitterPlay, emitterForward, emitterPause } =
    useWaveformEmitter({
      emitter,
    });

  return (
    <FinishedRecordingsButtonsWrapper>
      <RewindButton onClick={() => emitterRewind(10)} />
      {[
        PlaylistStatusEnum.PAUSED,
        PlaylistStatusEnum.STOPPED,
        PlaylistStatusEnum.IMPORTED_AUDIO,
      ].includes(audioState) && <PlayButton onClick={emitterPlay} />}
      {[PlaylistStatusEnum.PLAYING].includes(audioState) && (
        <PauseButton onClick={emitterPause} isRecordingPaused={false} />
      )}
      <ForwardButton onClick={() => emitterForward(10)} />
    </FinishedRecordingsButtonsWrapper>
  );
}
