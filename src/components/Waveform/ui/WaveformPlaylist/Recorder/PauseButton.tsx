import React from 'react';
import { Button } from '@patternfly/react-core';
import FaPauseIcon from '@patternfly/react-icons/dist/esm/icons/pause-icon';

export const PauseButton = ({
  onClick,
  disabled,
  isRecordingPaused = false,
}: {
  disabled?: boolean;
  onClick: () => void;
  isRecordingPaused: boolean;
}): React.ReactElement => {
  const fill = isRecordingPaused ? '#201C34' : '#ff271d';
  const backgroundColor = isRecordingPaused ? '#ff271d' : '#D4D4D4';

  return (
    <Button
      id="recording-pause-button"
      className="audio-controls-group-recorder-pause"
      data-testid="pause-button"
      style={{ backgroundColor }}
      {...(!disabled ? { onClick: onClick } : {})}
    >
      <FaPauseIcon style={{ fill }} />
    </Button>
  );
};
