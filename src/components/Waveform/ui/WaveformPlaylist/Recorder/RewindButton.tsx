import React from 'react';
import { Button } from '@patternfly/react-core';
import { RewindIcon } from '../../../../../assets/icon/studio/RewindIcon';

export const RewindButton = ({
  onClick,
  disabled,
}: {
  audioState?: string;
  disabled?: boolean;
  onClick: () => void;
}): React.ReactElement => {
  const classes = 'audio-controls-group-recorder-finished';
  return (
    <Button
      className={classes}
      data-testid="rewind-button"
      variant="plain"
      {...(!disabled ? { onClick: onClick } : {})}
    >
      <RewindIcon fill="white" />
    </Button>
  );
};
