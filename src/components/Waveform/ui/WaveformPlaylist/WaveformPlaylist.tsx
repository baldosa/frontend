import React, { useCallback, useMemo } from 'react';
import {
  // createBlobFromAudioBuffer,
  // downloadFile,
  setMicrophonePermission,
  // shortStringByNumber,
} from '../../../../utils/utils';
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import WaveformPlaylist from 'colmena-waveform-playlist';
import {
  setTextInputValue,
  getTextInputValue,
  PLAYBACK_POSITION,
  RECORDING_COUNTER,
  RECORDING_NAME,
  AUDIO_RENDERING_CONTEXT,
} from '../../../../utils/waveformUtils';
import { AudioGlobalConfigEnum, PlaylistStatusEnum } from '../../../../enums';
import { useDispatch, useSelector } from 'react-redux';
import { updateRecordingState } from '../../../../redux/actions/recordings';
import { useOutletContext } from 'react-router-dom';
import { emitterFactory } from '../../../../hooks/useWaveformEmitter';
import WaveformPlaylistWrapper from '../Wrappers/WaveformPlaylistWrapper';
import {
  getRecordingCounter,
  getAudioRenderingContext,
  getRecordingName,
} from '../../../../redux/reducers/recordings';
import {
  setRecordingCounter,
  setEditorOptionsVisibility,
} from '../../../../redux/actions/recordings';
import debounce from 'lodash.debounce';
import { ContextPropsType } from '../../../../pages/AppLayout/types.ts';

type Props = {
  /* eslint-disable @typescript-eslint/no-explicit-any */
  onAudioRenderingFinished?: (type: any, data: any) => void;
  onZipProjectExported?: (blob: any) => void;
};

const Waveform = ({
  onAudioRenderingFinished,
  onZipProjectExported,
}: Props) => {
  const recordingCounter: number = useSelector(getRecordingCounter);
  const audioRenderingContext: string = useSelector(getAudioRenderingContext);
  const recordingName: string = useSelector(getRecordingName);
  const dispatch = useDispatch();

  const { emitter } = useOutletContext<ContextPropsType>();

  const ee = !emitter ? emitterFactory() : emitter;

  const zoomLevels = [500, 1000, 3000, 5000];
  let userMediaStream: MediaStream;

  const applyCommitAfterShift = debounce(() => {
    ee.emit('commit');
  }, 1500);

  const container = useCallback(
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    (node) => {
      async function logError(err: string) {
        await setMicrophonePermission('no');
        // setShowControls(true);
        console.error(err);
      }

      async function gotStream(playlist: any, stream: MediaStream) {
        // eslint-disable-next-line react-hooks/exhaustive-deps
        userMediaStream = stream;
        playlist.initRecorder(userMediaStream);
        // setShowControls(true);
        await setMicrophonePermission('yes');
      }

      window.document.getElementById('waveform')?.firstChild?.remove();
      if (node !== null) {
        const constraints = {
          audio: {
            channelCount: 1,
          },
        };

        if (navigator.mediaDevices) {
          navigator.mediaDevices
            .getUserMedia(constraints)
            .then((stream) => gotStream(playlist, stream))
            .catch(logError);
        }

        const playlist = WaveformPlaylist(
          {
            samplesPerPixel: 500,
            mono: true,
            waveHeight: 135,
            sampleRate: AudioGlobalConfigEnum.SAMPLE_RATE,
            container: node,
            timescale: true,
            state: 'cursor',
            barWidth: 4,
            isAutomaticScroll: true,
            barGap: 2,
            innerWidth: 200,
            colors: {
              waveOutlineColor: '#0C0A21',
              timeColor: 'black',
              fadeColor: 'white',
            },
            controls: {
              show: true,
              width: 80,
              widgets: {
                muterOrSolo: true,
                volume: true,
                collapse: true,
                remove: true,
                stereoPan: true,
              },
            },
            zoomLevels,
          },
          ee,
        );

        ee.on('timeupdate', (playbackPosition: number) => {
          setTextInputValue(PLAYBACK_POSITION, playbackPosition);
        });

        ee.on('finished', () => {
          dispatch(updateRecordingState(PlaylistStatusEnum.STOPPED));
        });

        ee.on('audiosourceserror', (err) => {
          console.log(err);
          /*ee.emit('clear');
          dispatch(updateRecordingState(PlaylistStatusEnum.NONE));
          dispatch(setEditorOptionsVisibility(false));*/
        });

        ee.on('shift', () => {
          applyCommitAfterShift();
        });

        ee.on('removeTrack', () => {
          const recorderCounterInput = Number(
            getTextInputValue(RECORDING_COUNTER),
          );
          const rc = recorderCounterInput - 1;
          dispatch(setRecordingCounter(rc));
          if (rc == 0) {
            ee.emit('clear');
            dispatch(updateRecordingState(PlaylistStatusEnum.NONE));
            dispatch(setEditorOptionsVisibility(false));
          } else {
            ee.emit('commit');
            dispatch(updateRecordingState(PlaylistStatusEnum.STOPPED));
          }
        });

        ee.on('zipProjectExported', async (blob: any) => {
          onZipProjectExported && onZipProjectExported(blob);
        });

        ee.on('audiorenderingfinished', async (type, data) => {
          onAudioRenderingFinished && onAudioRenderingFinished(type, data);
        });

        // if (urlBlob && filename) {
        //   playlist.load([
        //     {
        //       src: urlBlob,
        //       name: shortStringByNumber(filename.split('/').reverse()[0], 12),
        //       waveOutlineColor: 'black',
        //       muted: false,
        //     },
        //   ]);
        // }

        //initialize the WAV exporter.
        playlist.initExporter();
      }
    },
    [ee],
  );

  const waveform = useMemo(
    () => (
      <div id="waveform" data-testid="waveform-container" ref={container}></div>
    ),
    [container],
  );

  return (
    <div>
      <input type="hidden" id={PLAYBACK_POSITION} />
      <input type="hidden" id={RECORDING_COUNTER} value={recordingCounter} />
      <input type="hidden" id={RECORDING_NAME} value={recordingName} />
      <input
        type="hidden"
        id={AUDIO_RENDERING_CONTEXT}
        value={audioRenderingContext}
      />
      <WaveformPlaylistWrapper>{waveform}</WaveformPlaylistWrapper>
    </div>
  );
};

const Playlist = (props: Props) => <Waveform {...props}></Waveform>;

export const PlaylistMemoized = React.memo(Playlist);

export default Playlist;
