import { useTranslation } from 'react-i18next';
import SmallModal from '../../SmallModal';
import { Button } from '@patternfly/react-core';

type Props = {
  isModalOpen: boolean;
  handleClose: () => void;
  onConfirm?: () => void;
  isLoading?: boolean;
};

export default function AbortAudioRecordingModal({
  isModalOpen,
  handleClose,
  onConfirm = undefined,
  isLoading = false,
}: Props) {
  const { t } = useTranslation();

  return (
    <SmallModal
      description={String(t('tools.abort_new_audio.description'))}
      isModalOpen={isModalOpen}
      handleClose={handleClose}
      actions={[
        <Button
          key="confirm"
          isLoading={isLoading}
          variant="primary"
          onClick={onConfirm}
        >
          {t('tools.abort_new_audio.confirm_button')}
        </Button>,
        <Button key="cancel" variant="link" onClick={handleClose}>
          {t('tools.abort_new_audio.cancel_button')}
        </Button>,
      ]}
    />
  );
}
