import React from 'react';
import { RecordButton } from './RecordButton';
import TooltipWrapper from '../../../TooltipWrapper';

export const RecordButtonWrapper = ({
  handleRecording,
  withTooltip = false,
  tooltipTitle = '',
  disabled = false,
}: {
  audioState: string;
  handleRecording: () => void;
  withTooltip?: boolean;
  tooltipTitle?: string;
  disabled?: boolean;
}): React.ReactElement => {
  const initialRecordButtonId = 'initial-record-button';
  return (
    <div
      id="render-recorder-button-with-tooltip"
      style={{
        display: 'flex',
        justifyContent: 'center',
        gap: 20,
      }}
    >
      <>
        {withTooltip && (
          <TooltipWrapper
            reference={initialRecordButtonId}
            title={tooltipTitle}
          />
        )}
        <RecordButton
          id={initialRecordButtonId}
          onClick={handleRecording}
          disabled={disabled}
        />
      </>
    </div>
  );
};
