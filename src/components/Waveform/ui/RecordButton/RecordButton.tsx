import React from 'react';
import { Button } from '@patternfly/react-core';
import { MicrophoneIcon } from '../../../../assets/icon/studio/MicrophoneIcon';

export const RecordButton = ({
  id,
  disabled,
  onClick,
}: {
  id: string;
  disabled?: boolean;
  onClick: () => void;
}): React.ReactElement => {
  return (
    <div className="audio-controls-group-recorder-button-finished-wrapper">
      <Button
        id={id}
        className="audio-controls-group-recorder-button-finished"
        data-testid="stop-button"
        style={{
          justifyContent: 'center',
        }}
        {...(!disabled ? { onClick: onClick } : {})}
      >
        <MicrophoneIcon width={'26'} height={'26'} />
      </Button>
    </div>
  );
};
