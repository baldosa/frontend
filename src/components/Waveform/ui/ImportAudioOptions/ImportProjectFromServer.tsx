import { useEffect, useState } from "react";
import { useTranslation } from 'react-i18next';
import SmallModal from "../../../SmallModal";
import { getAccessToken } from "../../../../redux/reducers/auth";
import { useSelector } from "react-redux";
import { Components } from "../../../../api/utilities/Definitions";
import { blobFromBase64 } from "../../../../utils/logics/fileUtils";
import { AxiosError } from "axios";
import Spinner from "../../../Spinner";
import { useContextClient } from '../../../../pages/AppLayout/Contexts.tsx';
import { getAllProjects, importProject } from '../../../../service/recorder.ts';

type Props = {
  handleClose: () => void
  isOpen: boolean,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  onImportError: (error: AxiosError<{error_code: string}, any>) => void,
  onSelectProject: (project: Blob) => void,
}

export default function ImportProjectFromServer({
  handleClose,
  isOpen,
  onImportError,
  onSelectProject,
}: Props) {
  const { t } = useTranslation();

  const defaultDescription: string = t('modals.project_list.description');
  const defaultTitle: string = t('modals.project_list.title');

  const [projectList, setProjectList] = useState<Components.Schemas.ProjectResponse[]>([]);
  const [modalDescription, setModalDescription] = useState<string>(defaultDescription);
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const accessToken: string = useSelector(getAccessToken);

  const { getOpenAPIClient } = useContextClient();

  function emptyProjectList(projectList: Components.Schemas.ProjectResponse[]) {
    return projectList.length === 0
  }

  const handleProjectListResponse = (projectList: Components.Schemas.ProjectResponse[]) => {
    if (emptyProjectList(projectList)) {
      setModalDescription(String(t('modals.project_list.empty_list')));
    } else {
      setModalDescription(String(defaultDescription));
    }
    setProjectList(projectList);
  }

  useEffect(() => {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    getAllProjects(accessToken)(getOpenAPIClient!)
      .then(response => {
        handleProjectListResponse(response.data);
      })
      .catch(error => {
        console.error(error);
      })
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [accessToken])

  function renderSharedTeams(teams: Components.Schemas.ProjectSharedTeam[]) {
    return teams.map(team => renderSharedTeam(team));
  }

  function renderSharedTeam(team: Components.Schemas.ProjectSharedTeam) {
    const primaryColor = team.is_personal_workspace ? "#f29b1e" : "#6A74AD"
    return (
      <span style={{
        borderColor: primaryColor, borderWidth: "1px",
        backgroundColor: "#fff",
        borderStyle: "solid", borderRadius: "1.5rem",
        padding: "0 0.25rem 0 0.25rem",
        margin: "0.25rem",
        lineHeight: "1.5rem",
        boxShadow: `1px 1px 1px 0 ${primaryColor}`,
        width: "fit-content",
        whiteSpace: "nowrap"
      }}>
        {renderTeamName(team)}
      </span>
    )
  }

  function renderTeamName(team: Components.Schemas.ProjectSharedTeam) {
    if (team.is_personal_workspace) {
      return t('tools.items.recorder.options.personal_workspace')
    }
    return team.name
  }

  function handleLoadingStart() {
    setIsLoading(true)
    setModalDescription(String(t("general.importing_project")))
  }

  function handleLoadingEnd() {
    setIsLoading(false)
    setModalDescription(String(defaultDescription))
  }

  const onProjectClick = (projectId: number) => {
    handleLoadingStart()
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    importProject(accessToken, projectId)(getOpenAPIClient!)
      .then((response) => {
        if (response.status === 200) {
          const blob = blobFromBase64(response.data)
          onSelectProject(blob)
        }
      })
      .catch((error) => {
        onImportError(error)
      })
      .finally(() => {
        handleLoadingEnd()
      });
  }

  const loader = (
    <div>
      <Spinner ariaLabel={t('general.importing_project')} />
    </div>
  )

  const content = isLoading ? loader : (
    <div style={{cursor: "default"}}>
      {projectList.map(project => (
        <div
          className="project-item"
          style={{
            cursor: "pointer",
            backgroundColor: "#eee",
            marginTop: "1rem", marginBottom: "1rem",
            padding: "0.5rem",
            borderColor: "#6A74AD", borderWidth: "1px",
            borderStyle: "solid", borderRadius: "0.5rem",
            boxShadow: "0 2px 1px 1px #6A74AD"
          }}
          onClick={() => onProjectClick(project.id)}
        >
          <div style={{
            display: "flex",
            flexDirection: "column"
          }}>
            <div>
              <p style={{
                color: "#666",
                fontWeight: "bold",
                padding: "0.5rem 0 0.5rem 0"
              }}>{project.name}</p>
            </div>
            <div style={{
              fontStyle: "italic",
              color: "#666",
              display: "inline",
              fontSize: "0.75rem",
            }}>
              <span style={{whiteSpace: "nowrap", padding: "0.25rem 0 0.25rem 0"}}>
                {t('modals.project_list.shared_with_hint')}:
              </span>
              {renderSharedTeams(project.shared_teams)}
            </div>
          </div>
        </div>
      ))}
    </div>
  )

  return  (
    <SmallModal
      title={String(defaultTitle)}
      description={String(modalDescription)}
      isModalOpen={isOpen}
      handleClose={handleClose}
      actions={[]}
      content={content}
      width={500}
      className="rouded-modal-md bg-opacity-md"
    />
  )
}