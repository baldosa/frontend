import React from 'react';
import { RecordButton } from './RecordButton/RecordButton';
import { ImportAudioButton } from './ImportAudioButton';
import TooltipWrapper from '../../TooltipWrapper';

export const InitialEditorButtonsWrapper = ({
  handleRecording,
  handleImportAudio,
  tooltipTitleRecord = '',
  tooltipTitleImportAudio = '',
  disabled = false,
}: {
  audioState: string;
  handleRecording: () => void;
  handleImportAudio: () => void;
  tooltipTitleRecord?: string;
  tooltipTitleImportAudio?: string;
  disabled?: boolean;
}): React.ReactElement => {
  const initialRecordButtonId = 'initial-record-button';
  const initialImportAudioButtonId = 'initial-import-audio-button';
  return (
    <div
      id="render-recorder-button-with-tooltip"
      style={{
        display: 'flex',
        justifyContent: 'center',
        gap: 20,
      }}
    >
      <>
        <TooltipWrapper
          reference={initialRecordButtonId}
          title={tooltipTitleRecord}
          maxWidth="7rem"
        />
        <RecordButton
          id={initialRecordButtonId}
          onClick={handleRecording}
          disabled={disabled}
        />
      </>
      <>
        <TooltipWrapper
          reference={initialImportAudioButtonId}
          title={tooltipTitleImportAudio}
          maxWidth="7rem"
        />
        <ImportAudioButton
          id={initialImportAudioButtonId}
          onClick={handleImportAudio}
          disabled={disabled}
        />
      </>
    </div>
  );
};
