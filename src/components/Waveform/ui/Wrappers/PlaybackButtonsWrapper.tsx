import React from 'react';
import { isBrowser } from 'react-device-detect';
import { useVerifyScreenOrientation } from '../../../../hooks/useVerifyScreenOrientation';

export const PlaybackButtonsWrapper = ({
  children,
}: {
  children: React.ReactNode;
}): React.ReactElement => {
  const { isPortraitView } = useVerifyScreenOrientation();
  return (
    <div
      id="portrait-view"
      style={{
        display: 'flex',
        flexDirection: 'row',
        width: '100%',
        justifyContent:
          isBrowser || !isPortraitView ? 'center' : 'space-around',
        alignItems: 'center',
      }}
    >
      {children}
    </div>
  );
};
