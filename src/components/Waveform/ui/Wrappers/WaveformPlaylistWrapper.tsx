import React from 'react';
import {
  getActiveRecordingState,
  getEditorOptionsVisibility,
  getEditorContext,
} from '../../../../redux/reducers/recordings';
import { useSelector } from 'react-redux';
import {
  PlaylistStatusEnum,
  PlaylistContextEnum,
} from '../../../../enums/index';
import { useVerifyScreenOrientation } from '../../../../hooks/useVerifyScreenOrientation';

type Props = {
  children: React.ReactNode;
};

export default function WrapperWaveformPlaylist({ children }: Props) {
  const audioState: PlaylistStatusEnum = useSelector(getActiveRecordingState);
  const { isPortraitView } = useVerifyScreenOrientation();
  const editorOptionsVisibility: boolean = useSelector(
    getEditorOptionsVisibility,
  );
  const editorContext: PlaylistContextEnum = useSelector(getEditorContext);

  let ftr = 0;

  if (isPortraitView) {
    if (editorContext === PlaylistContextEnum.RECORDER) {
      if (!editorOptionsVisibility)
        ftr = audioState === PlaylistStatusEnum.NONE ? 170 : 250;
      else ftr = 350;
    } else {
      if (
        [
          PlaylistStatusEnum.RECORDING,
          PlaylistStatusEnum.RECORDING_PAUSED,
        ].includes(audioState)
      )
        ftr = 220;
      else ftr = 190;
    }
  } else ftr = 210;

  return (
    <div
      style={{
        marginTop: 0,
        paddingBottom: 0,
        flexBasis: '80%',
        overflowY: 'scroll',
        maxHeight: window.innerHeight - ftr,
        minHeight: window.innerHeight / 2,
      }}
    >
      {children}
    </div>
  );
}
