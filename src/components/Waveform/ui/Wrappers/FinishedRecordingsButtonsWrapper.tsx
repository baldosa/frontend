import React from 'react';

export const FinishedRecordingsButtonsWrapper = ({
  children,
}: {
  children: React.ReactNode;
}): React.ReactElement => {
  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        gap: 10,
      }}
    >
      {children}
    </div>
  );
};
