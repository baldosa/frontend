import React from 'react';
import { Button } from '@patternfly/react-core';
import { ImportAudioIcon } from '../../../assets/icon/studio/ImportAudioIcon';

export const ImportAudioButton = ({
  id,
  disabled,
  onClick,
}: {
  id: string;
  disabled?: boolean;
  onClick: () => void;
}): React.ReactElement => {
  return (
    <div className="audio-controls-group-recorder-button-finished-wrapper">
      <Button
        id={id}
        className="audio-controls-group-recorder-button-finished"
        data-testid="stop-button"
        style={{
          justifyContent: 'center',
        }}
        {...(!disabled ? { onClick: onClick } : {})}
      >
        <ImportAudioIcon width={'26'} height={'26'} fill="#0C0A21" />
      </Button>
    </div>
  );
};
