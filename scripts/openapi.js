import axios  from 'axios';
import fs  from 'fs';
import path  from 'path';

// URL of the YAML file
const fileUrl = process.env.OPENAPI_SCHEMA_LOCATION

if(!fileUrl) throw new Error("No value assigned to OPENAPI_SCHEMA_LOCATION");

// Directory where the file will be saved
const downloadDir = './src/api';
const fileName = 'schema.json';

// Make sure the download directory exists
if (!fs.existsSync(downloadDir)) {
  fs.mkdirSync(downloadDir, { recursive: true });
}

// Path to save the downloaded file
const filePath = path.join(downloadDir, fileName);

// Function to download the file using axios
async function downloadFile(url, dest) {
  const writer = fs.createWriteStream(dest);

  try {
    const response = await axios({
      url,
      method: 'GET',
      responseType: 'stream',
    });

    response.data.pipe(writer);

    return new Promise((resolve, reject) => {
      writer.on('finish', resolve);
      writer.on('error', reject);
    });
  } catch (error) {
    fs.unlink(dest, () => {}); // Delete the file async if there's an error.
    throw error;
  }
}

// Call the function to download the file
downloadFile(fileUrl, filePath)
  .then(() => {
    console.log(`OPENAPI File downloaded and saved to ${filePath}`);
  })
  .catch((err) => {
    console.error('Error downloading file', err);
    throw new Error(`File not found at: ${fileUrl}`)
  });
